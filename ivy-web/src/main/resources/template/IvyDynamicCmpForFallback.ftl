package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeForComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpForFallback")
public class IvyDynamicCmpForFallback extends NodeForComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpForFallback.class);

    @Override
    public int processFor() throws Exception {
        LOG.info("IvyDynamicCmpForFallback executed!");
        System.out.println("IvyDynamicCmpForFallback executed!");
        return 3;
    }
}