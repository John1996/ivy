package com.ivy.cla;

import com.yomahub.liteflow.context.ContextBean;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Data
@ContextBean("IvyDynamicContext")
public class IvyDynamicContext {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicContext.class);

	private Map<String, Object> dataMap;

	public void putData(String key, Object val) {
		LOG.info(this.getClass().getSimpleName()+" executed putData("+key+","+val+")!");
		System.out.println(this.getClass().getSimpleName()+" executed putData("+key+","+val+")!");
		dataMap.put(key, val);
	}

	public Object getData(String key) {
		LOG.info(this.getClass().getSimpleName()+" executed getData("+key+")!");
		System.out.println(this.getClass().getSimpleName()+" executed getData("+key+")!");
		return dataMap.get(key);
	}

}

