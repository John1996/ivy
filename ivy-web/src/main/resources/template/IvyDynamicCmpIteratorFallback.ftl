package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeIteratorComponent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpIteratorFallback")
public class IvyDynamicCmpIteratorFallback extends NodeIteratorComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIteratorFallback.class);

    @Override
    public Iterator<?> processIterator() throws Exception {
        LOG.info("IvyDynamicCmpIteratorFallback executed!");
        System.out.println("IvyDynamicCmpIteratorFallback executed!");
        List<String> list = new ArrayList<String>(){{
            add("jack");add("mary");add("tom");
        }};
        return list.iterator();
    }
}