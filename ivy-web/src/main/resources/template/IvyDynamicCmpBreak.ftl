package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@LiteflowComponent("IvyDynamicCmpBoolean")
public class IvyDynamicCmpBoolean extends NodeBooleanComponent {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpBoolean.class);

	@Override
	public boolean processBoolean() {
		LOG.info("IvyDynamicCmpBoolean executed!");
		System.out.println("IvyDynamicCmpBoolean executed!");
		return true;
	}
}
