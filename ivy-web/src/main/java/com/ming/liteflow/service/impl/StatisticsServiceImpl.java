package com.ming.liteflow.service.impl;

import cn.hutool.core.util.ReflectUtil;
import com.ming.core.liteflow.entity.IvyCmp;
import com.ming.core.liteflow.entity.IvyDynamicClassTemplate;
import com.ming.core.query.Statistics;
import com.ming.liteflow.service.StatisticsService;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Resource
    private SQLManager sqlManager;

    @Override
    public <T> List<Statistics> groupByList(Class<T> clazz, String groupKey) {
        Integer isDel = ReflectUtil.hasField(clazz, "isDel") ? 0 : null;
        return sqlManager.lambdaQuery(clazz).andEq("is_del", LambdaQuery.filterEmpty(isDel)).groupBy(groupKey)
                .select(Statistics.class, groupKey+" as key_name","count(1) as count");
    }

    @Override
    public <T> Map<String, Integer> groupByMap(Class<T> clazz, String groupKey) {
        List<Statistics> list = groupByList(clazz, groupKey);
        return list.stream().collect(Collectors.toMap(Statistics::getKeyName, Statistics::getCount));
    }
}
