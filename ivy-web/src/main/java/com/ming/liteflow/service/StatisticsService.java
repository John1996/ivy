package com.ming.liteflow.service;

import com.ming.core.query.Statistics;

import java.util.List;
import java.util.Map;

public interface StatisticsService {

    <T> List<Statistics> groupByList(Class<T> clazz, String groupKey);

    <T> Map<String, Integer> groupByMap(Class<T> clazz, String groupKey);

}
