package com.ming.liteflow.cmp.script;

import com.yomahub.liteflow.script.ScriptExecuteWrap;
import com.yomahub.liteflow.script.body.JaninoBooleanScriptBody;

public class ScriptBooleanCmp implements JaninoBooleanScriptBody {
    @Override
    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {
        System.out.println("ScriptBooleanCmp executed!");
        return true;
    }
}
