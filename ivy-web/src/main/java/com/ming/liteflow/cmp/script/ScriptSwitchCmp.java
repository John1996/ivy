package com.ming.liteflow.cmp.script;

import com.yomahub.liteflow.script.ScriptExecuteWrap;
import com.yomahub.liteflow.script.body.JaninoSwitchScriptBody;

public class ScriptSwitchCmp implements JaninoSwitchScriptBody {
    @Override
    public String body(ScriptExecuteWrap scriptExecuteWrap) {
        System.out.println("ScriptSwitchCmp executed!");
        return "cmp_id";
    }
}
