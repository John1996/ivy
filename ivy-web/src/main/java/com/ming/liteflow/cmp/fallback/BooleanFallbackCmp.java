/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.ming.liteflow.cmp.fallback;

import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@FallbackCmp
@LiteflowComponent("BooleanFallbackCmp")
public class BooleanFallbackCmp extends NodeBooleanComponent {
	@Override
	public boolean processBoolean() {
		System.out.println("BooleanFallbackCmp executed!");
		return true;
	}
}
