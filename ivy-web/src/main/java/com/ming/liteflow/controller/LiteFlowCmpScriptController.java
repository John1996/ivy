package com.ming.liteflow.controller;

import com.ming.common.beetl.util.Result;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.ming.core.query.Option;
import com.yomahub.liteflow.enums.ScriptTypeEnum;
import org.beetl.sql.core.SQLManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/liteflow/cmp/script")
public class LiteFlowCmpScriptController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/options")
    @PermissionLimit(limit = false)
    public Result<List<Option>> options(@RequestBody Map<String,Object> map){
        ScriptTypeEnum[] values = ScriptTypeEnum.values();
        List<Option> optionList = new ArrayList<>();
        for (ScriptTypeEnum item : values){
            optionList.add(new Option(item.getEngineName(), item.getDisplayName()));
        }
        return Result.OK(optionList);
    }

}
