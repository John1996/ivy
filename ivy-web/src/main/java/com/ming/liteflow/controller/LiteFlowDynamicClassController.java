package com.ming.liteflow.controller;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import com.ivy.vueflow.parser.execption.LiteFlowELException;
import com.ivy.vueflow.util.IvyGsonUtil;
import com.ming.common.beetl.util.Result;
import com.ming.common.beetl.util.StrUtil;
import com.ming.common.util.ClassFieldUtil;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.ming.core.dynamic.IvyDynamicExec;
import com.ming.core.liteflow.entity.IvyDynamicClass;
import com.ming.core.liteflow.vo.IvyDynamicClassVo;
import com.ming.core.query.Options;
import com.ming.core.query.SortBy;
import com.yomahub.liteflow.core.FlowExecutor;
import groovy.lang.GroovyClassLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.core.query.LambdaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/liteflow/dynamic/class")
public class LiteFlowDynamicClassController {

    private static final Logger LOG = LoggerFactory.getLogger(LiteFlowDynamicClassController.class);
    @Resource
    private SQLManager sqlManager;

    @Autowired
    private ResourceLoader resourceLoader;

    @Resource
    private FlowExecutor flowExecutor;

    @PostMapping("/option")
    @PermissionLimit(limit = false)
    public Result<?> option(@RequestBody Map<String,Object> map) throws LiteFlowELException {
        return Result.OK(ClassFieldUtil.getFieldAnnoDescribe(IvyDynamicClass.class, map));
    }

    @PostMapping("/options")
    @PermissionLimit(limit = false)
    public Result<?> options(@RequestBody(required = false) Map<String,Object> map) throws LiteFlowELException {
        LambdaQuery<IvyDynamicClass> query = sqlManager.lambdaQuery(IvyDynamicClass.class)
                .andEq(IvyDynamicClass::getClassType,0)
                .andEq(IvyDynamicClass::getIsNewVersion,1)
                .andEq(IvyDynamicClass::getIsDel, 0);
        List<IvyDynamicClass> list = query.select(IvyDynamicClass::getId,IvyDynamicClass::getClassId,IvyDynamicClass::getSourceClassName,IvyDynamicClass::getClassName);
        list = list.stream().peek(m->m.setClassName(m.getClassName()+"【"+m.getClassId()+"】")).collect(Collectors.toList());
        return Result.OK(list);
    }

    @PostMapping("/page")
    @PermissionLimit(limit = false)
    public Result<PageResult<IvyDynamicClass>> page(@RequestBody IvyDynamicClassVo vo){
        LambdaQuery<IvyDynamicClass> lambdaQuery = sqlManager.lambdaQuery(IvyDynamicClass.class);
        lambdaQuery.andLike(IvyDynamicClass::getClassId, LambdaQuery.filterLikeEmpty(vo.getClassId()));
        lambdaQuery.andLike(IvyDynamicClass::getClassName, LambdaQuery.filterLikeEmpty(vo.getClassName()));
        lambdaQuery.andLike(IvyDynamicClass::getPackagePath, LambdaQuery.filterLikeEmpty(vo.getPackagePath()));
        lambdaQuery.andEq(IvyDynamicClass::getClassLoaderType, LambdaQuery.filterEmpty(vo.getClassLoaderType()));
        lambdaQuery.andEq(IvyDynamicClass::getClassType, LambdaQuery.filterEmpty(vo.getClassType()));
        lambdaQuery.andEq(IvyDynamicClass::getIsDel, 0);
        lambdaQuery.andEq(IvyDynamicClass::getIsNewVersion,1);
        Options options = vo.getOptions();
        List<SortBy> sortBy = options.getSortBy();
        for (SortBy sort : sortBy) {
            if ("desc".equalsIgnoreCase(sort.getOrder())) {
                lambdaQuery.desc(StrUtil.camelToSnake(sort.getKey()));
            } else {
                lambdaQuery.asc(StrUtil.camelToSnake(sort.getKey()));
            }
        }
        PageResult<IvyDynamicClass> page = lambdaQuery.page(options.getPage(), options.getItemsPerPage());
        return Result.OK(page);
    }

    @PostMapping("/add")
    @PermissionLimit(limit = false)
    public Result<?> add(@RequestBody IvyDynamicClass item){
        LambdaQuery<IvyDynamicClass> query = sqlManager.lambdaQuery(IvyDynamicClass.class);
        item.setVersion(1);
        item.setDataId(UUID.fastUUID().toString(true));
        item.setIsDel(0);
        item.setCreateTime(DateUtil.date().toJdkDate());
        item.setIsNewVersion(1);
        handlerIvyDynamicClass(item);
        int i = query.insert(item);
        return Result.OK(i);
    }

    @PostMapping("/update")
    @PermissionLimit(limit = false)
    public Result<Object> update(@RequestBody IvyDynamicClass item){
        return Result.OK("更新成功", saveOrUpdate(item));
    }

    //调试代码，不增加版本号
    @PostMapping("/updateSourceCode")
    @PermissionLimit(limit = false)
    public Result<Object> updateSourceCode(@RequestBody IvyDynamicClass item){
        int i = sqlManager.lambdaQuery(IvyDynamicClass.class).andEq(IvyDynamicClass::getId, item.getId()).updateSelective(item);
        return Result.OK("更新成功", i);
    }

    private int saveOrUpdate(IvyDynamicClass item){
        IvyDynamicClass oldIvyDynamicClass = sqlManager.lambdaQuery(IvyDynamicClass.class).andEq(IvyDynamicClass::getId, item.getId()).single();
        // 如果代码不相同
        if(!item.getSourceCode().equals(oldIvyDynamicClass.getSourceCode())){
            oldIvyDynamicClass.setClassName(cn.hutool.core.util.StrUtil.isBlank(item.getClassName()) ? null : item.getClassName());
            oldIvyDynamicClass.setSourceCode(cn.hutool.core.util.StrUtil.isBlank(item.getSourceCode()) ? null : item.getSourceCode());
            oldIvyDynamicClass.setInitData(cn.hutool.core.util.StrUtil.isBlank(item.getInitData()) ? null : item.getInitData());
            oldIvyDynamicClass.setClassType(item.getClassType() == null ? null : item.getClassType());
            oldIvyDynamicClass.setIsFallback(item.getIsFallback() == null ? null : item.getIsFallback());
            oldIvyDynamicClass.setVersion(oldIvyDynamicClass.getVersion() + 1);
            oldIvyDynamicClass.setIsNewVersion(1);
            oldIvyDynamicClass.setCreateTime(DateUtil.date().toJdkDate());
            oldIvyDynamicClass.setUpdateTime(DateUtil.date().toJdkDate());
            oldIvyDynamicClass.setId(null);
            oldIvyDynamicClass.setIsDel(0);
            handlerIvyDynamicClass(oldIvyDynamicClass);
            int i = sqlManager.lambdaQuery(IvyDynamicClass.class).insertSelective(oldIvyDynamicClass);

            IvyDynamicClass newIvyDynamicClass = new IvyDynamicClass();
            newIvyDynamicClass.setIsNewVersion(0);
            newIvyDynamicClass.setUpdateTime(DateUtil.date().toJdkDate());
            int j = sqlManager.lambdaQuery(IvyDynamicClass.class).andEq(IvyDynamicClass::getId, item.getId()).updateSelective(newIvyDynamicClass);
            return i;
        }
        item.setUpdateTime(DateUtil.date().toJdkDate());
        handlerIvyDynamicClass(item);
        return sqlManager.lambdaQuery(IvyDynamicClass.class).andEq(IvyDynamicClass::getId, item.getId()).updateSelective(item);
    }

    private void handlerIvyDynamicClass(IvyDynamicClass item){
        String sourceCode = item.getSourceCode();
        String packagePath = getPackagePath(sourceCode);
        if(cn.hutool.core.util.StrUtil.isBlank(packagePath)){
            throw new RuntimeException("源码未设置包路径！");
        }
        item.setPackagePath(packagePath);
        item.setSourceBeanName(getBeanName(sourceCode));
        item.setSourceClassName(getClassName(sourceCode));
        item.setClassId(item.getPackagePath()+"."+item.getSourceClassName());
    }

    @PostMapping("/delete")
    @PermissionLimit(limit = false)
    public Result<?> delete(@RequestBody IvyDynamicClass item){
        if(item.getId() == null){
            return Result.error("参数模板【id】不能为空");
        }
        IvyDynamicClass dynamicClass = new IvyDynamicClass();
        dynamicClass.setIsDel(1);
        dynamicClass.setUpdateTime(new Date());
        return Result.OK("删除成功",sqlManager.lambdaQuery(IvyDynamicClass.class)
                .andEq(IvyDynamicClass::getId, item.getId())
                .updateSelective(dynamicClass));
    }

    @PostMapping("/exec/cmp")
    @PermissionLimit(limit = false)
    public Result<?> execCmp(@RequestBody IvyDynamicClassVo item) throws Exception {
        IvyDynamicClass dynamicClass = sqlManager.lambdaQuery(IvyDynamicClass.class)
                .andEq(IvyDynamicClass::getId, item.getId())
                .single();
        if(!item.getSourceCode().equals(dynamicClass.getSourceCode())){
            dynamicClass.setSourceCode(item.getSourceCode());
        }
        return Result.OK(IvyDynamicExec.execComponent(dynamicClass, item.getEl()));
    }

    @PostMapping("/exec/common")
    @PermissionLimit(limit = false)
    public Result<?> execCommon(@RequestBody IvyDynamicClassVo vo) throws Exception {
        return Result.OK(IvyDynamicExec.execClass(vo.getSourceCode(),vo.getMethodName(),vo.getParams(), vo.getInitData()));
    }

    @PostMapping("/autoCreatedData")
    @PermissionLimit(limit = false)
    public Result<?> autoCreatedData(@RequestBody IvyDynamicClassVo vo) throws Exception {
        if(cn.hutool.core.util.StrUtil.isBlank(vo.getSourceCode())){
            return Result.OK("{}");
        }
        GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        Class<?> clazz = groovyClassLoader.parseClass(vo.getSourceCode());
        return Result.OK(assignRandomValuesAndToJson(clazz));
    }

    public static String assignRandomValuesAndToJson(Class<?> clazz) {
        try {
            // 创建类的实例
            Map<String,Object> objMap = new LinkedHashMap<>();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if(Modifier.isTransient(field.getModifiers()) || Modifier.isFinal(field.getModifiers())){
                    continue;
                }
                field.setAccessible(true); // 设置为可访问
                Class<?> fieldType = field.getType();
                if (fieldType == String.class) {
                    objMap.put(field.getName(), RandomUtil.randomString(10));
                } else if (fieldType == int.class || fieldType == Integer.class) {
                    objMap.put(field.getName(), RandomUtil.randomInt(1, 100));
                } else if (fieldType == long.class || fieldType == Long.class) {
                    objMap.put(field.getName(), RandomUtil.randomLong(1, 1000));
                } else if (fieldType == double.class || fieldType == Double.class) {
                    objMap.put(field.getName(), RandomUtil.randomDouble(1.0, 100.0));
                } else if (fieldType == boolean.class || fieldType == Boolean.class) {
                    objMap.put(field.getName(), RandomUtil.randomBoolean());
                } else if (fieldType == Date.class) {
                    objMap.put(field.getName(), generateRandomDate());
                } else if (fieldType == Map.class) {
                    objMap.put(field.getName(), generateRandomMap(RandomUtil.randomInt(1, 5)));
                } else if (fieldType == Set.class) {
                    objMap.put(field.getName(), generateRandomSet(RandomUtil.randomInt(1, 5)));
                } else if (fieldType == List.class) {
                    objMap.put(field.getName(), generateRandomList(RandomUtil.randomInt(1, 5)));
                }
            }
            return IvyGsonUtil.getGson(true).toJson(objMap);
        } catch (Exception e) {
            e.printStackTrace();
            return "{}";
        }
    }

    // 生成随机的 List
    public static List<Object> generateRandomList(int size) {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(generateRandomValue());
        }
        return list;
    }

    // 生成随机的 Set
    public static Set<Object> generateRandomSet(int size) {
        Set<Object> set = new LinkedHashSet<>();
        for (int i = 0; i < size; i++) {
            set.add(generateRandomValue());
        }
        return set;
    }

    // 生成随机的 Map
    public static Map<String, Object> generateRandomMap(int size) {
        Map<String, Object> map = new LinkedHashMap<>();
        for (int i = 0; i < size; i++) {
            map.put("key" + i, generateRandomValue());
        }
        return map;
    }

    // 生成随机值的方法
    private static Object generateRandomValue() {
        int type = RandomUtil.randomInt(4);
        switch (type) {
            case 0:
                return RandomUtil.randomString(10); // 随机字符串
            case 1:
                return RandomUtil.randomInt(1, 100); // 随机整数
            case 2:
                return RandomUtil.randomDouble(1.0, 100.0); // 随机双精度数
            case 3:
                return RandomUtil.randomBoolean(); // 随机布尔值
            default:
                return generateRandomDate();
        }
    }

    // 生成指定范围内的随机日期
    public static String generateRandomDate() {
        return generateRandomDate(DateUtil.date().offset(DateField.YEAR, -10).toJdkDate(), DateUtil.date().offset(DateField.YEAR, 10).toJdkDate());
    }
    public static String generateRandomDate(Date startDate, Date endDate) {
        long startMillis = startDate.getTime();
        long endMillis = endDate.getTime();
        long randomMillis = RandomUtil.randomLong(startMillis, endMillis);
        return DateUtil.date(new Date(randomMillis)).toString();
    }

    public static String getClassName(String sourceCode){
        Pattern pattern = Pattern.compile("class\\s+(\\w+)");
        Matcher matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    public static String getCmpId(String sourceCode){
        Pattern pattern = Pattern.compile("@LiteflowComponent\\(\"([^\"]+)\"\\)");
        Matcher matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            return  matcher.group(1);
        }
        return null;
    }

    public static String getBeanName(String sourceCode){
        Pattern pattern = Pattern.compile("@LiteflowComponent\\(\"([^\"]+)\"\\)");
        Matcher matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            return  matcher.group(1);
        }
        pattern = Pattern.compile("@ContextBean\\(\"([^\"]+)\"\\)");
        matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            return  matcher.group(1);
        }
        return null;
    }

    public static String getPackagePath(String sourceCode){
        // 定义正则表达式匹配包名
        String packageNameRegex = "\\bpackage\\s+([\\w.]+)\\s*;";
        // 编译正则表达式
        Pattern pattern = Pattern.compile(packageNameRegex);
        // 创建 Matcher 对象并匹配源代码
        Matcher matcher = pattern.matcher(sourceCode);
        // 提取包名
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
