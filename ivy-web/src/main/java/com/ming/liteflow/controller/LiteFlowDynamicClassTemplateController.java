package com.ming.liteflow.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.ivy.vueflow.parser.execption.LiteFlowELException;
import com.ming.common.beetl.util.Result;
import com.ming.common.util.ClassFieldUtil;
import com.ming.common.util.ResourceUtil;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.ming.core.dynamic.IvyDynamicExec;
import com.ming.core.liteflow.entity.IvyDynamicClass;
import com.ming.core.liteflow.entity.IvyDynamicClassTemplate;
import com.ming.core.liteflow.vo.IvyDynamicClassTemplateVo;
import com.ming.core.liteflow.vo.IvyDynamicClassVo;
import com.ming.core.query.*;
import com.ming.liteflow.service.StatisticsService;
import com.yomahub.liteflow.core.FlowExecutor;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.core.query.LambdaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/liteflow/dynamic/class/template")
public class LiteFlowDynamicClassTemplateController {

    private static final Logger LOG = LoggerFactory.getLogger(LiteFlowDynamicClassTemplateController.class);

    @Resource
    private SQLManager sqlManager;

    @Autowired
    private ResourceLoader resourceLoader;

    @Resource
    private FlowExecutor flowExecutor;

    @Resource
    private StatisticsService statisticsService;

    @PostMapping("/option")
    @PermissionLimit(limit = false)
    public Result<?> option(@RequestBody Map<String,Object> map) throws LiteFlowELException {
        Map<String, Integer> classTypeMap = statisticsService.groupByMap(IvyDynamicClassTemplate.class, "class_type");
        Map<String, SelectOption> optionMap = ClassFieldUtil.getFieldAnnoDescribe(IvyDynamicClassTemplate.class, map);
        SelectOption classType = optionMap.get("classType");
        List<SelectOptionItem> optionItemList = classType.getList();
        optionItemList.forEach(m->{
            m.setCount(classTypeMap.getOrDefault(m.getValue().toString(), 0));
        });
        return Result.OK(optionMap);
    }

    @PostMapping("/codeTemplate")
    @PermissionLimit(limit = false)
    public Result<?> codeTemplate(@RequestBody IvyDynamicClassTemplate item){
        if(item.getClassType() == null){
            return Result.error("未选择class类型！");
        }
        String title = "模板_"+ DateUtil.date().toString("yyyyMMdd_HHmmss");
        item.setTitle(title);
        if(item.getClassType() == 0){
            item.setSourceCode(ResourceUtil.getResourceFtl(resourceLoader,"IvyDynamicContext.ftl"));
            return Result.OK(CollUtil.toList(item));
        }
        String key = item.getClassType() + (item.getIsFallback() == null ? "" : item.getIsFallback().toString());
        String file = null;
        switch (key){
            case "10": file = "IvyDynamicCmpCommon.ftl";break;//普通组件类
            case "11": file = "IvyDynamicCmpCommonFallback.ftl";break;//普通组件类
            case "20": file = "IvyDynamicCmpSwitch.ftl";break;//选择组件类
            case "21": file = "IvyDynamicCmpSwitchFallback.ftl";break;//选择组件类
            case "30": file = "IvyDynamicCmpBoolean.ftl";break;//条件组件类
            case "31": file = "IvyDynamicCmpBooleanFallback.ftl";break;//条件组件类
            case "40": file = "IvyDynamicCmpFor.ftl";break;//次数循环组件类
            case "41": file = "IvyDynamicCmpForFallback.ftl";break;//次数循环组件类
            case "50": file = "IvyDynamicCmpIterator.ftl";break;//迭代循环组件类
            case "51": file = "IvyDynamicCmpIteratorFallback.ftl";break;//迭代循环组件类
            default: break;
        }
        item.setSourceCode(ResourceUtil.getResourceFtl(resourceLoader,file));
        return Result.OK(CollUtil.toList(item));
    }

    @PostMapping("/save")
    @PermissionLimit(limit = false)
    public Result<?> save(@RequestBody IvyDynamicClassTemplate item){
        if(StrUtil.isNotBlank(item.getSourceCode())){
            item.setClassName(getClassName(item.getSourceCode()));
        }
        if(item.getId() == null){
            //检测模板名称是否已存在
            long count = sqlManager.lambdaQuery(IvyDynamicClassTemplate.class)
                    .andEq(IvyDynamicClassTemplate::getTitle, item.getTitle())
                    .andEq(IvyDynamicClassTemplate::getIsDel, 0)
                    .count();
            if(count > 0){
                return Result.error("模板名称【title】已存在！");
            }
            item.setCreateTime(new Date());
            item.setIsDel(0);
            int i = sqlManager.lambdaQuery(IvyDynamicClassTemplate.class).insertSelective(item);
            return Result.OK(item);
        }
        //检测模板名称是否已存在
        long count = sqlManager.lambdaQuery(IvyDynamicClassTemplate.class)
                .andEq(IvyDynamicClassTemplate::getTitle, item.getTitle())
                .andEq(IvyDynamicClassTemplate::getIsDel, 0)
                .andNotEq(IvyDynamicClassTemplate::getId, item.getId())
                .count();
        if(count > 0){
            return Result.error("模板名称【title】已存在！");
        }

        item.setUpdateTime(new Date());
        int i = sqlManager.lambdaQuery(IvyDynamicClassTemplate.class)
                .andEq(IvyDynamicClassTemplate::getId, item.getId())
                .updateSelective(item);
        return Result.OK(item);
    }

    @PostMapping("/list")
    @PermissionLimit(limit = false)
    public Result<?> list(@RequestBody IvyDynamicClassTemplate item){
        List<IvyDynamicClassTemplateVo> list = sqlManager.lambdaQuery(IvyDynamicClassTemplate.class)
                .andEq(IvyDynamicClassTemplate::getIsDel, 0)
                .andEq(IvyDynamicClassTemplate::getClassType, LambdaQuery.filterEmpty(item.getClassType()))
                .andEq(IvyDynamicClassTemplate::getIsFallback, LambdaQuery.filterEmpty(item.getIsFallback()))
                .desc(IvyDynamicClassTemplate::getCreateTime)
                .select(IvyDynamicClassTemplateVo.class);
        list.forEach(m->{
            m.setFullName(m.getTitle()+"【"+m.getClassName()+"】");
        });
        return Result.OK(list);
    }

    @PostMapping("/delete")
    @PermissionLimit(limit = false)
    public Result<?> delete(@RequestBody IvyDynamicClassTemplate item){
        if(item.getId() == null){
            return Result.error("参数模板【id】不能为空");
        }
        IvyDynamicClassTemplate template = new IvyDynamicClassTemplate();
        template.setIsDel(1);
        template.setUpdateTime(new Date());
        return Result.OK(sqlManager.lambdaQuery(IvyDynamicClassTemplate.class)
                .andEq(IvyDynamicClassTemplate::getId, item.getId())
                .updateSelective(template));
    }

    public static String getClassName(String sourceCode){
        Pattern pattern = Pattern.compile("class\\s+(\\w+)");
        Matcher matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
