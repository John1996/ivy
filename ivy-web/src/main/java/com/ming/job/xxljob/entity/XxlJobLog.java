package com.ming.job.xxljob.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.fetch.annotation.Fetch;
import org.beetl.sql.fetch.annotation.FetchSql;

@Data
@Fetch(level = 2)
@Table(name = "xxl_job_log")
public class XxlJobLog {
	
	private Long id;
	
	// job info
	private Integer jobGroup;
	private Integer jobId;

	// execute info
	private String executorAddress;
	private String executorHandler;
	private String executorParam;
	private String executorShardingParam;
	private Integer executorFailRetryCount;
	
	// trigger info
	private String triggerTime;
	private Integer triggerCode;
	private String triggerMsg;
	
	// handle info
	private String handleTime;
	private Integer handleCode;
	private String handleMsg;

	// alarm info
	private Integer alarmStatus;

	@FetchSql("select title from xxl_job_group where id =#{jobGroup}")
	private XxlJobGroup xxlJobGroup;

	@FetchSql("select job_desc from xxl_job_info where id =#{jobId}")
	private XxlJobInfo xxlJobInfo;
}
