package com.ming.monitor.controller;

import com.ming.common.beetl.util.Result;
import org.beetl.sql.core.SQLManager;
import org.beifengtz.jvmm.core.JvmmExecutor;
import org.beifengtz.jvmm.core.JvmmFactory;
import org.beifengtz.jvmm.core.JvmmProfiler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/monitor/profiler")
public class IvyMonitorProfilerController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/info")
    public Result<?> info(@RequestBody(required = false) Map<String,Object> map) {
        //提供火焰图生成器
        JvmmProfiler profiler = JvmmFactory.getProfiler();
        return Result.OK(profiler);
    }


}
