package com.ming.monitor.controller;

import com.ming.common.beetl.util.Result;
import org.beetl.sql.core.SQLManager;
import org.beifengtz.jvmm.core.JvmmCollector;
import org.beifengtz.jvmm.core.JvmmExecutor;
import org.beifengtz.jvmm.core.JvmmFactory;
import org.beifengtz.jvmm.core.entity.info.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/monitor/executor")
public class IvyMonitorExecutorController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/info")
    public Result<?> info(@RequestBody(required = false) Map<String,Object> map) {
        //提供所有的执行接口
        JvmmExecutor executor = JvmmFactory.getExecutor();
        return Result.OK(executor);
    }


}
