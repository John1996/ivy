package com.ming.monitor.controller;

import cn.hutool.system.SystemUtil;
import cn.hutool.system.oshi.OshiUtil;
import com.ming.common.beetl.util.Result;
import org.beetl.sql.core.SQLManager;
import org.beifengtz.jvmm.core.JvmmCollector;
import org.beifengtz.jvmm.core.JvmmFactory;
import org.beifengtz.jvmm.core.entity.info.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import oshi.hardware.CentralProcessor;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HWDiskStore;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;
import oshi.software.os.OSProcess;
import oshi.software.os.OperatingSystem;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/monitor/os")
public class IvyMonitorOSController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/info")
    public Result<?> info(@RequestBody(required = false) Map<String,Object> map) {
        JvmmCollector collector = JvmmFactory.getCollector();
        SysInfo sys = collector.getSys();//操作系统信息
        SysMemInfo sysMem = collector.getSysMem();//操作系统内存数据
        List<SysFileInfo> sysFile = collector.getSysFile();//采集操作系统磁盘分区使用情况数据
        ProcessInfo process = collector.getProcess();//当前进程数据
        List<DiskInfo> disk = collector.getDisk();//物理机磁盘数据
        CompletableFuture<List<DiskIOInfo>> diskIO = collector.getDiskIO();//物理机磁盘IO及吞吐量数据
        CompletableFuture<DiskIOInfo> diskIO1 = collector.getDiskIO("C");//物理机磁盘IO及吞吐量数据
        CompletableFuture<CPUInfo> cpu = collector.getCPU();//物理机CPU负载数据
        CompletableFuture<NetInfo> network = collector.getNetwork();//物理机网卡信息及IO数据
        PortInfo portInfo = collector.getPortInfo(8080);//物理机器端口使用情况


        // 基础操作系统信息
        System.out.println("操作系统名称：" + SystemUtil.getOsInfo().getName());
        System.out.println("操作系统版本：" + SystemUtil.getOsInfo().getVersion());
        System.out.println("系统架构：" + SystemUtil.getOsInfo().getArch());
        System.out.println("用户名称：" + SystemUtil.getUserInfo().getName());
        System.out.println("用户目录：" + SystemUtil.getUserInfo().getHomeDir());
        System.out.println("用户工作目录：" + SystemUtil.getUserInfo().getCurrentDir());

        // 通过 Oshi 获取更多系统信息
        OperatingSystem os = OshiUtil.getOs();
        System.out.println("OS 制造商：" + os.getManufacturer());
        System.out.println("系统启动时间：" + os.getSystemBootTime());
        // 列举当前进程信息
        for (OSProcess osProcess : os.getProcesses()) {
            System.out.println("进程名称：" + osProcess.getName() + " | PID：" + osProcess.getProcessID());
        }
        System.out.println("Java 版本：" + SystemUtil.getJavaInfo().getVersion());

        // 获取物理磁盘信息
        List<HWDiskStore> diskStores = OshiUtil.getDiskStores();
        for (HWDiskStore diskStore : diskStores) {
            System.out.println("磁盘名称：" + diskStore.getName());
            System.out.println("磁盘模型：" + diskStore.getModel());
            System.out.println("磁盘序列号：" + diskStore.getSerial());
            System.out.println("磁盘大小：" + diskStore.getSize() / (1024 * 1024 * 1024) + " GB");
            System.out.println("读数据大小：" + diskStore.getReadBytes() / (1024 * 1024) + " MB");
            System.out.println("写数据大小：" + diskStore.getWriteBytes() / (1024 * 1024) + " MB");
            System.out.println("--------------------------------");
        }

        // 获取文件系统信息
        FileSystem fileSystem = OshiUtil.getOs().getFileSystem();
        List<OSFileStore> fileStores = fileSystem.getFileStores();
        for (OSFileStore fs : fileStores) {
            System.out.println("文件系统名称：" + fs.getName());
            System.out.println("挂载点：" + fs.getMount());
            System.out.println("总容量：" + fs.getTotalSpace() / (1024 * 1024 * 1024) + " GB");
            System.out.println("可用容量：" + fs.getUsableSpace() / (1024 * 1024 * 1024) + " GB");
        }


        CentralProcessor processor = OshiUtil.getProcessor();
        System.out.println("CPU 逻辑核心数：" + processor.getLogicalProcessorCount());
        System.out.println("CPU 物理核心数：" + processor.getPhysicalProcessorCount());
        System.out.println("CPU 制造商：" + processor.getProcessorIdentifier().getVendor());
        System.out.println("CPU 名称：" + processor.getProcessorIdentifier().getName());
        System.out.println("CPU 频率：" + processor.getProcessorIdentifier().getVendorFreq() / (1_000_000) + " MHz");


        GlobalMemory memory = OshiUtil.getMemory();
        System.out.println("总内存：" + memory.getTotal() / (1024 * 1024 * 1024) + " GB");
        System.out.println("可用内存：" + memory.getAvailable() / (1024 * 1024 * 1024) + " GB");
        System.out.println("已用内存：" +
                (memory.getTotal() - memory.getAvailable()) / (1024 * 1024 * 1024) + " GB");
        return Result.OK(collector);
    }

}
