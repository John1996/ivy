package com.ming.monitor.controller;

import cn.hutool.system.SystemUtil;
import com.ming.common.beetl.util.Result;
import org.beetl.sql.core.SQLManager;
import org.beifengtz.jvmm.core.JvmmCollector;
import org.beifengtz.jvmm.core.JvmmFactory;
import org.beifengtz.jvmm.core.entity.info.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.ThreadMXBean;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/monitor/jvm")
public class IvyMonitorJVMController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/info")
    public Result<?> info(@RequestBody(required = false) Map<String,Object> map) {
        JvmmCollector collector = JvmmFactory.getCollector();
        JvmClassLoadingInfo jvmClassLoading = collector.getJvmClassLoading();//JVM类加载信息
        List<JvmClassLoaderInfo> jvmClassLoaders = collector.getJvmClassLoaders();//JVM类加载器信息
        JvmCompilationInfo jvmCompilation = collector.getJvmCompilation();//JVM编译信息
        List<JvmGCInfo> jvmGC = collector.getJvmGC();//JVM垃圾收集器信息
        List<JvmMemoryManagerInfo> jvmMemoryManager = collector.getJvmMemoryManager();//JVM内存管理器信息
        List<JvmMemoryPoolInfo> jvmMemoryPool = collector.getJvmMemoryPool();//JVM内存池信息
        JvmMemoryInfo jvmMemory = collector.getJvmMemory();//JVM内存使用情况
        JvmThreadInfo jvmThread = collector.getJvmThread();//JVM线程统计数据
        //String jvmThreadStack = collector.getJvmThreadStack(0);//指定JVM线程堆栈数据
        //JvmThreadDetailInfo jvmThreadDetailInfo = collector.getJvmThreadDetailInfo(0);//采集JVM线程详情信息（CPU Time、Block Time、Locks等）
        //ThreadPoolInfo threadPoolInfo = collector.getThreadPoolInfo();//JVM线程池信息
        //collector.getOrderedThreadTimedInfo();//JVM线程在一定时间内CPU占用时间情况
        //collector.getOrderedThreadTimedStack();//JVM线程在一定时间内CPU占用时间情况
        //collector.dumpAllThreads();//dump所有线程堆栈数据


        // 基本 JVM 信息
        System.out.println("JVM 名称：" + SystemUtil.getJavaRuntimeInfo().getName());
        System.out.println("JVM 版本：" + SystemUtil.getJavaRuntimeInfo().getVersion());
        System.out.println("JVM 供应商：" + SystemUtil.getJavaInfo().getVendor());

        // 运行时内存信息
        System.out.println("JVM 内存最大可用：" + Runtime.getRuntime().maxMemory() / (1024 * 1024) + " MB");
        System.out.println("JVM 已分配内存：" + Runtime.getRuntime().totalMemory() / (1024 * 1024) + " MB");
        System.out.println("JVM 可用内存：" + Runtime.getRuntime().freeMemory() / (1024 * 1024) + " MB");

        // 使用 ManagementFactory 获取更详细的 JVM 信息
        MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
        MemoryUsage heapMemoryUsage = memoryMXBean.getHeapMemoryUsage();
        System.out.println("堆内存最大值：" + heapMemoryUsage.getMax() / (1024 * 1024) + " MB");
        System.out.println("堆内存已用：" + heapMemoryUsage.getUsed() / (1024 * 1024) + " MB");

        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        System.out.println("活动线程数：" + threadMXBean.getThreadCount());
        System.out.println("守护线程数：" + threadMXBean.getDaemonThreadCount());
        System.out.println("峰值线程数：" + threadMXBean.getPeakThreadCount());
        return Result.OK(collector);
    }


}
