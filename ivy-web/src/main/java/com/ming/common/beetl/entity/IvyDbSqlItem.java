package com.ming.common.beetl.entity;

import lombok.Data;

@Data
public class IvyDbSqlItem {

    private Integer index;
    private String type;
    private String column;

    private Integer startRow;
    private Integer pageSize;

}
