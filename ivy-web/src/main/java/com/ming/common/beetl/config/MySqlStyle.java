package com.ming.common.beetl.config;

import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.AutoID;
import org.beetl.sql.annotation.entity.SeqID;
import org.beetl.sql.clazz.kit.BeanKit;
import org.beetl.sql.clazz.kit.KeyWordHandler;
import org.beetl.sql.core.db.AbstractDBStyle;
import org.beetl.sql.core.range.OffsetLimitRange;
import org.beetl.sql.core.range.RangeSql;

import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.List;

public class MySqlStyle extends AbstractDBStyle {
    protected RangeSql rangeSql = null;

    public MySqlStyle() {
        this.rangeSql = new OffsetLimitRange(this);
        this.keyWordHandler = new KeyWordHandler() {
            public String getTable(String tableName) {
                return tableName;
            }

            public String getCol(String colName) {
                return colName;
            }
        };
    }

    public int getIdType(Class c, String idProperty) {
        List<Annotation> ans = BeanKit.getAllAnnotation(c, idProperty);
        int idType = 2;
        Iterator var5 = ans.iterator();

        while(var5.hasNext()) {
            Annotation an = (Annotation)var5.next();
            if (an instanceof AutoID) {
                idType = 2;
                break;
            }

            if (!(an instanceof SeqID) && an instanceof AssignID) {
                idType = 1;
            }
        }

        return idType;
    }

    public String getName() {
        return "mysql";
    }

    public int getDBType() {
        return 1;
    }

    public RangeSql getRangeSql() {
        return this.rangeSql;
    }
}

