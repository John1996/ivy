package com.ming.common.beetl.controller;

import com.ming.core.query.Options;
import com.ming.core.query.SortBy;
import com.ming.common.beetl.entity.IvyDbDatasource;
import com.ming.common.beetl.util.Result;
import com.ming.common.beetl.util.StrUtil;
import com.ming.common.beetl.vo.IvyDbDatasourceVo;
import com.ming.common.util.ClassFieldUtil;
import com.ming.common.xxljob.annotation.PermissionLimit;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.core.query.LambdaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/db/datasource")
public class IvyDbDatasourceController {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDbDatasourceController.class);

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/option")
    @PermissionLimit(limit = false)
    public Result<?> option(@RequestBody Map<String,Object> map) {
        return Result.OK(ClassFieldUtil.getFieldAnnoDescribe(IvyDbDatasource.class, map));
    }

    @PostMapping("/options")
    @PermissionLimit(limit = false)
    public Result<?> options(@RequestBody Map<String,Object> map) {
        LambdaQuery<IvyDbDatasource> query = sqlManager.lambdaQuery(IvyDbDatasource.class);
        List<IvyDbDatasource> list = query.select(IvyDbDatasource::getId,IvyDbDatasource::getDsBeanName,IvyDbDatasource::getDsName);
        list = list.stream().peek(m->m.setDsName(m.getDsName()+"【"+m.getDsBeanName()+"】")).collect(Collectors.toList());
        return Result.OK(list);
    }

    @PostMapping("/page")
    @PermissionLimit(limit = false)
    public Result<PageResult<IvyDbDatasource>> page(@RequestBody IvyDbDatasourceVo vo){
        LambdaQuery<IvyDbDatasource> lambdaQuery = sqlManager.lambdaQuery(IvyDbDatasource.class);
        lambdaQuery.andLike(IvyDbDatasource::getDsBeanName, LambdaQuery.filterLikeEmpty(vo.getDsBeanName()));
        lambdaQuery.andLike(IvyDbDatasource::getDsName, LambdaQuery.filterLikeEmpty(vo.getDsName()));
        lambdaQuery.andEq(IvyDbDatasource::getDisabled, LambdaQuery.filterEmpty(vo.getDisabled()));
        lambdaQuery.andLike(IvyDbDatasource::getJdbcUrl, LambdaQuery.filterLikeEmpty(vo.getJdbcUrl()));
        Options options = vo.getOptions();
        List<SortBy> sortBy = options.getSortBy();
        for (SortBy sort : sortBy) {
            if ("desc".equalsIgnoreCase(sort.getOrder())) {
                lambdaQuery.desc(StrUtil.camelToSnake(sort.getKey()));
            } else {
                lambdaQuery.asc(StrUtil.camelToSnake(sort.getKey()));
            }
        }
        PageResult<IvyDbDatasource> page = lambdaQuery.page(options.getPage(), options.getItemsPerPage());
        return Result.OK(page);
    }

    @PostMapping("/add")
    @PermissionLimit(limit = false)
    public Result<?> add(@RequestBody IvyDbDatasource item){
        LambdaQuery<IvyDbDatasource> lambdaQuery = sqlManager.lambdaQuery(IvyDbDatasource.class);
        lambdaQuery.andEq(IvyDbDatasource::getDsBeanName, item.getDsBeanName());
        long count = lambdaQuery.count();
        if(count > 0){
            return Result.error("新增失败：beanName已存在");
        }
        LambdaQuery<IvyDbDatasource> query = sqlManager.lambdaQuery(IvyDbDatasource.class);
        if(item.getDisabled() == null){
            item.setDisabled(false);
        }
        int i = query.insert(item);
        return Result.OK(i);
    }

    @PostMapping("/update")
    @PermissionLimit(limit = false)
    public Result<Object> update(@RequestBody IvyDbDatasource item){
        LambdaQuery<IvyDbDatasource> lambdaQuery = sqlManager.lambdaQuery(IvyDbDatasource.class);
        lambdaQuery.andNotEq(IvyDbDatasource::getId, item.getId());
        lambdaQuery.andEq(IvyDbDatasource::getDsBeanName, item.getDsBeanName());
        long count = lambdaQuery.count();
        if(count > 0){
            return Result.error("更新失败：beanName已存在");
        }
        int i = sqlManager.updateById(item);
        return Result.OK("更新成功", i);
    }

    @PostMapping("/delete")
    @PermissionLimit(limit = false)
    public Result<Integer> delete(@RequestBody IvyDbDatasource item){
        LambdaQuery<IvyDbDatasource> lambdaQuery = sqlManager.lambdaQuery(IvyDbDatasource.class);
        lambdaQuery.andEq(IvyDbDatasource::getId, item.getId());
        int i = lambdaQuery.delete();
        return Result.OK("删除成功",i);
    }


}
