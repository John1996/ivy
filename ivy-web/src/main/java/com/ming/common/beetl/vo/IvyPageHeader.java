package com.ming.common.beetl.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.page.PageResult;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IvyPageHeader {

    private String title;
    private String key;
    private Boolean sortable;
    private Boolean removable;

}
