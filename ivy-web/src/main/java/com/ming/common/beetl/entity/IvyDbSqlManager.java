package com.ming.common.beetl.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_db_sql_manager")
@Generate(isEffective = false, moduleName = "db", desc = "SQLManager管理")
public class IvyDbSqlManager {

    @Column
    @PrimaryKey
    private Long id;

    @Column(len = 20)
    @Describe(value = "主库数据源，ivy_db_datasource表ID")
    private Long datasourceId;

    @Column(len = 255)
    @Describe(value = "从库数据源,可选多个")
    private String slaveDatasourceIds;

    @Column(len = 20)
    @Describe(value = "数据库类型，默认mysql",items = {
        @DescribeItem(value = "MySqlStyle",desc = "Mysql"),
        @DescribeItem(value = "OracleStyle",desc = "Oralce"),
        @DescribeItem(value = "Oracle12Style",desc = "Oralce12"),
        @DescribeItem(value = "PostgresStyle",desc = "Postgres"),
        @DescribeItem(value = "Gbase8sStyle",desc = "Gbase8s"),
        @DescribeItem(value = "GreatSqlStyle",desc = "Great"),
        @DescribeItem(value = "GreenplumDBStyle",desc = "Greenplum"),
        @DescribeItem(value = "SqlServerStyle",desc = "SqlServer"),
        @DescribeItem(value = "SqlServer2012Style",desc = "SqlServer2012"),
        @DescribeItem(value = "H2Style",desc = "H2"),
        @DescribeItem(value = "DB2SqlStyle",desc = "DB2"),
        @DescribeItem(value = "DerbyStyle",desc = "Derby"),
        @DescribeItem(value = "SQLiteStyle",desc = "SQLite"),
        @DescribeItem(value = "ClickHouseStyle",desc = "ClickHouse"),
        @DescribeItem(value = "CassandraSqlStyle",desc = "Apache Cassandra"),
        @DescribeItem(value = "DrillStyle",desc = "Apache Drill"),
        @DescribeItem(value = "DruidStyle",desc = "Apache Druid"),
        @DescribeItem(value = "HiveStyle",desc = "Apache Hive"),
        @DescribeItem(value = "IgniteStyle",desc = "Apache Ignite"),
        @DescribeItem(value = "CouchBaseStyle",desc = "CouchBase"),
        @DescribeItem(value = "MachbaseStyle",desc = "Machbase"),
        @DescribeItem(value = "PrestoStyle",desc = "PrestoSQL"),
        @DescribeItem(value = "ShenTongSqlStyle",desc = "神通数据库"),
        @DescribeItem(value = "PolarDBStyle",desc = "阿里云数据库 polar"),
        @DescribeItem(value = "TaosStyle",desc = "TD-Engine"),
        @DescribeItem(value = "OpenGaussStyle",desc = "华为高斯"),
        @DescribeItem(value = "KingbaseStyle",desc = "人大金仓"),
        @DescribeItem(value = "DamengStyle",desc = "达梦"),
    })
    private String dbStyle;

    @Column(len = 12)
    @Describe(value = "管理器类型",items = {
        @DescribeItem(value = "single",desc = "单数据源"),
        @DescribeItem(value = "masterSlave",desc = "主从数据源"),
    })
    private String type;

    @Column(len = 255)
    @Describe(value = "Bean名称，如：mysqlManager")
    private String smBeanName;

    @Column(len = 255)
    @Describe(value = "sql管理器中文名称")
    private String smName;

    @Column(len = 255)
    @Describe(value = "描述信息")
    private String smDesc;

    @Column(len = 32)
    @Describe(value = "sql拦截器,可选多个",items = {
        @DescribeItem(value = "DebugInterceptor",desc = "DebugInterceptor"),
        @DescribeItem(value = "DebugWithNameInterceptor",desc = "DebugWithNameInterceptor"),
        //@DescribeItem(value = "SimpleCacheInterceptor",desc = "SimpleCacheInterceptor"),
        @DescribeItem(value = "SimpleDebugInterceptor",desc = "SimpleDebugInterceptor"),
        @DescribeItem(value = "Slf4JLogInterceptor",desc = "Slf4JLogInterceptor"),
        //@DescribeItem(value = "TimeStatInterceptor",desc = "TimeStatInterceptor"),
    })
    private String sqlInterceptors;

    @Column(len = 32)
    @Describe(value = "命名风格",items = {
        @DescribeItem(value = "DefaultNameConversion",desc = "DefaultNameConversion"),
        @DescribeItem(value = "UnderlinedNameConversion",desc = "UnderlinedNameConversion"),
    })
    private String nameConversion;

    @Column(len = 1)
    @Describe(value = "分页是否从0开始",items = {
        @DescribeItem(value = "0",desc = "否"),
        @DescribeItem(value = "1",desc = "是"),
    })
    private Boolean offsetStartZero;

    @Column(len = 1)
    @Describe(value = "是否禁用",items = {
        @DescribeItem(value = "0",desc = "启用"),
        @DescribeItem(value = "1",desc = "禁用"),
    })
    private Boolean disabled;
}
