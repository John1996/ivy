package com.ming.common.ivy;

import com.ming.common.util.FileUtil;
import org.beetl.sql.core.SQLManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;

@Component
public class IvyInit {

    private static final Logger LOG = LoggerFactory.getLogger(IvyInit.class);

    @Resource
    private SQLManager sqlManager;

    @PostConstruct
    public void init(){
        String root = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        String packagePath = String.join(File.separator,"com.ivy".split("\\."));
        String rootPath = root + packagePath;
        FileUtil.removeAllFile(rootPath);
    }

}
