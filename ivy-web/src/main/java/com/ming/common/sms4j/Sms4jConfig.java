package com.ming.common.sms4j;

import org.dromara.sms4j.core.datainterface.SmsReadConfig;
import org.dromara.sms4j.provider.config.BaseConfig;
import org.dromara.sms4j.unisms.config.UniConfig;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Sms4jConfig implements SmsReadConfig {
    @Override
    public BaseConfig getSupplierConfig(String configId) {
        UniConfig uniConfig = new UniConfig();
        //此处仅为示例，实际环境中，数据可以来自任意位置，
        return uniConfig;
    }

    @Override
    public List<BaseConfig> getSupplierConfigList() {
        //此处仅为示例，实际环境中，数据可以来自任意位置，
        return null;
    }
}
