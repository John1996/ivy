package com.ming.core.dynamic;

import com.ming.core.dynamic.spring.core.ClassInfo;
import com.ming.core.dynamic.spring.util.SpringBeanUtil;
import groovy.lang.GroovyClassLoader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IvyDynamicLoader {

    /**
     * 动态编译class并注册到spring容器中
     * @param javaCode
     * @return
     */
    public static <T> Class<? extends T> registerSpring(String javaCode){
        try {
            ClassInfo classInfo = getClassInfo(null,javaCode);

            GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
            Class clazz = groovyClassLoader.parseClass(javaCode);

            //Class<? extends T> clazz = (Class<? extends T>) loader.findClass(classInfo.getFullClassName());
            SpringBeanUtil.replace(classInfo.getBeanName(), clazz);
            return clazz;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> ClassInfo registerSpringClassInfo(String beanName,String javaCode){
        try {
            ClassInfo classInfo = getClassInfo(beanName, javaCode);
            //Class<? extends T> clazz = (Class<? extends T>) loader.findClass(classInfo.getFullClassName());
            GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
            Class clazz = groovyClassLoader.parseClass(javaCode);

            SpringBeanUtil.replace(classInfo.getBeanName(), clazz);
            classInfo.setClazz(clazz);
            return classInfo;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 动态构建组件
     * @param
     * @return
     */
    public static void registerComponent(){

    }

    /**
     * 动态加载jar
     * @param jarPath
     * @return
     */
    public static void loadJar(String jarPath){

    }

    public static ClassInfo getClassInfo(String beanName,String javaStr){
        String className = getClassName(javaStr);
        if(beanName == null){
            String cmpId = getCmpId(javaStr);
            if(cmpId != null){
                beanName = cmpId;
            }else{
                beanName = className.substring(0,1).toLowerCase()+className.substring(1);
            }
        }
        String packageName = getPackageName(javaStr);
        String fullClassName = packageName+"."+className;
        return new ClassInfo(className,packageName,beanName,fullClassName,null);
    }

    private static final Pattern CLASS_PATTERN = Pattern.compile("class\\s+([$_a-zA-Z][$_a-zA-Z0-9]*)\\s*");

    private static String getClassName(String javaCode){
        Matcher matcher = CLASS_PATTERN.matcher(javaCode);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new IllegalArgumentException("No valid class");
        }
    }

    /**
     * package名正则表达式
     */
    private static final Pattern PACKAGE_NAME_PATTERN = Pattern.compile("package\\s([a-zA-Z_][a-zA-Z0-9_]*)+([.][a-zA-Z_][a-zA-Z0-9_]*)+;");

    /**
     * 获取package
     *
     * @param sourceCode 源码
     * @return 包名
     */
    public static String getPackageName(String sourceCode) {
        Matcher matcher = PACKAGE_NAME_PATTERN.matcher(sourceCode);
        if (matcher.find()) {
            return matcher.group().replaceFirst("package", "").replace(";", "").trim();
        }
        return null;
    }

    private static String getCmpId(String sourceCode){
        Pattern pattern = Pattern.compile("@LiteflowComponent\\(\"([^\"]+)\"\\)");
        Matcher matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            return  matcher.group(1);
        }
        return null;
    }
}
