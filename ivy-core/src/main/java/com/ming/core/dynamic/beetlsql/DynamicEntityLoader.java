package com.ming.core.dynamic.beetlsql;

import com.ming.core.dynamic.IvyDynamicLoader;
import org.beetl.core.GroupTemplate;
import org.beetl.core.misc.ByteClassLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.gen.SourceBuilder;
import org.beetl.sql.gen.SourceConfig;
import org.beetl.sql.gen.simple.EntitySourceBuilder;
import org.beetl.sql.gen.simple.StringOnlyProject;

import javax.tools.JavaFileObject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

public class DynamicEntityLoader<T> {
    protected SQLManager sqlManager;
    protected Map<String, Class<? extends T>> cache;
    private final Pattern CLASS_PATTERN;
    private static Map<String, JavaFileObject> fileObjectMap = new ConcurrentHashMap();
    protected String pkg;
    protected Class<T> baseClass;
    protected ByteClassLoader loader;

    public DynamicEntityLoader(SQLManager sqlManager) {
        this(sqlManager, "com.test001", (Class<T>) BaseEntity.class);
    }

    public DynamicEntityLoader(SQLManager sqlManager, String pkg, Class<T> clazz) {
        this.cache = new ConcurrentHashMap();
        this.CLASS_PATTERN = Pattern.compile("class\\s+([$_a-zA-Z][$_a-zA-Z0-9]*)\\s*");
        this.loader = null;
        this.sqlManager = sqlManager;
        this.pkg = pkg;
        this.baseClass = clazz;
        ClassLoader defaultClassLoader = Thread.currentThread().getContextClassLoader() != null ? Thread.currentThread().getContextClassLoader() : GroupTemplate.class.getClassLoader();
        this.loader = new ByteClassLoader(defaultClassLoader);
    }

    public DynamicEntityLoader(SQLManager sqlManager, String pkg, Class<T> clazz, ClassLoader classLoader) {
        this.cache = new ConcurrentHashMap();
        this.CLASS_PATTERN = Pattern.compile("class\\s+([$_a-zA-Z][$_a-zA-Z0-9]*)\\s*");
        this.loader = null;
        this.sqlManager = sqlManager;
        this.pkg = pkg;
        this.baseClass = clazz;
        this.loader = new ByteClassLoader(classLoader);
    }

    public Class<? extends T> getDynamicEntity(String table) {
        return this.getDynamicEntity(table, this.baseClass);
    }

    public Class<? extends T> getDynamicEntity(String table, Class<T> clazz) {
        Class<? extends T> c = (Class)this.cache.get(table);
        if (c != null) {
            return c;
        } else {
            c = (Class)this.cache.computeIfAbsent(table, (s) -> {
                Class<? extends T> newCLass = this.compile(s, clazz.getName());
                return newCLass;
            });
            return c;
        }
    }

    protected Class<? extends T> compile(String table, String baseObject) {
        List<SourceBuilder> sourceBuilder = new ArrayList();
        SourceBuilder entityBuilder = new EntitySourceBuilder();
        sourceBuilder.add(entityBuilder);
        SourceConfig config = new SourceConfig(this.sqlManager, sourceBuilder);
        config.setEntityParentClass(baseObject);
        StringOnlyProject project = new StringOnlyProject() {
            public String getBasePackage(String sourceBuilderName) {
                return DynamicEntityLoader.this.pkg + "." + sourceBuilderName;
            }
        };
        config.gen(table, project);
        String javaCode = project.getContent();
        return IvyDynamicLoader.registerSpring(javaCode);
    }

}
