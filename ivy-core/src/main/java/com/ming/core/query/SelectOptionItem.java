package com.ming.core.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SelectOptionItem {

    private String title;

    private Object value;

    private Integer count;

    public SelectOptionItem(String title, Object value) {
        this.title = title;
        this.value = value;
    }

}
