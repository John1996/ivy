package com.ming.core.query;

import lombok.Data;

@Data
public class SortBy {

    private String key;

    private String order;

}
