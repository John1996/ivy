package com.ming.core.query;

import lombok.Data;

@Data
public class Statistics {

    private String keyName;

    private Integer count;

}
