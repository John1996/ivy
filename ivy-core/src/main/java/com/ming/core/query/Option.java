package com.ming.core.query;

import lombok.Data;

@Data
public class Option {

    private String title;

    private String value;

    public Option() { }

    public Option(String title, String value) {
        this.title = title;
        this.value = value;
    }

}

