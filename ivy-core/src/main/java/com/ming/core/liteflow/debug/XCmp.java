package com.ming.core.liteflow.debug;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("x")
public class XCmp extends NodeComponent {

    @Override
    public void process() {
        System.out.println("XCmp executed！");
    }
}
