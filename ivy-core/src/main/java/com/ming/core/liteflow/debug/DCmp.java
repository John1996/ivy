package com.ming.core.liteflow.debug;

import cn.hutool.core.thread.ThreadUtil;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("d")
public class DCmp extends NodeComponent {

    @Override
    public void process() {
        ThreadUtil.sleep(3000);
        System.out.println("DCmp executed！");
//        if(RandomUtil.randomInt(0, 11) < 4){
//            throw new RuntimeException("DCmp RuntimeException!");
//        }
    }
}
