package com.ming.core.liteflow.debug;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IvyContextBean {

    private String requestId;//请求ID

    private Map<String,Object> dataMap;

    public IvyContextBean(String requestId) {
        this.requestId = requestId;
    }
}
