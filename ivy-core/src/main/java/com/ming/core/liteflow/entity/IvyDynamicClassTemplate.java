package com.ming.core.liteflow.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import com.ming.core.generate.template.annotation.database.Text;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
@Table(name = "ivy_dynamic_class_template")
@Generate(isEffective = true, moduleName = "db", desc = "动态类模板")
public class IvyDynamicClassTemplate {

    @Column
    @PrimaryKey
    private Long id;

    @Column(len = 2)
    @Describe(value = "class类型",items = {
            @DescribeItem(value = "0",desc = "数据上下文【context】"),
            @DescribeItem(value = "1",desc = "普通组件类【common】"),
            @DescribeItem(value = "2",desc = "选择组件类【switch】"),
            @DescribeItem(value = "3",desc = "布尔组件类【boolean】"),
            @DescribeItem(value = "4",desc = "次数循环组件类【for】"),
            @DescribeItem(value = "5",desc = "迭代循环组件类【iterator】"),
    })
    private Integer classType;

    @Column(len = 1)
    @Describe(value = "是否为降级组件",items = {
            @DescribeItem(value = "0",desc = "非降级组件"),
            @DescribeItem(value = "1",desc = "降级组件"),
    })
    private Integer isFallback;

    @Column
    @Describe(value = "模板标题（中文）")
    private String title;

    @Column
    @Describe(value = "类名")
    private String className;

    @Text
    @Column
    @Describe(value = "Java源码")
    private String sourceCode;

    @Column(len = 1)
    @Describe(value = "是否删除",items = {
            @DescribeItem(value = "0",desc = "未删除"),
            @DescribeItem(value = "1",desc = "已删除"),
    })
    private Integer isDel;

    @Column
    @Describe(value = "创建时间")
    private Date createTime;

    @Column
    @Describe(value = "更新时间")
    private Date updateTime;

}
