package com.ming.core.liteflow.debug;

import cn.hutool.core.thread.ThreadUtil;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("e")
public class ECmp extends NodeComponent {

    @Override
    public void process() {
        ThreadUtil.sleep(3000);
        System.out.println("ECmp executed！");
//        if(RandomUtil.randomInt(0, 11) < 4){
//            throw new RuntimeException("ECmp RuntimeException!");
//        }
    }
}
