package com.ming.core.liteflow.vo;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import com.ming.core.generate.template.annotation.database.Text;
import com.ming.core.liteflow.entity.IvyDynamicClassTemplate;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
public class IvyDynamicClassTemplateVo extends IvyDynamicClassTemplate {

    private String fullName;

}
