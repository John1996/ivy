package com.ming.core.liteflow.debug;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("b")
public class BCmp extends NodeComponent {

    @Override
    public void process() {
        ThreadUtil.sleep(3000);
        System.out.println("BCmp executed！");
//        if(RandomUtil.randomInt(0, 11) < 4){
//            throw new RuntimeException("BCmp RuntimeException!");
//        }
    }
}
