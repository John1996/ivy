package com.ming.core.liteflow.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import com.ming.core.generate.template.annotation.database.Text;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
@Table(name = "ivy_dynamic_class")
@Generate(isEffective = true, isCover = true, moduleName = "db", desc = "动态类")
public class IvyDynamicClass {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    @Describe(value = "数据ID")
    private String dataId;

    @Column(len = 128)
    @Describe(value = "动态类ID")
    private String classId;

    @Column(len = 2)
    @Describe(value = "class类型",items = {
            @DescribeItem(value = "0",desc = "数据上下文【context】"),
            @DescribeItem(value = "1",desc = "普通组件类【common】"),
            @DescribeItem(value = "2",desc = "选择组件类【switch】"),
            @DescribeItem(value = "3",desc = "布尔组件类【boolean】"),
            @DescribeItem(value = "4",desc = "次数循环组件类【for】"),
            @DescribeItem(value = "5",desc = "迭代循环组件类【iterator】"),
    })
    private Integer classType;

    @Column(len = 1)
    @Describe(value = "是否为降级组件",items = {
            @DescribeItem(value = "0",desc = "非降级组件"),
            @DescribeItem(value = "1",desc = "降级组件"),
    })
    private Integer isFallback;

    @Column
    @Describe(value = "包路径")
    private String packagePath;

    @Column
    @Describe(value = "动态类标题")
    private String className;

    @Column(len = 1)
    @Describe(value = "加载器类型",items = {
            @DescribeItem(value = "0",desc = "Groovy类加载器"),
            @DescribeItem(value = "1",desc = "应用程序类加载器"),
            @DescribeItem(value = "2",desc = "自定义类加载器"),
    })
    private Integer classLoaderType;

    @Text
    @Column
    @Describe(value = "Java源码")
    private String sourceCode;

    @Column
    @Describe(value = "版本号")
    private Integer version;

    @Column
    @Describe(value = "是否最新版本",items = {
            @DescribeItem(value = "0",desc = "否"),
            @DescribeItem(value = "1",desc = "是"),
    })
    private Integer isNewVersion;

    @Column
    @Describe(value = "Bean名称")
    private String sourceBeanName;

    @Column
    @Describe(value = "源码类名")
    private String sourceClassName;

    @Column
    @Describe(value = "备注")
    private String remark;

    @Text
    @Column
    @Describe(value = "初始化数据（JSON）")
    private String initData;

    @Column(len = 1)
    @Describe(value = "是否删除",items = {
            @DescribeItem(value = "0",desc = "未删除"),
            @DescribeItem(value = "1",desc = "已删除"),
    })
    private Integer isDel;

    @Column
    @Describe(value = "创建时间")
    private Date createTime;

    @Column
    @Describe(value = "更新时间")
    private Date updateTime;

}
