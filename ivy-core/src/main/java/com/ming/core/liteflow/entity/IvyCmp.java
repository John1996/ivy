package com.ming.core.liteflow.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import com.ming.core.generate.template.annotation.database.Text;
import com.yomahub.liteflow.enums.NodeTypeEnum;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
@Table(name = "ivy_cmp")
@Generate(isEffective = true, moduleName = "db", tableName = "ivy_cmp", desc = "组件信息")
public class IvyCmp {

    @PrimaryKey
    @Column
    private Long id;

    @Column(len = 64)
    private String componentId;

    @Column(len = 64)
    private String componentName;

    @Column
    private String type;//

    private NodeTypeEnum nodeType;//switch,for

    @Column
    @Text
    private String script;//

    @Column
    private String language;//

    @Column(len = 256)
    private String clazz;//

    @Column
    private Long fallbackId;//

    private String fallbackType;//

    @Column(len = 256)
    private String el;

    @Column(len = 256)
    private String elFormat;

    @Column(len = 256)
    private String cmpPre;

    @Column(len = 256)
    private String cmpFinallyOpt;

    @Column
    private String cmpId;

    @Column
    private String cmpTag;

    @Column
    private Integer cmpMaxWaitSeconds;

    @Column(len = 256)
    private String cmpTo;

    @Column(len = 256)
    private String cmpDefaultOpt;

    private Object cmpToEL;
    private Object cmpDefaultOptEL;

    @Column(len = 256)
    private String cmpTrueOpt;
    private Object cmpTrueOptEL;

    @Column(len = 256)
    private String cmpFalseOpt;
    private Object cmpFalseOptEL;

    @Column
    private Boolean cmpParallel;

    @Column(len = 256)
    private String cmpDoOpt;
    private Object cmpDoOptEL;

    @Column(len = 256)
    private String cmpBreakOpt;
    private Object cmpBreakOptEL;

    @Column(len = 32)
    private String cmpDataName;

    @Text
    @Column
    private String cmpData;

    @Column(len = 1)
    @Describe(value = "是否删除",items = {
            @DescribeItem(value = "0",desc = "未删除"),
            @DescribeItem(value = "1",desc = "已删除"),
    })
    private Integer isDel;

    @Column
    @Describe(value = "创建时间")
    private Date createTime;

    @Column
    @Describe(value = "更新时间")
    private Date updateTime;
}
