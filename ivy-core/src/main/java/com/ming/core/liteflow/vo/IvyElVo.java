package com.ming.core.liteflow.vo;

import com.ming.core.query.Options;
import com.ming.core.liteflow.entity.IvyEl;
import lombok.Data;

@Data
public class IvyElVo extends IvyEl {

    private Options options;

}
