package com.ming.core.liteflow.vo;

import com.ming.core.liteflow.entity.IvyCmp;
import com.ming.core.query.Options;
import lombok.Data;

@Data
public class IvyCmpVo extends IvyCmp {

    private Options options;

}
