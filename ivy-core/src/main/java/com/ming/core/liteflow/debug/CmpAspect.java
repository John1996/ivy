package com.ming.core.liteflow.debug;

import com.github.benmanes.caffeine.cache.Cache;
import com.yomahub.liteflow.aop.ICmpAroundAspect;
import com.yomahub.liteflow.core.NodeComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CmpAspect implements ICmpAroundAspect {

    @Autowired
    private Cache<String, Map<String,IvyCmpStep>> ivyCmpStepCache;

    @Override
    public void beforeProcess(NodeComponent cmp) {
        //before business
    }

    @Override
    public void afterProcess(NodeComponent cmp) {
        //after business
    }

    @Override
    public void onSuccess(NodeComponent cmp) {
        cache(cmp,true,null);
    }

    @Override
    public void onError(NodeComponent cmp, Exception e) {
        cache(cmp,false,e);
    }

    private synchronized void cache(NodeComponent cmp, boolean isSuccess, Exception e){
        try {
            IvyContextBean context = cmp.getContextBean(IvyContextBean.class);
            String requestId = context.getRequestId();
            Map<String, IvyCmpStep> cmpStepMap = ivyCmpStepCache.getIfPresent(requestId);
            boolean flag = false;
            if(cmpStepMap == null){
                cmpStepMap = new ConcurrentHashMap<>();
                flag = true;
            }
            IvyCmpStep ivyCmpStep = new IvyCmpStep();
            ivyCmpStep.setNodeId(cmp.getNodeId());
            ivyCmpStep.setIsSuccess(isSuccess);
            ivyCmpStep.setException(e);
            cmpStepMap.put(cmp.getNodeId(), ivyCmpStep);
            if(flag) {
                ivyCmpStepCache.put(context.getRequestId(), cmpStepMap);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
