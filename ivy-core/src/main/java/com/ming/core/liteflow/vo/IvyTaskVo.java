package com.ming.core.liteflow.vo;

import com.ming.core.query.Options;
import com.ming.core.liteflow.entity.IvyTask;
import lombok.Data;

@Data
public class IvyTaskVo extends IvyTask {

    private Options options;

}
