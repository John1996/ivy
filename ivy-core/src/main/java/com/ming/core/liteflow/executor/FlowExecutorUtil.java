package com.ming.core.liteflow.executor;

import com.ming.core.liteflow.config.IvyConfigUtil;
import com.ming.core.liteflow.entity.IvyConfig;
import com.ming.core.util.NumberUtil;
import com.yomahub.liteflow.builder.el.LiteFlowChainELBuilder;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.core.FlowExecutorHolder;
import com.yomahub.liteflow.flow.LiteflowResponse;
import com.yomahub.liteflow.property.LiteflowConfig;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class FlowExecutorUtil {

    public static FlowExecutor getFlowExecutor(){
        return FlowExecutorHolder.loadInstance(IvyConfigUtil.getDefaultConfig());
    }

    public static FlowExecutor getFlowExecutor(IvyConfig conf){
        return FlowExecutorHolder.loadInstance(IvyConfigUtil.getConfig(conf));
    }

    public static FlowExecutor getFlowExecutor(LiteflowConfig config){
        return FlowExecutorHolder.loadInstance(config);
    }

    public static String executor(FlowExecutor flowExecutor, String el,Object param, Object... contextBeanArray){
        String chainId = "chain_test_"+ NumberUtil.getNextIndex();
        LiteFlowChainELBuilder.createChain().setChainId(chainId).setChainName(chainId).setEL(el).build();
        Future<LiteflowResponse> future = flowExecutor.execute2Future(chainId, param, contextBeanArray);
        try {
            //System.out.println("-----------------------------exec start-----------------------------");
            LiteflowResponse response = future.get();
            //System.out.println("-----------------------------exec end-----------------------------");
            return response.getExecuteStepStrWithTime();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e.getCause());
        }
    }

    public static String executor(String el,Object param, Object... contextBeanArray){
        FlowExecutor flowExecutor = FlowExecutorUtil.getFlowExecutor();
        return executor(flowExecutor, el, param, contextBeanArray);
    }
}
