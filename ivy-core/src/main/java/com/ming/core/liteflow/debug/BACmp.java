package com.ming.core.liteflow.debug;

import cn.hutool.core.thread.ThreadUtil;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("ba")
public class BACmp extends NodeBooleanComponent {

    @Override
    public boolean processBoolean() throws Exception {
        System.out.println("BACmp executed！");
        return true;
    }
}
