package com.ming.core.liteflow.debug;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("y")
public class YCmp extends NodeComponent {

    @Override
    public void process() {
        System.out.println("YCmp executed！");
    }
}
