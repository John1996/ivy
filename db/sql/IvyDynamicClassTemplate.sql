DROP TABLE IF EXISTS `ivy_dynamic_class_template`;
-- 建表语句
CREATE TABLE `ivy_dynamic_class_template` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`class_type` INT(2)  DEFAULT NULL  COMMENT 'class类型【0:数据上下文【context】,1:普通组件类【common】,2:选择组件类【switch】,3:布尔组件类【boolean】,4:次数循环组件类【for】,5:迭代循环组件类【iterator】】',
`is_fallback` INT(1)  DEFAULT NULL  COMMENT '是否为降级组件【0:非降级组件,1:降级组件】',
`title` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '模板标题（中文）',
`class_name` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '类名',
`source_code` TEXT  DEFAULT NULL  COMMENT 'Java源码',
`is_del` INT(1)  DEFAULT NULL  COMMENT '是否删除【0:未删除,1:已删除】',
`create_time` datetime  DEFAULT NULL  COMMENT '创建时间',
`update_time` datetime  DEFAULT NULL  COMMENT '更新时间',
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='动态类模板';

-- 新增字段
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN class_type INT(2)  DEFAULT NULL  COMMENT 'class类型【0:数据上下文【context】,1:普通组件类【common】,2:选择组件类【switch】,3:布尔组件类【boolean】,4:次数循环组件类【for】,5:迭代循环组件类【iterator】】';
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN is_fallback INT(1)  DEFAULT NULL  COMMENT '是否为降级组件【0:非降级组件,1:降级组件】';
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN title VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '模板标题（中文）';
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN class_name VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '类名';
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN source_code TEXT  DEFAULT NULL  COMMENT 'Java源码';
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN is_del INT(1)  DEFAULT NULL  COMMENT '是否删除【0:未删除,1:已删除】';
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN create_time datetime  DEFAULT NULL  COMMENT '创建时间';
ALTER TABLE `ivy_dynamic_class_template` ADD COLUMN update_time datetime  DEFAULT NULL  COMMENT '更新时间';

