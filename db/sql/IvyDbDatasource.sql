DROP TABLE IF EXISTS `ivy_db_datasource`;
-- 建表语句
CREATE TABLE `ivy_db_datasource` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`ds_bean_name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT 'Bean名称，如：ds0',
`ds_name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据源中文名称',
`ds_desc` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '描述信息',
`driver_class_name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据库驱动路径',
`jdbc_url` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据库连接路径',
`username` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '用户名',
`password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '密码',
`disabled` TINYINT(1)  DEFAULT NULL  COMMENT '是否禁用【0:启用,1:禁用】',
`maximum_pool_size` INT(11)  DEFAULT NULL  COMMENT '连接池的最大连接数',
`minimum_idle` INT(11)  DEFAULT NULL  COMMENT '连接池的最小空闲连接数',
`idle_timeout` BIGINT(20)  DEFAULT NULL  COMMENT '连接在池中保持空闲的最大时间，超过这个时间会被释放',
`connection_timeout` BIGINT(20)  DEFAULT NULL  COMMENT '获取连接的超时时间',
`validation_timeout` BIGINT(20)  DEFAULT NULL  COMMENT '在连接池中进行连接有效性验证的超时时间',
`max_lifetime` BIGINT(20)  DEFAULT NULL  COMMENT '连接在池中允许存活的最长时间',
`connection_test_query` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '用于测试连接是否有效的SQL查询语句',
`connection_init_sql` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '在连接被添加到连接池之前执行的SQL语句',
`pool_name` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '连接池的名称',
`allow_pool_suspension` TINYINT(1)  DEFAULT NULL  COMMENT '是否允许连接池暂停',
`read_only` TINYINT(1)  DEFAULT NULL  COMMENT '连接是否只读',
`auto_commit` TINYINT(1)  DEFAULT NULL  COMMENT '是否自动提交',
`register_mbeans` TINYINT(1)  DEFAULT NULL  COMMENT '是否注册MBeans',
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据源';

-- 新增字段
ALTER TABLE `ivy_db_datasource` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_db_datasource` ADD COLUMN ds_bean_name VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT 'Bean名称，如：ds0';
ALTER TABLE `ivy_db_datasource` ADD COLUMN ds_name VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据源中文名称';
ALTER TABLE `ivy_db_datasource` ADD COLUMN ds_desc VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '描述信息';
ALTER TABLE `ivy_db_datasource` ADD COLUMN driver_class_name VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据库驱动路径';
ALTER TABLE `ivy_db_datasource` ADD COLUMN jdbc_url VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据库连接路径';
ALTER TABLE `ivy_db_datasource` ADD COLUMN username VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '用户名';
ALTER TABLE `ivy_db_datasource` ADD COLUMN password VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '密码';
ALTER TABLE `ivy_db_datasource` ADD COLUMN disabled TINYINT(1)  DEFAULT NULL  COMMENT '是否禁用【0:启用,1:禁用】';
ALTER TABLE `ivy_db_datasource` ADD COLUMN maximum_pool_size INT(11)  DEFAULT NULL  COMMENT '连接池的最大连接数';
ALTER TABLE `ivy_db_datasource` ADD COLUMN minimum_idle INT(11)  DEFAULT NULL  COMMENT '连接池的最小空闲连接数';
ALTER TABLE `ivy_db_datasource` ADD COLUMN idle_timeout BIGINT(20)  DEFAULT NULL  COMMENT '连接在池中保持空闲的最大时间，超过这个时间会被释放';
ALTER TABLE `ivy_db_datasource` ADD COLUMN connection_timeout BIGINT(20)  DEFAULT NULL  COMMENT '获取连接的超时时间';
ALTER TABLE `ivy_db_datasource` ADD COLUMN validation_timeout BIGINT(20)  DEFAULT NULL  COMMENT '在连接池中进行连接有效性验证的超时时间';
ALTER TABLE `ivy_db_datasource` ADD COLUMN max_lifetime BIGINT(20)  DEFAULT NULL  COMMENT '连接在池中允许存活的最长时间';
ALTER TABLE `ivy_db_datasource` ADD COLUMN connection_test_query VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '用于测试连接是否有效的SQL查询语句';
ALTER TABLE `ivy_db_datasource` ADD COLUMN connection_init_sql VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '在连接被添加到连接池之前执行的SQL语句';
ALTER TABLE `ivy_db_datasource` ADD COLUMN pool_name VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '连接池的名称';
ALTER TABLE `ivy_db_datasource` ADD COLUMN allow_pool_suspension TINYINT(1)  DEFAULT NULL  COMMENT '是否允许连接池暂停';
ALTER TABLE `ivy_db_datasource` ADD COLUMN read_only TINYINT(1)  DEFAULT NULL  COMMENT '连接是否只读';
ALTER TABLE `ivy_db_datasource` ADD COLUMN auto_commit TINYINT(1)  DEFAULT NULL  COMMENT '是否自动提交';
ALTER TABLE `ivy_db_datasource` ADD COLUMN register_mbeans TINYINT(1)  DEFAULT NULL  COMMENT '是否注册MBeans';

