DROP TABLE IF EXISTS `ivy_cmp`;
-- 建表语句
CREATE TABLE `ivy_cmp` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`component_id` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`component_name` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`type` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`script` TEXT  DEFAULT NULL ,
`language` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`clazz` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`fallback_id` BIGINT(20)  DEFAULT NULL ,
`el` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`el_format` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_pre` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_finally_opt` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_id` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_tag` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_max_wait_seconds` INT(11)  DEFAULT NULL ,
`cmp_to` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_default_opt` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_true_opt` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_false_opt` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_parallel` TINYINT(1)  DEFAULT NULL ,
`cmp_do_opt` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_break_opt` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_data_name` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ,
`cmp_data` TEXT  DEFAULT NULL ,
`is_del` INT(1)  DEFAULT NULL  COMMENT '是否删除【0:未删除,1:已删除】',
`create_time` datetime  DEFAULT NULL  COMMENT '创建时间',
`update_time` datetime  DEFAULT NULL  COMMENT '更新时间',
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='组件信息';

-- 新增字段
ALTER TABLE `ivy_cmp` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN component_id VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN component_name VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN type VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN script TEXT  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN language VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN clazz VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN fallback_id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN el VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN el_format VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_pre VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_finally_opt VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_id VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_tag VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_max_wait_seconds INT(11)  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_to VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_default_opt VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_true_opt VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_false_opt VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_parallel TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_do_opt VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_break_opt VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_data_name VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN cmp_data TEXT  DEFAULT NULL ;
ALTER TABLE `ivy_cmp` ADD COLUMN is_del INT(1)  DEFAULT NULL  COMMENT '是否删除【0:未删除,1:已删除】';
ALTER TABLE `ivy_cmp` ADD COLUMN create_time datetime  DEFAULT NULL  COMMENT '创建时间';
ALTER TABLE `ivy_cmp` ADD COLUMN update_time datetime  DEFAULT NULL  COMMENT '更新时间';

