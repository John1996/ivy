DROP TABLE IF EXISTS `ivy_dynamic_class`;
-- 建表语句
CREATE TABLE `ivy_dynamic_class` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`data_id` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据ID',
`class_id` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '动态类ID',
`class_type` INT(2)  DEFAULT NULL  COMMENT 'class类型【0:数据上下文【context】,1:普通组件类【common】,2:选择组件类【switch】,3:布尔组件类【boolean】,4:次数循环组件类【for】,5:迭代循环组件类【iterator】】',
`is_fallback` INT(1)  DEFAULT NULL  COMMENT '是否为降级组件【0:非降级组件,1:降级组件】',
`package_path` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '包路径',
`class_name` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '动态类标题',
`class_loader_type` INT(1)  DEFAULT NULL  COMMENT '加载器类型【0:Groovy类加载器,1:应用程序类加载器,2:自定义类加载器】',
`source_code` TEXT  DEFAULT NULL  COMMENT 'Java源码',
`version` INT(11)  DEFAULT NULL  COMMENT '版本号',
`is_new_version` INT(11)  DEFAULT NULL  COMMENT '是否最新版本【0:否,1:是】',
`source_bean_name` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT 'Bean名称',
`source_class_name` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '源码类名',
`remark` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '备注',
`init_data` TEXT  DEFAULT NULL  COMMENT '初始化数据（JSON）',
`is_del` INT(1)  DEFAULT NULL  COMMENT '是否删除【0:未删除,1:已删除】',
`create_time` datetime  DEFAULT NULL  COMMENT '创建时间',
`update_time` datetime  DEFAULT NULL  COMMENT '更新时间',
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='动态类';

-- 新增字段
ALTER TABLE `ivy_dynamic_class` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_dynamic_class` ADD COLUMN data_id VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '数据ID';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_id VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '动态类ID';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_type INT(2)  DEFAULT NULL  COMMENT 'class类型【0:数据上下文【context】,1:普通组件类【common】,2:选择组件类【switch】,3:布尔组件类【boolean】,4:次数循环组件类【for】,5:迭代循环组件类【iterator】】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN is_fallback INT(1)  DEFAULT NULL  COMMENT '是否为降级组件【0:非降级组件,1:降级组件】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN package_path VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '包路径';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_name VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '动态类标题';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_loader_type INT(1)  DEFAULT NULL  COMMENT '加载器类型【0:Groovy类加载器,1:应用程序类加载器,2:自定义类加载器】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN source_code TEXT  DEFAULT NULL  COMMENT 'Java源码';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN version INT(11)  DEFAULT NULL  COMMENT '版本号';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN is_new_version INT(11)  DEFAULT NULL  COMMENT '是否最新版本【0:否,1:是】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN source_bean_name VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT 'Bean名称';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN source_class_name VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '源码类名';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN remark VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL  COMMENT '备注';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN init_data TEXT  DEFAULT NULL  COMMENT '初始化数据（JSON）';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN is_del INT(1)  DEFAULT NULL  COMMENT '是否删除【0:未删除,1:已删除】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN create_time datetime  DEFAULT NULL  COMMENT '创建时间';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN update_time datetime  DEFAULT NULL  COMMENT '更新时间';

