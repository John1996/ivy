package com.ivy.vueflow.convert.flow;

import com.yomahub.liteflow.builder.el.ELWrapper;

public interface FlowConvert {

    String el2Json(String el);

}
