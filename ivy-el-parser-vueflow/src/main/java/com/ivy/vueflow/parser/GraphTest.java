package com.ivy.vueflow.parser;

import com.ivy.vueflow.builder.IvyBuilderUtil;
import com.ivy.vueflow.builder.IvyParserUtil;
import com.ivy.vueflow.builder.flow.FlowBuilder;
import com.ivy.vueflow.parser.entity.edge.Edge;
import com.ivy.vueflow.parser.entity.edge.EdgeData;
import com.ivy.vueflow.parser.entity.node.Node;
import com.ivy.vueflow.parser.enums.IvyEnums;
import com.ivy.vueflow.parser.execption.LiteFlowELException;
import com.ivy.vueflow.parser.graph.Graph;
import com.ivy.vueflow.parser.graph.GraphEL;
import com.ivy.vueflow.parser.graph.GraphInfo;

public class GraphTest {

    public static void main(String[] args) throws LiteFlowELException {
        Node a = IvyBuilderUtil.buildCommonNode().id("a").name("组件a").build();
        Node b = IvyBuilderUtil.buildNode(IvyEnums.NodeEnum.COMMON).id("b").name("组件b").build();

        Edge e = IvyBuilderUtil.buildCommonEdge(a,b).label("普通路径")
                .data(EdgeData.builder().id(null).tag(null).build()).build();

        FlowBuilder builder = IvyBuilderUtil.buildFlow().format(true);
        builder.addNode(a);
        builder.addNode(b);
        builder.addEdge(e);


        //获取流程解析相关信息
        GraphEL graphEL = new GraphEL(builder.build());
        //输出EL表达式
        System.out.println(graphEL.toString());
    }

    /*public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addNode(new Node("node1", "普通节点1"));
        graph.addNode(new Node("node2", "普通节点2"));
        graph.addNode(new Node("node3", "普通节点3"));
        graph.addNode(new Node("node4", "普通节点4"));

        graph.addEdge(new Edge("node1", "node2"));
        graph.addEdge(new Edge("node1", "node3"));
        graph.addEdge(new Edge("node2", "node4"));
        graph.addEdge(new Edge("node3", "node4"));

//        List<List<Node>> allPaths = graph.findAllPaths("node1", "node4");
//        List<Node> segmentationPoints = graph.findSegmentationPoints(allPaths);
//        List<List<Node>> segments = graph.getProcessSegments("node1", "node4");
//
//        System.out.println("All paths:");
//        for (List<Node> path : allPaths) {
//            for (Node node : path) {
//                System.out.print(node + " ");
//            }
//            System.out.println();
//        }
//
//        System.out.println("Segmentation Points:");
//        for (Node node : segmentationPoints) {
//            System.out.println(node);
//        }
//
//        System.out.println("Process Segments:");
//        for (List<Node> segment : segments) {
//            for (Node node : segment) {
//                System.out.print(node + " ");
//            }
//            System.out.println();
//        }
    }*/
}