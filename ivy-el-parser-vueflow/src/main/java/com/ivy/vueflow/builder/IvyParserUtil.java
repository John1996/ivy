package com.ivy.vueflow.builder;


import com.ivy.vueflow.builder.flow.FlowBuilder;
import com.ivy.vueflow.parser.entity.FlowData;
import com.ivy.vueflow.parser.execption.LiteFlowELException;
import com.ivy.vueflow.parser.graph.Graph;
import com.ivy.vueflow.parser.graph.GraphInfo;

public class IvyParserUtil {


    public static GraphInfo parserFlow(FlowBuilder builder) throws LiteFlowELException {
        return parserFlow(builder.getFlowData());
    }


    public static GraphInfo parserFlow(String flowJson) throws LiteFlowELException {
        return new Graph(flowJson).toELInfo();
    }

    public static GraphInfo parserFlow(FlowData flowData) throws LiteFlowELException {
        return new Graph(flowData).toELInfo();
    }
}
