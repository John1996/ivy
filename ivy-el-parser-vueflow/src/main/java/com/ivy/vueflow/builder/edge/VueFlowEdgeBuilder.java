package com.ivy.vueflow.builder.edge;

import cn.hutool.core.util.StrUtil;
import com.ivy.vueflow.parser.entity.edge.Edge;
import com.ivy.vueflow.parser.entity.edge.EdgeData;
import com.ivy.vueflow.parser.entity.node.Node;
import com.ivy.vueflow.parser.enums.IvyEnums;

import java.util.UUID;

public class VueFlowEdgeBuilder implements EdgeBuilder {

    private Edge edge = new Edge();

    private IvyEnums.PATH_ENUM pathEnum;

    @Override
    public EdgeBuilder createEdge(Node sourceNode, Node targetNode) {
        EdgeData edgeData = new EdgeData();

        edge.setSourceNode(sourceNode);
        edge.setTargetNode(targetNode);
        edge.setId(UUID.randomUUID().toString());
        edge.setType("animation");
        edge.setSource(sourceNode.getId());
        edge.setTarget(targetNode.getId());
//        edge.setSourceHandle();
//        edge.setTargetHandle();
        edge.setData(edgeData);
        edge.setLabel("");
        edge.setAnimated(true);
        edge.setUpdatable(true);
        edge.setMarkerEnd("arrowclosed");
//        edge.setSourceX();
//        edge.setSourceY();
//        edge.setTargetX();
//        edge.setTargetY();
//        edge.setStyle();
//        edge.setLabelStyle();
//        edge.setLabelBgPadding();
//        edge.setLabelBgBorderRadius();
//        edge.setLabelBgStyle();

        this.edgeType(IvyEnums.PATH_ENUM.common_path);
        return this;
    }

    @Override
    public EdgeBuilder edgeType(IvyEnums.PATH_ENUM pathEnum) {
        this.pathEnum = pathEnum;
        EdgeData data = edge.getData();
        if(data != null){
            data.setType(pathEnum.getValue());
        }else{
            data = new EdgeData();
            data.setType(pathEnum.getValue());
            edge.setData(data);
        }
        return this;
    }

    @Override
    public EdgeBuilder label(String label) {
        edge.setLabel(label);
        return this;
    }

    @Override
    public EdgeBuilder data(EdgeData edgeData) {
        if(edgeData != null && StrUtil.isBlank(edgeData.getType()) && pathEnum != null){
            edgeData.setType(pathEnum.getValue());
        }
        edge.setData(edgeData);
        return this;
    }

    @Override
    public Edge build() {
        return edge;
    }

}
