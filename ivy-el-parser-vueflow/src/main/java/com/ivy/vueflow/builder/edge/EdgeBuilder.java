package com.ivy.vueflow.builder.edge;

import com.ivy.vueflow.parser.entity.edge.Edge;
import com.ivy.vueflow.parser.entity.edge.EdgeData;
import com.ivy.vueflow.parser.entity.node.Node;
import com.ivy.vueflow.parser.enums.IvyEnums;

public interface EdgeBuilder {

    EdgeBuilder createEdge(Node sourceNode, Node targetNode);

    EdgeBuilder edgeType(IvyEnums.PATH_ENUM pathEnum);
    EdgeBuilder label(String label);

    EdgeBuilder data(EdgeData edgeData);

    Edge build();

}
