package com.ivy.vueflow.builder.flow;

import com.ivy.vueflow.parser.entity.FlowData;
import com.ivy.vueflow.parser.entity.edge.Edge;
import com.ivy.vueflow.parser.entity.node.Node;

import java.util.List;

public interface FlowBuilder {


    FlowBuilder defaultData();

    FlowBuilder addNode(Node... nodes);

    FlowBuilder addEdge(Edge... edges);

    FlowBuilder format(boolean format);

    String build();

    FlowData getFlowData();
    List<Node> getNodes();
    List<Edge> getEdges();
    Node getNodeByCmpId(String cmpId);
    Node getNodeById(String id);

}
