package com.ivy.vueflow.builder.flow;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ivy.vueflow.builder.IvyBuilderUtil;
import com.ivy.vueflow.convert.bean.CmpProperty;
import com.ivy.vueflow.parser.entity.FlowData;
import com.ivy.vueflow.parser.entity.Viewport;
import com.ivy.vueflow.parser.entity.edge.Edge;
import com.ivy.vueflow.parser.entity.edge.EdgeData;
import com.ivy.vueflow.parser.entity.node.*;
import com.ivy.vueflow.parser.entity.style.NodeStyle;
import com.ivy.vueflow.parser.entity.style.NodeStyleHandles;
import com.ivy.vueflow.parser.entity.style.NodeStyleToolbar;
import com.ivy.vueflow.util.IvyGsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VueFlowBuilder implements FlowBuilder {

    private FlowData flowData = new FlowData();

    @Override
    public FlowBuilder defaultData() {
        flowData.setNodes(new ArrayList<>());
        flowData.setEdges(new ArrayList<>());
        flowData.setPosition(CollUtil.toList(0d,0d));
        flowData.setZoom(1d);
        flowData.setViewport(new Viewport(0d,0d,1d));
        return this;
    }

    @Override
    public FlowBuilder addNode(Node... nodes) {
        if(ArrayUtil.isNotEmpty(nodes)){
            for(Node node : nodes){
                if(node != null){
                    flowData.getNodes().add(node);
                }
            }
        }
        return this;
    }

    @Override
    public FlowBuilder addEdge(Edge... edges) {
        if(ArrayUtil.isNotEmpty(edges)){
            for(Edge edge : edges){
                if(edge != null) {
                    flowData.getEdges().add(edge);
                }
            }
        }
        return this;
    }

    @Override
    public FlowBuilder format(boolean format) {
        flowData.setFormat(format);
        return this;
    }

    @Override
    public String build() {
        return IvyGsonUtil.getGson(flowData.getFormat()).toJson(flowData);
    }

    @Override
    public FlowData getFlowData() {
        return flowData;
    }

    @Override
    public List<Node> getNodes() {
        return flowData.getNodes();
    }

    @Override
    public List<Edge> getEdges() {
        return flowData.getEdges();
    }

    @Override
    public Node getNodeByCmpId(String id) {
        assert id != null;
        return getNodes().stream().filter(m->id.equals(m.getData().getId())).findFirst().orElse(null);
    }
    @Override
    public Node getNodeById(String id) {
        assert id != null;
        return getNodes().stream().filter(m->id.equals(m.getId())).findFirst().orElse(null);
    }

    public static Node buildNode(CmpProperty property){
        String nodeId = property.getId();
        String type = property.getType();

        Node node = new Node();
        node.setId(UUID.randomUUID().toString());
        node.setType("common");
        node.setLabel(nodeId);
        node.setPosition(new Position(0d,0d));

        NodeData nodeData = new NodeData();
        nodeData.setId(nodeId);
        nodeData.setName(nodeId);
        nodeData.setType("common");
        nodeData.setMode("default");
        nodeData.setNodeDataBase(new NodeDataBase());
        nodeData.setNodeDataSwitch(new NodeDataSwitch());
        nodeData.setNodeDataIf(new NodeDataIf());

        NodeStyle nodeStyle = new NodeStyle();
        nodeStyle.setToolbar(new NodeStyleToolbar("top",false,10));
        List<NodeStyleHandles> handles = new ArrayList<>();
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"left",1d,"target",""));
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"right",1d,"source",""));
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"top",0d,"target",""));
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"bottom",0d,"source",""));
        nodeStyle.setHandles(handles);
        nodeStyle.setExtendHandles(new ArrayList<>());

        nodeData.setStyle(nodeStyle);

        node.setData(nodeData);
        node.setParentNode(null);
        node.setInitialized(false);

        property.setNode(node);
        return node;
    }

}
