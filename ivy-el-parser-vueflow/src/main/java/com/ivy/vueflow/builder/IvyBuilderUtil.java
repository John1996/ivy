package com.ivy.vueflow.builder;

import com.ivy.vueflow.builder.edge.EdgeBuilder;
import com.ivy.vueflow.builder.edge.VueFlowEdgeBuilder;
import com.ivy.vueflow.builder.flow.FlowBuilder;
import com.ivy.vueflow.builder.flow.VueFlowBuilder;
import com.ivy.vueflow.builder.node.NodeBuilder;
import com.ivy.vueflow.builder.node.VueFlowNodeBuilder;
import com.ivy.vueflow.parser.entity.node.Node;
import com.ivy.vueflow.parser.enums.IvyEnums;

public class IvyBuilderUtil {

    public static FlowBuilder buildFlow(){
        return new VueFlowBuilder().defaultData();
    }

    public static FlowBuilder buildFormatFlow(){
        return new VueFlowBuilder().defaultData().format(true);
    }

    public static EdgeBuilder buildEdge(Node sourceNode, Node targetNode){
        return new VueFlowEdgeBuilder().createEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.common_path);
    }

    public static EdgeBuilder buildCommonEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.common_path);
    }
    public static EdgeBuilder buildToEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.to_path);
    }
    public static EdgeBuilder buildDefaultEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.default_path);
    }
    public static EdgeBuilder buildBooleanEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.boolean_path);
    }
    public static EdgeBuilder buildTrueEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.true_path);
    }
    public static EdgeBuilder buildFalseEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.false_path);
    }
    public static EdgeBuilder buildDoEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.do_path);
    }
    public static EdgeBuilder buildBreakEdge(Node sourceNode, Node targetNode){
        return buildEdge(sourceNode, targetNode).edgeType(IvyEnums.PATH_ENUM.break_path);
    }


    public static NodeBuilder buildNode(IvyEnums.NodeEnum nodeEnum){ return new VueFlowNodeBuilder().createNode(nodeEnum); }
    public static NodeBuilder buildNode(Node node){ return new VueFlowNodeBuilder(node); }

    public static NodeBuilder buildCommonNode(){ return buildNode(IvyEnums.NodeEnum.COMMON); }
    public static NodeBuilder buildSwitchNode(){ return buildNode(IvyEnums.NodeEnum.SWITCH); }
    public static NodeBuilder buildIfNode(){ return buildNode(IvyEnums.NodeEnum.IF); }
    public static NodeBuilder buildBooleanNode(){ return buildNode(IvyEnums.NodeEnum.BOOLEAN); }
    public static NodeBuilder buildWhileNode(){ return buildNode(IvyEnums.NodeEnum.WHILE); }
    public static NodeBuilder buildForNode(){ return buildNode(IvyEnums.NodeEnum.FOR); }
    public static NodeBuilder buildIteratorNode(){ return buildNode(IvyEnums.NodeEnum.ITERATOR); }
    public static NodeBuilder buildScriptNode(){ return buildNode(IvyEnums.NodeEnum.SCRIPT); }
    public static NodeBuilder buildSwitchScriptNode(){ return buildNode(IvyEnums.NodeEnum.SWITCH_SCRIPT); }
    public static NodeBuilder buildIfScriptNode(){ return buildNode(IvyEnums.NodeEnum.IF_SCRIPT); }
    public static NodeBuilder buildBooleanScriptNode(){ return buildNode(IvyEnums.NodeEnum.BOOLEAN_SCRIPT); }
    public static NodeBuilder buildWhileScriptNode(){ return buildNode(IvyEnums.NodeEnum.WHILE_SCRIPT); }
    public static NodeBuilder buildForScriptNode(){ return buildNode(IvyEnums.NodeEnum.FOR_SCRIPT); }
    public static NodeBuilder buildIteratorScriptNode(){ return buildNode(IvyEnums.NodeEnum.ITERATOR_SCRIPT); }
    public static NodeBuilder buildChainNode(){ return buildNode(IvyEnums.NodeEnum.CHAIN); }
    public static NodeBuilder buildContextNode(){ return buildNode(IvyEnums.NodeEnum.CONTEXT); }
    public static NodeBuilder buildRouterNode(){ return buildNode(IvyEnums.NodeEnum.ROUTER); }
    public static NodeBuilder buildSubFlowNode(){ return buildNode(IvyEnums.NodeEnum.SUB_FLOW); }
    public static NodeBuilder buildSubVarNode(){ return buildNode(IvyEnums.NodeEnum.SUB_VAR); }
    public static NodeBuilder buildThenNode(){ return buildNode(IvyEnums.NodeEnum.THEN); }
    public static NodeBuilder buildWhenNode(){ return buildNode(IvyEnums.NodeEnum.WHEN); }
    public static NodeBuilder buildNoteNode(){ return buildNode(IvyEnums.NodeEnum.NOTE); }
    public static NodeBuilder buildAndNode(){ return buildNode(IvyEnums.NodeEnum.AND); }
    public static NodeBuilder buildOrNode(){ return buildNode(IvyEnums.NodeEnum.OR); }
    public static NodeBuilder buildNotNode(){ return buildNode(IvyEnums.NodeEnum.NOT); }

}
