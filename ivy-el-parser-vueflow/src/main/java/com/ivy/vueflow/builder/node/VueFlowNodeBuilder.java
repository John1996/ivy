package com.ivy.vueflow.builder.node;

import com.ivy.vueflow.parser.entity.node.*;
import com.ivy.vueflow.parser.entity.style.NodeStyle;
import com.ivy.vueflow.parser.entity.style.NodeStyleHandles;
import com.ivy.vueflow.parser.entity.style.NodeStyleToolbar;
import com.ivy.vueflow.parser.enums.IvyEnums;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VueFlowNodeBuilder implements NodeBuilder {

    private Node node = new Node();

    public VueFlowNodeBuilder() {
    }

    public VueFlowNodeBuilder(Node node) {
        this.node = node;
    }

    @Override
    public NodeBuilder initData(IvyEnums.NodeEnum nodeEnum) {
        initData(nodeEnum.getType(), nodeEnum.getSubType());
        return this;
    }

    @Override
    public NodeBuilder initData(String type, String subType) {
        node.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        node.setType(type);

        node.setPosition(new Position(0d,0d));

        NodeData nodeData = new NodeData();
        nodeData.setType(subType);
        nodeData.setMode("default");
        NodeDataBase dataBase = new NodeDataBase();
        dataBase.setClassType(1);
        dataBase.setRetryExceptions(new String[]{});
        nodeData.setNodeDataBase(dataBase);

        //样式设置
        NodeStyle nodeStyle = new NodeStyle();
        nodeStyle.setToolbar(new NodeStyleToolbar("top",false,10));
        List<NodeStyleHandles> handles = new ArrayList<>();
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"left",1d,"target",""));
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"right",1d,"source",""));
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"top",0d,"target",""));
        handles.add(new NodeStyleHandles(UUID.randomUUID().toString(),"bottom",0d,"source",""));
        nodeStyle.setHandles(handles);
        nodeStyle.setExtendHandles(new ArrayList<>());
        nodeData.setStyle(nodeStyle);

        node.setData(nodeData);
        return this;
    }

    @Override
    public NodeBuilder createNode(IvyEnums.NodeEnum nodeEnum) {
        this.initData(nodeEnum);
        return this;
    }

    @Override
    public NodeBuilder id(String id) {
        node.getData().setId(id);
        return this;
    }

    @Override
    public NodeBuilder name(String name) {
        node.getData().setName(name);
        node.setLabel(name);
        return this;
    }

    @Override
    public NodeBuilder baseData(NodeDataBase data) {
        node.getData().setNodeDataBase(data);
        return this;
    }

    @Override
    public NodeBuilder chainData(NodeDataChain data) {
        node.getData().setNodeDataChain(data);
        return this;
    }

    @Override
    public NodeBuilder contextData(NodeDataContext data) {
        node.getData().setNodeDataContext(data);
        return this;
    }

    @Override
    public NodeBuilder ifData(NodeDataIf data) {
        node.getData().setNodeDataIf(data);
        return this;
    }

    @Override
    public NodeBuilder loopData(NodeDataLoop data) {
        node.getData().setNodeDataLoop(data);
        return this;
    }

    @Override
    public NodeBuilder routerData(NodeDataRouter data) {
        node.getData().setNodeDataRouter(data);
        return this;
    }

    @Override
    public NodeBuilder subFlowData(NodeDataSubFlow data) {
        node.getData().setNodeDataSubflow(data);
        return this;
    }

    @Override
    public NodeBuilder subVarData(NodeDataSubVar data) {
        node.getData().setNodeDataSubvar(data);
        return this;
    }

    @Override
    public NodeBuilder switchData(NodeDataSwitch data) {
        node.getData().setNodeDataSwitch(data);
        return this;
    }

    @Override
    public NodeBuilder thenData(NodeDataThen data) {
        node.getData().setNodeDataThen(data);
        return this;
    }

    @Override
    public NodeBuilder whenData(NodeDataWhen data) {
        node.getData().setNodeDataWhen(data);
        return this;
    }


    @Override
    public Node build() {
        return node;
    }
}
