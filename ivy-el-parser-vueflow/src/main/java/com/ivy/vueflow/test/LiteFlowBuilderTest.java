package com.ivy.vueflow.test;

import com.ivy.vueflow.builder.IvyBuilderUtil;
import com.ivy.vueflow.builder.IvyParserUtil;
import com.ivy.vueflow.builder.flow.FlowBuilder;
import com.ivy.vueflow.parser.entity.FlowData;
import com.ivy.vueflow.parser.entity.edge.Edge;
import com.ivy.vueflow.parser.entity.edge.EdgeData;
import com.ivy.vueflow.parser.entity.node.Node;
import com.ivy.vueflow.parser.enums.IvyEnums;
import com.ivy.vueflow.parser.execption.LiteFlowELException;
import com.ivy.vueflow.parser.graph.Graph;
import com.ivy.vueflow.parser.graph.GraphInfo;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

public class LiteFlowBuilderTest {

//    public static void main(String[] args) throws LiteFlowELException {
//        init();
//    }

    public static void init() throws LiteFlowELException {
        FlowBuilder builder = IvyBuilderUtil.buildFlow().format(true);

        Node a = IvyBuilderUtil.buildCommonNode().id("a").name("组件a").build();
        Node b = IvyBuilderUtil.buildNode(IvyEnums.NodeEnum.COMMON).id("b").name("组件b").build();
        Edge e = IvyBuilderUtil.buildCommonEdge(a,b).label("普通路径")
                .data(EdgeData.builder().id(null).tag(null).build()).build();

        builder.addNode(a);
        builder.addNode(b);
        builder.addEdge(e);

        //获取流程解析相关信息
        GraphInfo graphInfo1 = IvyParserUtil.parserFlow(builder);
        GraphInfo graphInfo2 = IvyParserUtil.parserFlow(builder.build());
        GraphInfo graphInfo3 = IvyParserUtil.parserFlow(builder.getFlowData());

        //输出EL表达式
        System.out.println(graphInfo1.toString());
        System.out.println(graphInfo2.toString());
        System.out.println(graphInfo3.toString());
    }

}
