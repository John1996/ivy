package com.ivy.vueflow.test;

import com.ivy.vueflow.convert.IvyConvertUtil;
import com.ivy.vueflow.convert.flow.FlowConvert;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class FlowConvertTest implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        FlowConvert convert = IvyConvertUtil.convert();

        //串行编排
//        convert.el2Json("THEN(a, b, c, d);");
//        convert.el2Json("THEN(a, b, THEN(c, d), e);");
//        convert.el2Json("SER(a, b, c, d);");

        //并行编排
//        convert.el2Json("WHEN(a, b, c);");
//        convert.el2Json("PAR(a, b, c);");

        //嵌套编排
//        convert.el2Json("THEN(a, WHEN(b, c, d), e);");
//        convert.el2Json("THEN(a, WHEN(b, THEN(c, d)), e);");

//        convert.el2Json("THEN(a, WHEN(b, WHEN(c, d)), e);");


        //忽略错误
//        convert.el2Json("WHEN(b, c, d).ignoreError(true);");
//        convert.el2Json("THEN(a, WHEN(b, c, d).ignoreError(true), e);");

        //任一节点先执行完则忽略其他
//        convert.el2Json("THEN(a, WHEN(b, THEN(c, d), e).any(true), f);");

        //指定任意节点先执行完则忽略其他
//        convert.el2Json("THEN(a, WHEN(b, c, d).must(b, c), f);");
//        convert.el2Json("THEN(a, WHEN(b, THEN(c, d).id(\"t1\"), e).must(b, \"t1\"), f);");

        //并行编排分组
//        convert.el2Json("THEN(WHEN(a, b, c, d));");
//        convert.el2Json("THEN(WHEN(a, b),WHEN(c, d));");

    }

}
