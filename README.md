演示地址：http://118.25.104.139/

使用文档：https://gitee.com/yunzy/ivy/wikis/Home

LogicFlow版本后端项目：https://gitee.com/yunzy/ivy-logicflow

LogicFlow版本前端项目：https://gitee.com/yunzy/ivy-web-app

## 项目结构
```
ivy
 |-- db
   |-- ivy_init_all.sql     初始化SQL只需要执行这一个即可
 |-- docker                 使用Docker部署项目
 |-- ivy-core               核心包
 |-- ivy-el-parser-vueflow  EL表达式构建器、解析器模块
 |-- ivy-ui-vueflow         前端VueFlow项目
 |-- ivy-web                WEB服务
```

## 流程数据构建 & 流程数据解析
### 引入依赖
```xml
<dependency>
    <groupId>com.ming</groupId>
    <artifactId>ivy-el-parser-vueflow</artifactId>
    <version>1.0.0</version>
</dependency>
```

### 构建节点
```java
Node a = IvyBuilderUtil.buildCommonNode().id("a").name("组件a").build();
Node b = IvyBuilderUtil.buildNode(IvyEnums.NodeEnum.COMMON).id("b").name("组件b").build();
```

### 构建路径
```java
Edge e = IvyBuilderUtil.buildCommonEdge(a,b).label("普通路径")
        .data(EdgeData.builder().id(null).tag(null).build()).build();
```

### 构建流程
```java
FlowBuilder builder = IvyBuilderUtil.buildFlow().format(true);
builder.addNode(a);
builder.addNode(b);
builder.addEdge(e);
```

### 解析流程

```java
//获取流程解析相关信息
GraphInfo graphInfo1 = IvyParserUtil.parserFlow(builder);
GraphInfo graphInfo2 = IvyParserUtil.parserFlow(builder.build());
GraphInfo graphInfo3 = IvyParserUtil.parserFlow(builder.getFlowData());

//输出EL表达式
System.out.println(graphInfo1.toString());
System.out.println(graphInfo2.toString());
System.out.println(graphInfo3.toString());
```

## EL转JSON

已支持以下EL表达式转换：
```java
//转换器
FlowConvert convert = IvyConvertUtil.convert();

//串行编排
convert.el2Json("THEN(a, b, c, d);");
convert.el2Json("THEN(a, b, THEN(c, d), e);");
convert.el2Json("SER(a, b, c, d);");

//并行编排
convert.el2Json("WHEN(a, b, c);");
convert.el2Json("PAR(a, b, c);");

//嵌套编排
convert.el2Json("THEN(a, WHEN(b, c, d), e);");
convert.el2Json("THEN(a, WHEN(b, THEN(c, d)), e);");
```

## 问题反馈
ivy交流Q群：908863074

