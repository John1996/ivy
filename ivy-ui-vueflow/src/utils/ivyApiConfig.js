export const API_URLS = {
  resultMsg: {
    msgVisible: false,
    msgContent: '',
    msgColor: 'error',
  },

  

  // 字典表
  ivyDictSelectByCode: '/ivy/dict/selectByCode',
  ivyDictInsertByCode: '/ivy/dict/insertByCode',
  ivyDictUpdateByCode: '/ivy/dict/updateByCode',
  ivyDictSaveByCode: '/ivy/dict/saveByCode',

  // 连接数据库
  sqlManagerOptions: '/db/sql/manager/options',
  dbTableData: '/db/table/data',
  dbTables: '/db/tables',

  // 动态数据源管理
  dbDatasourceOptions: '/db/datasource/options',
  dbDatasourcePage: '/db/datasource/page',
  dbDatasourceDelete: '/db/datasource/delete',
  dbDatasourceAdd: '/db/datasource/add',
  dbDatasourceUpdate: '/db/datasource/update',

  // sql管理器
  dbSqlManagerOption: '/db/sql/manager/option',
  dbSqlManagerPage: '/db/sql/manager/page',
  dbSqlManagerDelete: '/db/sql/manager/delete',
  dbSqlManagerAdd: '/db/sql/manager/add',
  dbSqlManagerUpdate: '/db/sql/manager/update',
  dbSqlManagerExec: '/db/sql/manager/exec',

  // 动态sql管理
  dbSqlOption: '/db/sql/option',
  dbSqlPage: '/db/sql/page',
  dbSqlDelete: '/db/sql/delete',
  dbSqlAdd: '/db/sql/add',
  dbSqlUpdate: '/db/sql/update',
  dbSqlQueryTypeOption: '/db/sql/queryTypeOption',
  dbSqlColumnList: '/db/sql/column/list',
  dbSqlBuild: '/db/sql/build',

  // 动态类管理
  lfDynamicClassPage: '/liteflow/dynamic/class/page',
  lfDynamicClassDelete: '/liteflow/dynamic/class/delete',
  lfDynamicClassAdd: '/liteflow/dynamic/class/add',
  lfDynamicClassUpdate: '/liteflow/dynamic/class/update',
  lfDynamicClassUpdateSourceCode: '/liteflow/dynamic/class/updateSourceCode',
  // lfDynamicClassExec: '/liteflow/dynamic/class/exec',
  lfDynamicClassExecCommon: '/liteflow/dynamic/class/exec/common',
  lfDynamicClassExecCmp: '/liteflow/dynamic/class/exec/cmp',
  lfDynamicClassOption: '/liteflow/dynamic/class/option',
  lfDynamicClassOptions: '/liteflow/dynamic/class/options',
  // lfDynamicClassCodeTemplate: '/liteflow/dynamic/class/codeTemplate',
  lfDynamicClassAutoCreatedData: '/liteflow/dynamic/class/autoCreatedData',

  // 动态类模板
  lfDynamicClassTemplateCodeTemplate: '/liteflow/dynamic/class/template/codeTemplate',
  lfDynamicClassTemplateSave: '/liteflow/dynamic/class/template/save',
  lfDynamicClassTemplateList: '/liteflow/dynamic/class/template/list',
  lfDynamicClassTemplateDelete: '/liteflow/dynamic/class/template/delete',
  lfDynamicClassTemplateOption: '/liteflow/dynamic/class/template/option',

  // 组件模板
  lfCmpStatistics: '/liteflow/cmp/statistics',
  lfCmpTypeOptions: '/liteflow/cmp/type/options',
  lfCmpScriptOptions: '/liteflow/cmp/script/options',
  lfCmpPage: '/liteflow/cmp/page',
  lfCmpList: '/liteflow/cmp/list',
  lfCmpDelete: '/liteflow/cmp/delete',
  lfCmpAdd: '/liteflow/cmp/add',
  lfCmpUpdate: '/liteflow/cmp/update',
  lfCmpExec: '/liteflow/cmp/exec',
  lfCmpOptions: '/liteflow/cmp/options',
  lfCmpCmpOptions: '/liteflow/cmp/cmpOptions',
  lfCmpFallbackList: '/liteflow/cmp/fallback/list',

  // 流程编排 - 配置项管理
  lfConfigPage: '/liteflow/config/page',
  lfConfigSelectById: '/liteflow/config/selectById',
  lfConfigDelete: '/liteflow/config/delete',
  lfConfigAdd: '/liteflow/config/add',
  lfConfigUpdate: '/liteflow/config/update',
  lfConfigOption: '/liteflow/config/option',
  lfConfigOptions: '/liteflow/config/options',
  lfConfigExec: '/liteflow/config/exec',

  // 流程编排 - 执行器管理
  lfExecutorPage: '/liteflow/executor/page',
  lfExecutorDelete: '/liteflow/executor/delete',
  lfExecutorAdd: '/liteflow/executor/add',
  lfExecutorUpdate: '/liteflow/executor/update',
  lfExecutorOption: '/liteflow/executor/option',
  lfExecutorOptions: '/liteflow/executor/options',

  // 流程编排 - 表达式管理
  lfElPage: '/liteflow/el/page',
  lfElDelete: '/liteflow/el/delete',
  lfElAdd: '/liteflow/el/add',
  lfElUpdate: '/liteflow/el/update',
  lfElOptionEl: '/liteflow/el/optionEl',
  lfElValidate: '/liteflow/el/validate',
  lfElBuild: '/liteflow/el/build/new',
  lfElExec: '/liteflow/el/exec/new',
  lfElOptions: '/liteflow/el/options',

  // 调试
  lfDebugDebug: '/liteflow/debug/debug',
  lfDebugStatus: '/liteflow/debug/status',
  lfDebugLog: '/liteflow/debug/log',
  lfDebugStartingNodes: '/liteflow/debug/startingNodes',

  // 流程编排 - 链路管理
  lfChainPage: '/liteflow/chain/page',
  lfChainDelete: '/liteflow/chain/delete',
  lfChainAdd: '/liteflow/chain/add',
  lfChainUpdate: '/liteflow/chain/update',
  lfChainOptions: '/liteflow/chain/options',
  lfChainSync: '/liteflow/chain/sync',

  // 任务调度 - 报表
  xxljobReportDashboardInfo: '/xxljob/report/dashboardInfo',
  xxljobReportChartInfo: '/xxljob/report/chartInfo',

  // 任务调度 - 日志
  xxljobLogPage: '/xxljob/log/page',
  xxljobLogDelete: '/xxljob/log/delete',
  xxljobLogAdd: '/xxljob/log/add',
  xxljobLogUpdate: '/xxljob/log/update',
  xxljobLogClear: '/xxljob/log/clear',
  xxljobLogKill: '/xxljob/log/kill',
  xxljobLogLogDetailCat: '/xxljob/log/logDetailCat',

  // 任务调度 - 执行器
  xxljobGroupOptions: '/xxljob/group/options',
  xxljobGroupPage: '/xxljob/group/page',
  xxljobGroupDelete: '/xxljob/group/delete',
  xxljobGroupAdd: '/xxljob/group/add',
  xxljobGroupUpdate: '/xxljob/group/update',

  // 任务调度 - 任务管理
  xxljobTaskOptions: '/xxljob/task/options',
  xxljobTaskPage: '/xxljob/task/page',
  xxljobTaskDelete: '/xxljob/task/delete',
  xxljobTaskAdd: '/xxljob/task/add',
  xxljobTaskUpdate: '/xxljob/task/update',
  xxljobTaskOption: '/xxljob/task/option',
  xxljobTaskStart: '/xxljob/task/start',
  xxljobTaskStop: '/xxljob/task/stop',
  xxljobTaskCodeInit: '/xxljob/task/code/init',
  xxljobTaskCodeRefresh: '/xxljob/task/code/refresh',
  xxljobTaskCode: '/xxljob/task/code',
  xxljobTaskTrigger: '/xxljob/task/trigger',
  xxljobTaskNextTriggerTime: '/xxljob/task/nextTriggerTime',
}

// API_URLS
