
const createShowMsg = () => {

  const createMsg = async (res = {},snackbar = {}) => {
    if(res){
      if(res.code == 200){
        snackbar.msgVisible = true
        snackbar.msgColor = "success"
        snackbar.msgContent = res.message
      }else{
        snackbar.msgVisible = true
        snackbar.msgColor = "error"
        snackbar.msgContent = res.message
      }
    }
  }

  return {
    show: (data = {},snackbar = {}) => createMsg(data,snackbar),
  }
}

export const $resultMsg = createShowMsg()
