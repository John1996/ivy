import App from '@/App.vue'
// Styles
import '@core/scss/template/index.scss'
import { registerPlugins } from '@core/utils/plugins'
import '@styles/styles.scss'
import 'material-icons/iconfont/material-icons.css'
import 'tailwindcss/tailwind.css'
import { createApp } from 'vue'
import { plugin as Slicksort } from 'vue-slicksort'

import Vuesax from 'vuesax-alpha'
import 'vuesax-alpha/dist/index.css'

import './components/styles/vuesax.styl'
import vsCollapse from './components/vsCollapse'
import vsDivider from './components/vsDivider'
import vsIcon from './components/vsIcon'
import vsImages from './components/vsImages'
import vsTabs from './components/vsTabs'
import vsTextarea from './components/vsTextarea'

// main.ts
import * as VuesaxAlphaIconsVue from '@vuesax-alpha/icons-vue'



import editorWorker from 'monaco-editor/esm/vs/editor/editor.worker?worker'
import cssWorker from 'monaco-editor/esm/vs/language/css/css.worker?worker'
import htmlWorker from 'monaco-editor/esm/vs/language/html/html.worker?worker'
import jsonWorker from 'monaco-editor/esm/vs/language/json/json.worker?worker'
import tsWorker from 'monaco-editor/esm/vs/language/typescript/ts.worker?worker'

self.MonacoEnvironment = {
  getWorker(_, label) {
    if (label === 'json') {
      return new jsonWorker();
    }
    if (label === 'css' || label === 'scss' || label === 'less') {
      return new cssWorker();
    }
    if (label === 'html' || label === 'handlebars' || label === 'razor') {
      return new htmlWorker();
    }
    if (label === 'typescript' || label === 'javascript') {
      return new tsWorker();
    }
    return new editorWorker();
  },
};

// Create vue app
const app = createApp(App)

app.use(Vuesax)
app.use(Slicksort)
app.use(vsIcon)
app.use(vsCollapse)
app.use(vsTextarea)
app.use(vsDivider)
app.use(vsTabs)
app.use(vsImages)

for (const [key, component] of Object.entries(VuesaxAlphaIconsVue)) {
    app.component(`VsIcon${key}`, component)
}

// Register plugins
registerPlugins(app)

// Mount vue app
app.mount('#app')
