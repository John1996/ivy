import { MarkerType, useVueFlow } from '@vue-flow/core';

const instance = useVueFlow()



//节点初始化参数
function initEdge(sourceId, targetId) {
    const sourceNode = instance.findNode(sourceId)
    let data = {
        source: sourceId,
        target: targetId,
        sourceHandle: null,
        targetHandle: null,
        animated: true,
        id: uuid(),
        type: 'animation',
        label: '',
        updatable: true,
        showToolbar: false,
        markerEnd: MarkerType.ArrowClosed,
        data: {type: 'common'}
    }
    if(sourceNode.type == 'note'){
        data.data = {type: 'note'}
        data.label = '注释'
    }
    return data;
}

function initEdges(sourceId, targetId){
    return [initEdge(sourceId, targetId)]
}

function uuid() {
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0,
          v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

export { initEdge, initEdges };

