import dagre from '@dagrejs/dagre'
import { Position, useVueFlow } from '@vue-flow/core'
import { ref } from 'vue'

/**
 * Composable to run the layout algorithm on the graph.
 * It uses the `dagre` library to calculate the layout of the nodes and edges.
 */
export function useLayout() {
  const { findNode } = useVueFlow()

  const graph = ref(new dagre.graphlib.Graph())

  const previousDirection = ref('LR')

  function layout(nodes, edges, direction) {
    // we create a new graph instance, in case some nodes/edges were removed, otherwise dagre would act as if they were still there
    const dagreGraph = new dagre.graphlib.Graph()

    graph.value = dagreGraph

    dagreGraph.setDefaultEdgeLabel(() => ({}))

    const isHorizontal = direction === 'LR'

    /*

        对象	属性	       默认值	           描述
        graph	rankdir	    TB	              排位节点的方向。可以是 TB、BT、LR 或 RL，其中 T = 上，B = 下，L = 左，R = 右。
        graph	align	      undefined	        排位节点的对齐方式。可以是 UL、UR、DL 或 DR，其中 U = 上，D = 下，L = 左，R = 右。
        graph	nodesep	    50	              布局中节点水平间隔的像素数。
        graph	edgesep	    10	              布局中边水平间隔的像素数。
        graph	ranksep	    50	              布局中每个级别之间的像素数。
        graph	marginx	    0	                图的左右边距的像素数。
        graph	marginy	    0	                图的上下边距的像素数。
        graph	acyclicer	  undefined	        如果设置为 greedy，则使用贪心启发式方法来寻找图的反馈弧集。反馈弧集是一组可以移除以使图变为无环的边。
        graph	ranker	    network-simplex	  分配输入图中每个节点级别的算法类型。可能的值有：network-simplex、tight-tree 或 longest-path。
        node	width	      0	                节点的宽度（像素）。
        node	height	    0	                节点的高度（像素）。
        edge	minlen	    1	                边的源和目标之间保持的级别数。
        edge	weight	    1	                分配给边的权重。较高权重的边通常比低权重的边更短且更直。
        edge	width	      0	                边标签的宽度（像素）。
        edge	height	    0	                边标签的高度（像素）。
        edge	labelpos	  r	                标签相对于边的位置。l = 左，c = 中，r = 右。
        edge	labeloffset	10	              标签远离边移动的像素数。仅当 labelpos 为 l 或 r 时适用。
    */

    dagreGraph.setGraph({ 
      rankdir: direction,
      nodesep: 150, // 设置节点间的水平间距
      ranksep: 150  // 设置节点间的垂直间距 
    })

    previousDirection.value = direction

    for (const node of nodes) {
      // if you need width+height of nodes for your layout, you can use the dimensions property of the internal node (`GraphNode` type)
      const graphNode = findNode(node.id)

      dagreGraph.setNode(node.id, { width: graphNode.dimensions.width || 150, height: graphNode.dimensions.height || 50 })
    }

    for (const edge of edges) {
      dagreGraph.setEdge(edge.source, edge.target)
    }

    dagre.layout(dagreGraph)

    // set nodes with updated positions
    return nodes.map((node) => {
      const nodeWithPosition = dagreGraph.node(node.id)

      return {
        ...node,
        targetPosition: isHorizontal ? Position.Left : Position.Top,
        sourcePosition: isHorizontal ? Position.Right : Position.Bottom,
        position: { x: nodeWithPosition.x, y: nodeWithPosition.y },
      }
    })
  }

  function layout0(nodes, edges) {
    // we create a new graph instance, in case some nodes/edges were removed, otherwise dagre would act as if they were still there
    const dagreGraph = new dagre.graphlib.Graph()

    graph.value = dagreGraph

    dagreGraph.setDefaultEdgeLabel(() => ({}))

    const direction = 'LR'
    const isHorizontal = direction === 'LR'

    dagreGraph.setGraph({ 
      rankdir: direction,
      nodesep: 150, // 设置节点间的水平间距
      ranksep: 150  // 设置节点间的垂直间距 
    })

    previousDirection.value = direction

    for (const node of nodes) {
      // if you need width+height of nodes for your layout, you can use the dimensions property of the internal node (`GraphNode` type)
      const graphNode = findNode(node.id)

      dagreGraph.setNode(node.id, { width: graphNode.dimensions.width || 150, height: graphNode.dimensions.height || 50 })
    }

    for (const edge of edges) {
      dagreGraph.setEdge(edge.source, edge.target)
    }

    dagre.layout(dagreGraph)

    return nodes.map((node) => {
      const nodeWithPosition = dagreGraph.node(node.id)

      return {
        ...node,
        // targetPosition: isHorizontal ? Position.Left : Position.Top,
        // sourcePosition: isHorizontal ? Position.Right : Position.Bottom,
        // position: { x: nodeWithPosition.x, y: nodeWithPosition.y },
      }
    })
  }

  return { graph, layout, layout0, previousDirection }
}
