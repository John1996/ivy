//节点初始化参数
function initNodeProp(type,name,id) {
    if(type == 'common' || type == 'boolean' || type == 'switch' || type == 'if' || type == 'for' || type == 'while' || type == 'iterator' || type == 'chain'){
        let data = {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataBase: {
                classType: 1,
            },
            style: {
                toolbar: {
                    // isVisible: false,
                    position: 'top',
                    showIcon: false,
                    offset: 10
                },
                handles: [
                    {position: 'left', opacity: 1, type: 'target', style: ''},
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                    {position: 'top', opacity: 0, type: 'target', style: ''},
                    {position: 'bottom', opacity: 0, type: 'source', style: ''},
                ],
                extendHandles: []
            },
        }
        if(type == 'boolean'){
            data.nodeDataBoolean = {};
        }else if(type == 'switch'){
            data.nodeDataSwitch = {};
        }else if(type == 'if'){
            data.nodeDataIf = {};
        }else if(type == 'for'){
            data.nodeDataLoop = {};
        }else if(type == 'iterator'){
            data.nodeDataLoop = {};
        }else if(type == 'while'){
            data.nodeDataLoop = {};
        }
        return data;
    }else if(type == 'script' || type == 'boolean_script' || type == 'switch_script' || type == 'if_script' || type == 'for_script' || type == 'while_script' || type == 'iterator_script'){
        let data = {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataBase: {},
            style: {
                toolbar: {
                    // isVisible: false,
                    position: 'top',
                    showIcon: false,
                    offset: 10
                },
                handles: [
                    {position: 'left', opacity: 1, type: 'target', style: ''},
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                    {position: 'top', opacity: 0, type: 'target', style: ''},
                    {position: 'bottom', opacity: 0, type: 'source', style: ''},
                ],
                extendHandles: []
            },
        }
        if(type == 'boolean_script'){
            data.nodeDataBoolean = {};
        }else if(type == 'switch_script'){
            data.nodeDataSwitch = {};
        }else if(type == 'if_script'){
            data.nodeDataIf = {};
        }else if(type == 'for_script'){
            data.nodeDataLoop = {};
        }else if(type == 'iterator_script'){
            data.nodeDataLoop = {};
        }else if(type == 'while_script'){
            data.nodeDataLoop = {};
        }
        return data;
    }else if(type == 'when'){
        return {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataWhen: {},
            style: {
                handles: [
                    {position: 'left', opacity: 1, type: 'target', style: ''},
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                ],
            }
        }
    }else if(type == 'then'){
        return {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataThen: {},
            style: {
                handles: [
                    {position: 'left', opacity: 1, type: 'target', style: ''},
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                ],
            }
        }
    }else if(type == 'subvar'){
        return {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataSubvar: {},
            style: {
                handles: [
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                ],
            }
        }
    }else if(type == 'subflow'){
        return {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataSubflow: {},
            style: {
                handles: [
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                ],
            }
        }
    }else if(type == 'context'){
        return {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataContext: {},
            style: {
                handles: [],
            }
        }
    }else if(type == 'router'){
        return {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataRouter: {},
            style: {
                handles: [
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                ],
            }
        }
    }else if(type == 'note'){
        return {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            nodeDataNote: {
                label: '',
                fontSize: 16,
                showResizer: false
            },
            style: {
                handles: [
                    {position: 'left', opacity: 0, type: 'source', style: ''},
                    {position: 'right', opacity: 0, type: 'source', style: ''},
                    {position: 'top', opacity: 0, type: 'source', style: ''},
                    {position: 'bottom', opacity: 0, type: 'source', style: ''},
                ],
            }
        }
    }else if(type == 'and' || type == 'or' || type == 'not'){
        let data = {
            id: id,
            type: type,
            name: name,
            mode: 'default', // default，simple
            style: {
                handles: [
                    {position: 'left', opacity: 1, type: 'target', style: ''},
                    {position: 'right', opacity: 1, type: 'source', style: ''},
                ],
            }
        }
        if(type == 'and'){
            data.nodeDataAnd = {};
        }else if(type == 'or'){
            data.nodeDataOr = {};
        }else{
            data.nodeDataNot = {};
        }
        return data;
    }
    return {}
}


function initCommonNodeProp() {
    return initNodeProp('common','普通组件','');
}
function initSwitchNodeProp() {
    return initNodeProp('switch','选择组件','');
}
function initIfNodeProp() {
    return initNodeProp('if','条件组件','');
}
function initBooleanNodeProp() {
    return initNodeProp('boolean','布尔组件','');
}
function initWhileNodeProp() {
    return initNodeProp('while','循环条件组件','');
}
function initForNodeProp() {
    return initNodeProp('for','循环次数组件','');
}
function initIteratorNodeProp() {
    return initNodeProp('iterator','循环迭代组件','');
}

function initWhenNodeProp() {
    return initNodeProp('when','并行','');
}
function initThenNodeProp() {
    return initNodeProp('then','串行','');
}

function initSubvarNodeProp() {
    return initNodeProp('subvar','子变量','');
}

function initSubflowNodeProp() {
    return initNodeProp('subflow','子流程','');
}
function initContextNodeProp() {
    return initNodeProp('context','数据上下文','');
}
function initRouterNodeProp() {
    return initNodeProp('router','策略路由','');
}
function initNoteNodeProp() {
    return initNodeProp('note','注释','');
}
function initScriptNodeProp() {
    return initNodeProp('script','普通脚本组件','');
}
function initBooleanScriptNodeProp() {
    return initNodeProp('boolean_script','布尔脚本组件','');
}
function initForScriptNodeProp() {
    return initNodeProp('for_script','循环次数脚本组件','');
}
function initIfScriptNodeProp() {
    return initNodeProp('if_script','条件脚本组件','');
}
function initIteratorScriptNodeProp() {
    return initNodeProp('iterator_script','循环迭代脚本组件','');
}
function initSwitchScriptNodeProp() {
    return initNodeProp('switch_script','选择脚本组件','');
}
function initWhileScriptNodeProp() {
    return initNodeProp('while_script','循环条件脚本组件','');
}


function initNodePropByType(type) {
    if(type == 'common'){
        return initCommonNodeProp()
    }else if(type == 'switch'){
       return initSwitchNodeProp()
    }else if(type == 'if'){
        return initIfNodeProp()
    }else if(type == 'boolean'){
        return initBooleanNodeProp()
    }else if(type == 'while'){
        return initWhileNodeProp()
    }else if(type == 'for'){
        return initForNodeProp()
    }else if(type == 'iterator'){
        return initIteratorNodeProp()
    }else if(type == 'script'){
        return initScriptNodeProp()
    }else if(type == 'boolean_script'){
        return initBooleanScriptNodeProp()
    }else if(type == 'for_script'){
        return initForScriptNodeProp()
    }else if(type == 'if_script'){
        return initIfScriptNodeProp()
    }else if(type == 'iterator_script'){
        return initIteratorScriptNodeProp()
    }else if(type == 'switch_script'){
        return initSwitchScriptNodeProp()
    }else if(type == 'while_script'){
        return initWhileScriptNodeProp()
    }
    return {};
}




export { initBooleanNodeProp, initCommonNodeProp, initContextNodeProp, initNodeProp, initNodePropByType, initNoteNodeProp, initRouterNodeProp, initSubflowNodeProp, initSubvarNodeProp, initSwitchNodeProp, initThenNodeProp, initWhenNodeProp };

