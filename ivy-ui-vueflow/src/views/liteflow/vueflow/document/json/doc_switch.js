function switchFlow(){
    return {
      "nodes": [
        {
          "id": "e345ce437d864009a8b86b3d66084b1d",
          "type": "switch",
          "initialized": false,
          "position": {
            "x": 68.5,
            "y": 84.484375
          },
          "data": {
            "id": "a",
            "type": "switch",
            "name": "选择组件a",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "c7283e6e89d44265b6f711b0a95ae76d"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "51825acb0b3a4375988e865f495430a4"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "98a12603e561497ab1f301a5aa34ae86"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "1415d2ec23414b318022b4f4ed3ef1f3"
                }
              ],
              "extendHandles": []
            },
            "nodeDataSwitch": {}
          },
          "label": "选择组件"
        },
        {
          "id": "104601a7b16e440b957eb04e57404eb7",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 540.5,
            "y": 79.484375
          },
          "data": {
            "id": "b",
            "type": "common",
            "name": "普通组件b",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "1e5447d2e3e340879818f5ffd4e95ff1"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "24fcd5409d4f41d1a8f143da3ac5f023"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "f65d3fd2ea734f0c9300e582e08b33a8"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "a6a16650cca245ed8010fd9dd20a3306"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "44ded0ea986648c49d0df5d360899774",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 953.5,
            "y": 80.484375
          },
          "data": {
            "id": "c",
            "type": "common",
            "name": "普通组件c",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "0f9b0a851d93495183ea667f8419ef0f"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "e81b675730be4640a94ab3893f96cb6b"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "000683d48b1740dbb949805cc148ce1d"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "0be5b6f9beb24cfdbdb01e50bed85546"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "8964c9d71f8b4619ac7b69034d76bbab",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 550.5,
            "y": -150.515625
          },
          "data": {
            "id": "d",
            "type": "common",
            "name": "普通组件d",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "40bed687760649b6a143f096f9201839"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "12b22d6a5a5c4921af0b9e26c0cd0a53"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "070e585531ed40aea2a9090cedaaa62b"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "43161821bdc546b0a683079460e42814"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "b5b8e70f1bd44d338d064f0d8d32b3c1",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 554.610161000826,
            "y": 316.42104040483974
          },
          "data": {
            "id": "e",
            "type": "common",
            "name": "普通组件e",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "ad291ad6a6264d41a5adc85577460616"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "3da3a25d80384ad68209a6ecc9469e33"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "0577e5b877a746b9abf2afd61a8ac4b1"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "be58329f302f4fd8bf176c9c2c4b7853"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        }
      ],
      "edges": [
        {
          "id": "f8c537b31f794aaa8ba90300adbfa893",
          "type": "animation",
          "source": "104601a7b16e440b957eb04e57404eb7",
          "target": "44ded0ea986648c49d0df5d360899774",
          "sourceHandle": "24fcd5409d4f41d1a8f143da3ac5f023",
          "targetHandle": "0f9b0a851d93495183ea667f8419ef0f",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 846.5,
          "sourceY": 159.984375,
          "targetX": 947.5,
          "targetY": 160.984375
        },
        {
          "id": "7cc89b198e7e4c2fb1f4a58000aa830f",
          "type": "animation",
          "source": "e345ce437d864009a8b86b3d66084b1d",
          "target": "8964c9d71f8b4619ac7b69034d76bbab",
          "sourceHandle": "51825acb0b3a4375988e865f495430a4",
          "targetHandle": "40bed687760649b6a143f096f9201839",
          "updatable": true,
          "data": {
            "type": "to"
          },
          "label": "to",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 374.5,
          "sourceY": 159.984375,
          "targetX": 544.5,
          "targetY": -70.015625
        },
        {
          "id": "7c7e13eb14f04cacabcb20520cc95009",
          "type": "animation",
          "source": "e345ce437d864009a8b86b3d66084b1d",
          "target": "104601a7b16e440b957eb04e57404eb7",
          "sourceHandle": "51825acb0b3a4375988e865f495430a4",
          "targetHandle": "1e5447d2e3e340879818f5ffd4e95ff1",
          "updatable": true,
          "data": {
            "type": "to",
            "id": "bb"
          },
          "label": "to",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": true,
          "sourceX": 374.5,
          "sourceY": 159.984375,
          "targetX": 534.5,
          "targetY": 159.984375
        },
        {
          "id": "3ae59de3ad4f481db02104217860d9f9",
          "type": "animation",
          "source": "e345ce437d864009a8b86b3d66084b1d",
          "target": "b5b8e70f1bd44d338d064f0d8d32b3c1",
          "sourceHandle": "51825acb0b3a4375988e865f495430a4",
          "targetHandle": "ad291ad6a6264d41a5adc85577460616",
          "updatable": true,
          "data": {
            "type": "to",
            "tag": "ee"
          },
          "label": "to",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 374.5,
          "sourceY": 159.984375,
          "targetX": 548.6101185446122,
          "targetY": 396.92103518350376
        }
      ],
      "position": [
        -38.723898802587996,
        134.70104791649794
      ],
      "zoom": 0.8705505632961241,
      "viewport": {
        "x": -38.723898802587996,
        "y": 134.70104791649794,
        "zoom": 0.8705505632961241
      }
    }
}

export { switchFlow };
