function forFlow(){
    return {
        "nodes": [
          {
            "id": "34566d3bf4404521be2c4a115f2dc925",
            "type": "for",
            "initialized": false,
            "position": {
              "x": 117.5,
              "y": 173.5
            },
            "data": {
              "id": "e315eb22ba4242d49bf65bfb741b10e8",
              "type": "for",
              "name": "循环次数组件",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "d1a68a930fed4854856b5455782cae2f"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "20071d1caf4241e79cb0d074f035099b"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "b8dc4bd6755b44c1980a3a530eed2003"
                  },
                  {
                    "position": "bottom",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "32d091b81d7b4034bd507514005815da"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataFor": {}
            },
            "label": "循环次数组件"
          },
          {
            "id": "c5733a6d959149af857d26752ccdc473",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 640.5,
              "y": 69.5
            },
            "data": {
              "id": "a",
              "type": "common",
              "name": "普通组件a",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "15c18808a04844bfafd0c5df2019725f"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "4a04e42aea334056942b9267cffbccc5"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "85328839c381475d9536217f103cb34c"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "8098e1039f694e88b3468251a8003b65"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "64d8f78ab6d3464fb63e2611c4d550e8",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 116.5,
              "y": 473.5
            },
            "data": {
              "id": "c",
              "type": "common",
              "name": "普通组件c",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "e0c605b5861240808984883cd4e0f547"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "d8a5d351c00d4dc2b94c14f50c9c3892"
                  },
                  {
                    "position": "top",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "11b91e807a754e1390f5c2cafdbf364c"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "c5f41978ba1a452d8cd416690c53f9ef"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "a47b39c7f0b5421896163abdef36ced5",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 640.5,
              "y": 303.5
            },
            "data": {
              "id": "b",
              "type": "boolean",
              "name": "布尔组件b",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "2a0ae3825ab34828bdd7c39e1fd24e7c"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "4b9ec012e24049438c1a9b5b352ed721"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "8f3000d1cf12470a9c047453f9923907"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "22911f01ce024ae1a1dd85fbd6e42122"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          }
        ],
        "edges": [
          {
            "id": "e514a19ddec04543b72e8e8915a60160",
            "type": "animation",
            "source": "34566d3bf4404521be2c4a115f2dc925",
            "target": "c5733a6d959149af857d26752ccdc473",
            "sourceHandle": "20071d1caf4241e79cb0d074f035099b",
            "targetHandle": "15c18808a04844bfafd0c5df2019725f",
            "updatable": true,
            "data": {
              "type": "do"
            },
            "label": "do",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 423.5,
            "sourceY": 249,
            "targetX": 634.5,
            "targetY": 150
          },
          {
            "id": "2f87f5bee770461ca5bc530219130d80",
            "type": "animation",
            "source": "34566d3bf4404521be2c4a115f2dc925",
            "target": "a47b39c7f0b5421896163abdef36ced5",
            "sourceHandle": "20071d1caf4241e79cb0d074f035099b",
            "targetHandle": "2a0ae3825ab34828bdd7c39e1fd24e7c",
            "updatable": true,
            "data": {
              "type": "break"
            },
            "label": "break",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 423.5,
            "sourceY": 249,
            "targetX": 634.5,
            "targetY": 379
          },
          {
            "id": "441f5a27b9a04fa194e22e8fb0998586",
            "type": "animation",
            "source": "34566d3bf4404521be2c4a115f2dc925",
            "target": "64d8f78ab6d3464fb63e2611c4d550e8",
            "sourceHandle": "32d091b81d7b4034bd507514005815da",
            "targetHandle": "11b91e807a754e1390f5c2cafdbf364c",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 267.5,
            "sourceY": 330.5,
            "targetX": 266.5,
            "targetY": 467.5
          }
        ],
        "position": [
          -36.609506875139914,
          -10.639015821545627
        ],
        "zoom": 1,
        "viewport": {
          "x": -36.609506875139914,
          "y": -10.639015821545627,
          "zoom": 1
        }
      }
}
export { forFlow };
