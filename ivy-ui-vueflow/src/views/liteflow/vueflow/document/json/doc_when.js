function whenFlow() {
    return {
      "nodes": [
        {
          "id": "595ea2b6209a4308a261da2e5f3cbfb4",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 150,
            "y": 236
          },
          "data": {
            "id": "a",
            "type": "common",
            "name": "a",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "bb3a03bedddf484bae58db676b98c9d9"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "8be6506fb2e942c5b17f9cf89560989a"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "5090008452dc4e798fd00c54f96ed29b"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "1df8424cf88e4cdb92f03cf7b8a1db57"
                }
              ],
              "extendHandles": []
            },
            "isRunning": false,
            "isFinished": false,
            "hasError": false,
            "isSkipped": false,
            "isCancelled": false
          },
          "label": "普通组件",
          "targetPosition": "left",
          "sourcePosition": "right"
        },
        {
          "id": "916883b506cf428ba4ba464f281330d2",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 601.711925222248,
            "y": 139.30883207938876
          },
          "data": {
            "id": "b",
            "type": "common",
            "name": "b",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "848563027b5746ef82d9ae7cc39aeadd"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "415c201287d64be5a1dc226d860568e9"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "15a37838f48a4e3987d3d7f4c24c693e"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "90fcfe8e1bb547d6a570b704dfae7393"
                }
              ],
              "extendHandles": []
            },
            "isRunning": false,
            "isFinished": false,
            "hasError": false,
            "isSkipped": false,
            "isCancelled": false
          },
          "label": "普通组件",
          "targetPosition": "left",
          "sourcePosition": "right"
        },
        {
          "id": "bb47d81e448343e1a55eff1cd1db9eb6",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 603.2276417887585,
            "y": 314.69877777822387
          },
          "data": {
            "id": "c",
            "type": "common",
            "name": "c",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "1ebe2bab7913495d9ecf2a4202c82a45"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "94fe488bb3334959ad0bd08fb4baa908"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "56bb217adec04c2d868245ded4a61e66"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "12a8eba928ac46a2be533abd016520ae"
                }
              ],
              "extendHandles": []
            },
            "isRunning": false,
            "isFinished": false,
            "hasError": false,
            "isSkipped": false,
            "isCancelled": false
          },
          "label": "普通组件",
          "targetPosition": "left",
          "sourcePosition": "right"
        },
        {
          "id": "f34becbacd1f45b8afc8e589819a4715",
          "type": "note",
          "initialized": false,
          "position": {
            "x": 15.328752766794082,
            "y": 119.50701774802258
          },
          "data": {
            "id": "ca52a8ecb9444f3bb7fdf38d7816ef05",
            "type": "note",
            "name": "注释",
            "mode": "default",
            "nodeDataNote": {
              "label": "并行编排方式一",
              "fontSize": 16,
              "showResizer": false
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "70c0be7f19d84888b9e91b3929cde024"
                },
                {
                  "position": "right",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "1d06e9e5120d4c709a9d4995c8acb7e4"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "fba57dd08f5e48a4a9bd3cb8d918dc11"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "d7d19136e8a84be5aa30ef1463a3337c"
                }
              ]
            }
          },
          "label": "注释"
        },
        {
          "id": "f4fa377e36a74c3297bb4136bba35d81",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 144.95621365692455,
            "y": 613.5897748594547
          },
          "data": {
            "id": "d",
            "type": "common",
            "name": "普通组件d",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "7f22aefc461948bfb0ef56044f38ab89"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "76ac26b27e5b4f95b047ddb9b1d8f4e6"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "7f6fb319f9b54140af93ef97100d090a"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "e73451d0a93348a692d231037214bbbd"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "705e8319d9da412aa50e6d72a55527d0",
          "type": "when",
          "initialized": false,
          "position": {
            "x": 496.4162532306681,
            "y": 677.6054914259652
          },
          "data": {
            "id": "a05f8db9286a42958bb247c5eea83ef8",
            "type": "when",
            "name": "并行",
            "mode": "default",
            "nodeDataWhen": {
              "ignoreError": true
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "d3b33616accd4c368d382b7b72d10e8f"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "8e212a115e4e4db69602fea78ae78bf3"
                }
              ]
            }
          },
          "label": "并行"
        },
        {
          "id": "c54a20cbb2a74a52a9d33b8d398373dc",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 617.0258486119025,
            "y": 529.1020644463474
          },
          "data": {
            "id": "e",
            "type": "common",
            "name": "普通组件e",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "c316cb1f31e84f7da653a140d21896a5"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "6bd151fced204837a34e3e87ba847dd9"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "e7094bdb545946a19e1aa16b86e2983e"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "0a65cc446d9b4755a7fc50bc71d00f0a"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "c8b0c2d0425c4d59b5c6173148736d89",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 616.8296399561652,
            "y": 718.0319619723241
          },
          "data": {
            "id": "f",
            "type": "common",
            "name": "普通组件f",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "0c443e4f6d9b48afa13078b5ebbba44b"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "0106ac2c7b0542969a829322a2beceba"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "64e375f74549424099893fa61ed7a01f"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "75b47937f20f4ba19410253bd10d2294"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "af7472ff174a4fd7bf6585c422fb8e48",
          "type": "note",
          "initialized": false,
          "position": {
            "x": -16.52992133626607,
            "y": 508.85058673839603
          },
          "data": {
            "id": "a0f458a8e33f425db1ee74a55ec8591c",
            "type": "note",
            "name": "注释",
            "mode": "default",
            "nodeDataNote": {
              "label": "并行编排方式二",
              "fontSize": 16,
              "showResizer": false
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "3eff20bec4e94d5897a3242dec3535d2"
                },
                {
                  "position": "right",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "12050658f3674f2d9e1aaf92984d20c1"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "446c10d8c4b7440f85ae71256c6e50ed"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "e81a2df7c6354491bfeb7ea07ace0543"
                }
              ]
            }
          },
          "label": "注释"
        },
        {
          "id": "f8e28088405347c9aecfe0fa35532d82",
          "type": "note",
          "initialized": false,
          "position": {
            "x": 1022.5262741023166,
            "y": 204.495601764204
          },
          "data": {
            "id": "eab4d884464747df9cd4bbcf61ba963d",
            "type": "note",
            "name": "注释",
            "mode": "default",
            "nodeDataNote": {
              "label": "并行编排方式三",
              "fontSize": 16,
              "showResizer": false
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "c23c57c410854ac7b9cb57a098946d51"
                },
                {
                  "position": "right",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "c1308fc38ef84e35af0819b05231356e"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "d54df8bb837b4a2a87a45c8ca89df385"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "e66bf582402448509bbb6175c49add80"
                }
              ]
            }
          },
          "label": "注释"
        },
        {
          "id": "95b263ff60e046eba8a221eb1adaba36",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 1146.7719686290236,
            "y": 386.50702570332487
          },
          "data": {
            "id": "m",
            "type": "common",
            "name": "普通组件m",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "1dd528ab9bf44e96bed40a15947febb2"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "e084c672a6074437a056feca7bf75fda"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "f877f1c11e4e48539ad4231e58223145"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "e9674c99db6846429fc87e695d899b45"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "983b6bf2e1734f4ab02e77d5f673601d",
          "type": "when",
          "initialized": false,
          "position": {
            "x": 1549.2710903374664,
            "y": 346.0854086630391
          },
          "data": {
            "id": "7f1d12dad2e245908b51d9ebfcf434e2",
            "type": "when",
            "name": "并行(未完成)",
            "mode": "default",
            "nodeDataWhen": {},
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "020c22d01095441685b1a948c03fc482"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "53536a918f16494290ac7316c44c0856"
                }
              ]
            }
          },
          "label": "并行(未完成)"
        },
        {
          "id": "29227a57659145f589caa1ce6ca53aaa",
          "type": "when",
          "initialized": false,
          "position": {
            "x": 1536.0760112297373,
            "y": 536.0945478143359
          },
          "data": {
            "id": "440c784306f744f59b66d1bb7244e386",
            "type": "when",
            "name": "并行(未完成)",
            "mode": "default",
            "nodeDataWhen": {},
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "e64e4138fa2249f2bcc7517fe3bc5672"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "dadc1e077e5c48438b172d8f777831d4"
                }
              ]
            }
          },
          "label": "并行(未完成)"
        },
        {
          "id": "fb89b298613b4da4a787ce71c2449704",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 1720.7579098152319,
            "y": 283.58540866303923
          },
          "data": {
            "id": "n",
            "type": "common",
            "name": "普通组件n",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "aaf3857105884507a86ed8c1a1f3afdd"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "fd521e4c114f4cf6acbc45b011fba217"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "6c6051ee02b24de5b6ab85934380b1e1"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "84d8758ec8c74e8793ad5e139f6773e4"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "90a95211efd8423e804888f7a1028e7a",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 1722.077417726005,
            "y": 474.9140557251088
          },
          "data": {
            "id": "x",
            "type": "common",
            "name": "普通组件x",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "ff87c9e50bac41b69dc660ecce47929c"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "f8a20d2210974e62a2a6b932af6bd335"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "c0cbd9ba8ca54185a7836b071234b9ff"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "d3f6b08d835e455ba639f0cf9988592f"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "23642ce5d4da4119845c178ed81d1c18",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 1716.799386082913,
            "y": 668.8817186087241
          },
          "data": {
            "id": "y",
            "type": "common",
            "name": "普通组件y",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "a4805756e7594fb19c0e75d927df3751"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "fa7ccae3af38463cafe1ce74cb8b4cfd"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "8541a1adb3504f6bb06a8a9415f5cdb2"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "c28ebeff9a3546c49cf96de2679a8c41"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        }
      ],
      "edges": [
        {
          "id": "ee4f9b2e9ed944fb8d0e66eeed46f94b",
          "type": "animation",
          "source": "595ea2b6209a4308a261da2e5f3cbfb4",
          "target": "916883b506cf428ba4ba464f281330d2",
          "sourceHandle": "8be6506fb2e942c5b17f9cf89560989a",
          "targetHandle": "848563027b5746ef82d9ae7cc39aeadd",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 456.000107212635,
          "sourceY": 316.5000350468648,
          "targetX": 595.711923034235,
          "targetY": 219.80886712625357
        },
        {
          "id": "f02ccc90a13f4c0ea4abeaaeaae501f1",
          "type": "animation",
          "source": "595ea2b6209a4308a261da2e5f3cbfb4",
          "target": "bb47d81e448343e1a55eff1cd1db9eb6",
          "sourceHandle": "8be6506fb2e942c5b17f9cf89560989a",
          "targetHandle": "1ebe2bab7913495d9ecf2a4202c82a45",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 456.000107212635,
          "sourceY": 316.5000350468648,
          "targetX": 597.2276396007455,
          "targetY": 395.1987725568879
        },
        {
          "id": "af9722c3ca774bea856e7ca693035528",
          "type": "animation",
          "source": "f34becbacd1f45b8afc8e589819a4715",
          "target": "595ea2b6209a4308a261da2e5f3cbfb4",
          "sourceHandle": "d7d19136e8a84be5aa30ef1463a3337c",
          "targetHandle": "bb3a03bedddf484bae58db676b98c9d9",
          "updatable": true,
          "data": {
            "type": "note"
          },
          "label": "注释",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 90.32877902294959,
          "sourceY": 172.50703488745742,
          "targetX": 143.99999781198704,
          "targetY": 316.5000350468648
        },
        {
          "id": "feb0cca71e6f4608bf2a6545b4b78c8d",
          "type": "animation",
          "source": "f4fa377e36a74c3297bb4136bba35d81",
          "target": "705e8319d9da412aa50e6d72a55527d0",
          "sourceHandle": "76ac26b27e5b4f95b047ddb9b1d8f4e6",
          "targetHandle": "d3b33616accd4c368d382b7b72d10e8f",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 450.95632086955953,
          "sourceY": 694.0896891017171,
          "targetX": 493.416272270762,
          "targetY": 695.6055572982988
        },
        {
          "id": "b83494d4c81f4a0eb4f8fb351438c93d",
          "type": "animation",
          "source": "705e8319d9da412aa50e6d72a55527d0",
          "target": "c54a20cbb2a74a52a9d33b8d398373dc",
          "sourceHandle": "8e212a115e4e4db69602fea78ae78bf3",
          "targetHandle": "c316cb1f31e84f7da653a140d21896a5",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 535.4162853988397,
          "sourceY": 695.6055572982988,
          "targetX": 611.0258464238894,
          "targetY": 609.6020994932122
        },
        {
          "id": "93ca500f41ed4e6a8d502990bdc0afac",
          "type": "animation",
          "source": "705e8319d9da412aa50e6d72a55527d0",
          "target": "c8b0c2d0425c4d59b5c6173148736d89",
          "sourceHandle": "8e212a115e4e4db69602fea78ae78bf3",
          "targetHandle": "0c443e4f6d9b48afa13078b5ebbba44b",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 535.4162853988397,
          "sourceY": 695.6055572982988,
          "targetX": 610.8296377681522,
          "targetY": 798.5319567509881
        },
        {
          "id": "c3caec7ea374495381ffe4586ea4b140",
          "type": "animation",
          "source": "af7472ff174a4fd7bf6585c422fb8e48",
          "target": "f4fa377e36a74c3297bb4136bba35d81",
          "sourceHandle": "e81a2df7c6354491bfeb7ea07ace0543",
          "targetHandle": "7f22aefc461948bfb0ef56044f38ab89",
          "updatable": true,
          "data": {
            "type": "note"
          },
          "label": "注释",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 58.47010491988944,
          "sourceY": 561.8506240119312,
          "targetX": 138.9562114689116,
          "targetY": 694.0896891017171
        },
        {
          "id": "75a5a9f233c14ec4bf3a8e8e5f7ba992",
          "type": "animation",
          "source": "f8e28088405347c9aecfe0fa35532d82",
          "target": "95b263ff60e046eba8a221eb1adaba36",
          "sourceHandle": "e66bf582402448509bbb6175c49add80",
          "targetHandle": "1dd528ab9bf44e96bed40a15947febb2",
          "updatable": true,
          "data": {
            "type": "note"
          },
          "label": "注释",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 1097.526300358472,
          "sourceY": 257.49561890363884,
          "targetX": 1140.7719261728098,
          "targetY": 467.0070607501897
        },
        {
          "id": "10c99fd423a747c6a60bd946dd8b0fca",
          "type": "animation",
          "source": "95b263ff60e046eba8a221eb1adaba36",
          "target": "983b6bf2e1734f4ab02e77d5f673601d",
          "sourceHandle": "e084c672a6074437a056feca7bf75fda",
          "targetHandle": "020c22d01095441685b1a948c03fc482",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 1452.7721161098593,
          "sourceY": 467.0070607501897,
          "targetX": 1546.2711093775604,
          "targetY": 364.08543426717193
        },
        {
          "id": "edbf1890e802416e8ec64598c68e217e",
          "type": "animation",
          "source": "95b263ff60e046eba8a221eb1adaba36",
          "target": "29227a57659145f589caa1ce6ca53aaa",
          "sourceHandle": "e084c672a6074437a056feca7bf75fda",
          "targetHandle": "e64e4138fa2249f2bcc7517fe3bc5672",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 1452.7721161098593,
          "sourceY": 467.0070607501897,
          "targetX": 1533.0760302698313,
          "targetY": 554.0945331502679
        },
        {
          "id": "176547de10294b05a8f599892894d03d",
          "type": "animation",
          "source": "983b6bf2e1734f4ab02e77d5f673601d",
          "target": "fb89b298613b4da4a787ce71c2449704",
          "sourceHandle": "53536a918f16494290ac7316c44c0856",
          "targetHandle": "aaf3857105884507a86ed8c1a1f3afdd",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 1588.2710419692364,
          "sourceY": 364.08543426717193,
          "targetX": 1714.7577868226165,
          "targetY": 364.08540344170325
        },
        {
          "id": "0ccaa5afea3e4a978ce3041cab63bc8e",
          "type": "animation",
          "source": "29227a57659145f589caa1ce6ca53aaa",
          "target": "90a95211efd8423e804888f7a1028e7a",
          "sourceHandle": "dadc1e077e5c48438b172d8f777831d4",
          "targetHandle": "ff87c9e50bac41b69dc660ecce47929c",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 1575.0761239343105,
          "sourceY": 554.0945331502679,
          "targetX": 1716.0774558061928,
          "targetY": 555.4140907719736
        },
        {
          "id": "0154467d06b84b6987464cb18a05b9a1",
          "type": "animation",
          "source": "29227a57659145f589caa1ce6ca53aaa",
          "target": "23642ce5d4da4119845c178ed81d1c18",
          "sourceHandle": "dadc1e077e5c48438b172d8f777831d4",
          "targetHandle": "a4805756e7594fb19c0e75d927df3751",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 1575.0761239343105,
          "sourceY": 554.0945331502679,
          "targetX": 1710.7992630902977,
          "targetY": 749.3816328509865
        }
      ],
      "position": [
        34.73062208134343,
        -78.94076776763848
      ],
      "zoom": 0.7578582832551992,
      "viewport": {
        "x": 34.73062208134343,
        "y": -78.94076776763848,
        "zoom": 0.7578582832551992
      }
    }
}


export { whenFlow };

