function subflowFlow(){
    return {
      "nodes": [
        {
          "id": "ee8e0a110d574f9987587f6e99cfee98",
          "type": "subflow",
          "initialized": false,
          "position": {
            "x": 196.5,
            "y": 61.984375
          },
          "data": {
            "id": "467a432e7a1d4acb81fc1ff420755601",
            "type": "subflow",
            "name": "子流程(未完成)",
            "mode": "default",
            "nodeDataSubflow": {
              "chainName": "subflow1"
            },
            "style": {
              "handles": [
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "7a7934f559284ed5be23c4803644d127"
                }
              ]
            }
          },
          "label": "子流程(未完成)"
        },
        {
          "id": "a517cd62ec1f4946af91b8e27948f611",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 317.5,
            "y": -1.197265625
          },
          "data": {
            "id": "a",
            "type": "common",
            "name": "普通组件a",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "1aa9cbb3230844aab06a1e9e3b08ee07"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "6b7a8490a778489fa559bb028cf9b479"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "278a3e0134534292bd1b75f712431245"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "36f2645cfbf9467794183c21c540d1ff"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "91553b2eb6ed49328290612d7655688c",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 708.9753417091006,
            "y": -2.5024916014962173
          },
          "data": {
            "id": "b",
            "type": "common",
            "name": "普通组件b",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "fe7bcbd8e407454697bea2c7b0fb46b9"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "00c0d243df564d3388ecc38f27686008"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "9bb762cfdb5c450f83f825e56aa0c283"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "7123cb7ad4c14d93992885c78436c7ec"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "c7cbcc74d6b54c8ca4d2310f126644ef",
          "type": "subflow",
          "initialized": false,
          "position": {
            "x": 201.52691078036673,
            "y": 255.716537491177
          },
          "data": {
            "id": "b7e1153b2df2444395d8ae377b6f54ec",
            "type": "subflow",
            "name": "子流程(未完成)",
            "mode": "default",
            "nodeDataSubflow": {
              "chainName": "subflow2"
            },
            "style": {
              "handles": [
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "904d66a42559462f9bed02bd6c7553af"
                }
              ]
            }
          },
          "label": "子流程(未完成)"
        },
        {
          "id": "8aa061bc712d47d98b952849bd3cf426",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 313.05516835369303,
            "y": 193.99014720318542
          },
          "data": {
            "id": "c",
            "type": "common",
            "name": "普通组件c",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "f0e4e9537c9445b38bf8dcb7855cc6a4"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "a62bc235cd3c4cc880f5205fda3591e5"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "a4a4cf62c5b54289aa1e5c697b4e88b8"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "1151c3f6005348c09fa721e1d0066968"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "83e003c7dddf49a889a0a4be54376e27",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 710.2759776711191,
            "y": 194.76375691519394
          },
          "data": {
            "id": "d",
            "type": "common",
            "name": "普通组件d",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "c8cfc5cb5b824cc7bd7964e5ce2b2aec"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "56ec205d5fae471d9851aeb48907f4fe"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "886b7036d2c24c32a6526d434aa593f5"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "0f20b5ffa0034de58c0df2feeaad64b4"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "377c1ae311944084a424821219e6ccd9",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 318.9588191717944,
            "y": 445.1578933566775
          },
          "data": {
            "id": "e",
            "type": "common",
            "name": "普通组件e",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "29aa4da6b97e4ebfaa688a66493a3268"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "62c7d42cee83429bb82d4f2153118f20"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "bf9f1a7ec4654bcbabd18c7cb24c4adc"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "1f559378189146b18bc93ee46376f1d0"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "11e065fa47094e34bed55c522b63edd0",
          "type": "chain",
          "initialized": false,
          "position": {
            "x": 715.6527875873758,
            "y": 387.72736717979905
          },
          "data": {
            "id": "0f72769a116f47e185718c4fd0edff8d",
            "type": "chain",
            "name": "链路组件",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "a5f947a7e93b455182d821b099634830"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "964ff03052db4d2393bb59729b7b73ef"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "6ea9cbfc83994f16a84b3982cba97b6a"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "d13f1a57e8814ad9b8ee4c6ad75b97c9"
                }
              ],
              "extendHandles": []
            },
            "nodeDataChain": {
              "chainId": "subflow1"
            }
          },
          "label": "链路组件"
        },
        {
          "id": "8019c5e2203d4464b3fc0dbcba027ead",
          "type": "chain",
          "initialized": false,
          "position": {
            "x": 716.953423549394,
            "y": 589.3259412926353
          },
          "data": {
            "id": "fbb8cd823a8f47fa80d56ae59d197318",
            "type": "chain",
            "name": "链路组件",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "19c8c8b936e3424cb74a0d56900fda08"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "7c83a366bbd94006821fef37248477a7"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "b97743babaed4eeb8f6deb1320cd464e"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "2f441822e8fe4565bec5b4cd2c82e237"
                }
              ],
              "extendHandles": []
            },
            "nodeDataChain": {
              "chainId": "subflow2"
            }
          },
          "label": "链路组件"
        },
        {
          "id": "6cad8a3f4c244acb97473197d6decd2b",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 1066.8244973323162,
            "y": 583.0253053306171
          },
          "data": {
            "id": "d",
            "type": "common",
            "name": "普通组件d",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "7d18778a47d24d96bcc00ce77c2f7d7d"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "d7552c6c7e4442d0b8e8111a685f65e2"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "4fca8314467545f69d445e3dfaa3f5b9"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "940ccef3b466448ab0dcb3e23f493888"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        }
      ],
      "edges": [
        {
          "id": "e98e1ee28dff4da8bd18e5372a5f4357",
          "type": "animation",
          "source": "ee8e0a110d574f9987587f6e99cfee98",
          "target": "a517cd62ec1f4946af91b8e27948f611",
          "sourceHandle": "7a7934f559284ed5be23c4803644d127",
          "targetHandle": "1aa9cbb3230844aab06a1e9e3b08ee07",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 235.5,
          "sourceY": 79.984375,
          "targetX": 311.5,
          "targetY": 79.302734375
        },
        {
          "id": "0ae0701ee37946c7af72ed89ef437065",
          "type": "animation",
          "source": "a517cd62ec1f4946af91b8e27948f611",
          "target": "91553b2eb6ed49328290612d7655688c",
          "sourceHandle": "6b7a8490a778489fa559bb028cf9b479",
          "targetHandle": "fe7bcbd8e407454697bea2c7b0fb46b9",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 623.5,
          "sourceY": 79.302734375,
          "targetX": 702.975389255694,
          "targetY": 77.99749750982615
        },
        {
          "id": "c8707a478926482396c2b8312fed5e58",
          "type": "animation",
          "source": "8aa061bc712d47d98b952849bd3cf426",
          "target": "83e003c7dddf49a889a0a4be54376e27",
          "sourceHandle": "a62bc235cd3c4cc880f5205fda3591e5",
          "targetHandle": "c8cfc5cb5b824cc7bd7964e5ce2b2aec",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 619.0550433961669,
          "sourceY": 274.49006449090547,
          "targetX": 704.2759397189858,
          "targetY": 275.2637138951552
        },
        {
          "id": "eb85406519f74f0393cead9fc0a800e7",
          "type": "animation",
          "source": "c7cbcc74d6b54c8ca4d2310f126644ef",
          "target": "8aa061bc712d47d98b952849bd3cf426",
          "sourceHandle": "904d66a42559462f9bed02bd6c7553af",
          "targetHandle": "f0e4e9537c9445b38bf8dcb7855cc6a4",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 240.52692284063866,
          "sourceY": 273.7165554079613,
          "targetX": 307.0551700938009,
          "targetY": 274.49006449090547
        },
        {
          "id": "f4071d4530da415990f7de53f31ca773",
          "type": "animation",
          "source": "377c1ae311944084a424821219e6ccd9",
          "target": "11e065fa47094e34bed55c522b63edd0",
          "sourceHandle": "62c7d42cee83429bb82d4f2153118f20",
          "targetHandle": "a5f947a7e93b455182d821b099634830",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 624.9586942142682,
          "sourceY": 525.65789002888,
          "targetX": 709.6528290197249,
          "targetY": 463.22733222522373
        },
        {
          "id": "25af160363ef413ebc88fdf70097a358",
          "type": "animation",
          "source": "377c1ae311944084a424821219e6ccd9",
          "target": "8019c5e2203d4464b3fc0dbcba027ead",
          "sourceHandle": "62c7d42cee83429bb82d4f2153118f20",
          "targetHandle": "19c8c8b936e3424cb74a0d56900fda08",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 624.9586942142682,
          "sourceY": 525.65789002888,
          "targetX": 710.9533855972606,
          "targetY": 664.82590633806
        },
        {
          "id": "81fc76fec8f94bd2a7ba7973fcba6c11",
          "type": "animation",
          "source": "8019c5e2203d4464b3fc0dbcba027ead",
          "target": "6cad8a3f4c244acb97473197d6decd2b",
          "sourceHandle": "7c83a366bbd94006821fef37248477a7",
          "targetHandle": "7d18778a47d24d96bcc00ce77c2f7d7d",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 1022.9532192073855,
          "sourceY": 664.82590633806,
          "targetX": 1060.8244593801828,
          "targetY": 663.5253020028197
        }
      ],
      "position": [
        -29.061166126818648,
        16.829862356067906
      ],
      "zoom": 0.7688546443450798,
      "viewport": {
        "x": -29.061166126818648,
        "y": 16.829862356067906,
        "zoom": 0.7688546443450798
      }
    }
}
export { subflowFlow };
