function whileFlow(){
    return {
        "nodes": [
          {
            "id": "1dadeae1cd5449418101a7bda8c5e678",
            "type": "while",
            "initialized": false,
            "position": {
              "x": 63.5,
              "y": 134.5
            },
            "data": {
              "id": "dc15feaac7c8421397ac47ca98203183",
              "type": "while",
              "name": "循环条件组件",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "ab850fe81815402e8b5b9c078100c170"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "a281f9c930c34283abf8c2086c64ad46"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "31c8d1f1a83c47699f233c9cb5dd0358"
                  },
                  {
                    "position": "bottom",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "92704d3d7b6d429c8485bf7934d9caae"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataWhile": {}
            },
            "label": "循环条件组件"
          },
          {
            "id": "9523de98e8f547868491bc5b3652d5ab",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 567.5,
              "y": 236.5
            },
            "data": {
              "id": "d",
              "type": "common",
              "name": "普通组件d",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "3e900703e6db4d3a8946c7009e3bd43c"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "a5426824c9984de8a98fa85b3b6bb3e6"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "18c8e3dba03942a38518b54b54a9e2e1"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "130bf87a1ffe49e1ba8554b235af5fad"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "97682a2c75fc4d2e9920aec94aa122c7",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 61.5,
              "y": 422.5
            },
            "data": {
              "id": "f",
              "type": "common",
              "name": "普通组件f",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "54ae4793d5024dff8aa08f25bc905742"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "1947496226be456bb10b6ef30b34bd41"
                  },
                  {
                    "position": "top",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "605754d7ea964c1b89a6464c102ca30d"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "9bbc7fc02ca14d58b53defedd4805d80"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "db3289e08df74c4094a8975df66a137d",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 566.5,
              "y": 429.5
            },
            "data": {
              "id": "e",
              "type": "boolean",
              "name": "布尔组件e",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "5f3c98cf0a364ec5b0f2093ab6d5d44b"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "977629091c144d799d2d01f1996f700b"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "909ea0837689400ba3c23b82d95afa06"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "2ed93b3ada854fe3992dd3f44af5b837"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          },
          {
            "id": "e93088c39cfb4de3adbc6c77d3df8fbf",
            "type": "and",
            "initialized": false,
            "position": {
              "x": 499.0721984150465,
              "y": -5.410534775559853
            },
            "data": {
              "id": "9900070cff6341a7a6cf105474946ada",
              "type": "and",
              "name": "与",
              "mode": "default",
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "68014e7188a94817ae4982323c0e107d"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "4cf032f082614908ba39e49910e525a3"
                  }
                ]
              },
              "nodeDataAnd": {},
              "nodeDataThen": {}
            },
            "label": "与"
          },
          {
            "id": "78051a1630dc48349e933b7a2ab386f5",
            "type": "or",
            "initialized": false,
            "position": {
              "x": 624.4254499384713,
              "y": -74.02494613575035
            },
            "data": {
              "id": "caf1e743a06d43c48c78d509fb6571be",
              "type": "or",
              "name": "或",
              "mode": "default",
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "6a46c4409fc54edd826ea96ac3d74111"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "d2481905df204dddb4d70dfcb274bbf4"
                  }
                ]
              },
              "nodeDataOr": {},
              "nodeDataThen": {}
            },
            "label": "或"
          },
          {
            "id": "1fafc1d853a54e658ff429554bed6ba9",
            "type": "not",
            "initialized": false,
            "position": {
              "x": 624.4254499384712,
              "y": 112.02566928322773
            },
            "data": {
              "id": "54f4af7824634eb882aceef62cc895cc",
              "type": "not",
              "name": "非",
              "mode": "default",
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "e12b19c9dd5c40eab8e08fac74a28830"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "5cb293ef05a745b6a6691f753c18fe7c"
                  }
                ]
              },
              "nodeDataNot": {},
              "nodeDataThen": {}
            },
            "label": "非"
          },
          {
            "id": "fe0cbe8ca55c479ab38232d5773d6dae",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 910.7094576534788,
              "y": 141.61319139423875
            },
            "data": {
              "id": "c",
              "type": "boolean",
              "name": "布尔组件c",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "ab9f1f7711d84f82a6baec372c5e61cd"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "c71826b6bde944f6bde367f3c646c041"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "c7b5b714a0054737940522ec4319cbc0"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "7bcdcaa8cd79468aae3195460773501d"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          },
          {
            "id": "6b19210a66434437ab1982792c44c1df",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 910.7094576534787,
              "y": -29.9228370062375
            },
            "data": {
              "id": "b",
              "type": "boolean",
              "name": "布尔组件b",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "20dc7d3d2d5648f9881a3bcc05812c7d"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "c09f62e178c44be7b39e310e7e90a671"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "0c274a5931b84e39bc987adb9eee01d1"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "9cd241f3e05141c6a2498623785cf3d7"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          },
          {
            "id": "5c0afdab43b64a50bb03e2006324630f",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 910.7094576534786,
              "y": -200.40325907809546
            },
            "data": {
              "id": "a",
              "type": "boolean",
              "name": "布尔组件a",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "4728089bc265443ea55617a27ee598e3"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "23da52a5749348b8aa17d1e450307b26"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "054753e208ab4728a96010172c6bd3e2"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "6b43312e9aa64d4a8f535dff6728f727"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          }
        ],
        "edges": [
          {
            "id": "86b156e65a2c4a9ab893a220eb1b360d",
            "type": "animation",
            "source": "1dadeae1cd5449418101a7bda8c5e678",
            "target": "9523de98e8f547868491bc5b3652d5ab",
            "sourceHandle": "a281f9c930c34283abf8c2086c64ad46",
            "targetHandle": "3e900703e6db4d3a8946c7009e3bd43c",
            "updatable": true,
            "data": {
              "type": "do"
            },
            "label": "do",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 369.5,
            "sourceY": 210,
            "targetX": 561.5000380801879,
            "targetY": 316.999994778664
          },
          {
            "id": "807016cbbdd04b23ab34d52b127369ec",
            "type": "animation",
            "source": "1dadeae1cd5449418101a7bda8c5e678",
            "target": "97682a2c75fc4d2e9920aec94aa122c7",
            "sourceHandle": "92704d3d7b6d429c8485bf7934d9caae",
            "targetHandle": "605754d7ea964c1b89a6464c102ca30d",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 213.5,
            "sourceY": 291.5,
            "targetX": 211.50001005609724,
            "targetY": 416.49995754378625
          },
          {
            "id": "0ee9cfed14a14d469a416b7638ad1854",
            "type": "animation",
            "source": "1dadeae1cd5449418101a7bda8c5e678",
            "target": "db3289e08df74c4094a8975df66a137d",
            "sourceHandle": "a281f9c930c34283abf8c2086c64ad46",
            "targetHandle": "5f3c98cf0a364ec5b0f2093ab6d5d44b",
            "updatable": true,
            "data": {
              "type": "break"
            },
            "label": "break",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 369.5,
            "sourceY": 210,
            "targetX": 560.4999575437862,
            "targetY": 505.0000533576211
          },
          {
            "id": "824370d57ccc47339c05d98aa6625100",
            "type": "animation",
            "source": "1dadeae1cd5449418101a7bda8c5e678",
            "target": "e93088c39cfb4de3adbc6c77d3df8fbf",
            "sourceHandle": "a281f9c930c34283abf8c2086c64ad46",
            "targetHandle": "68014e7188a94817ae4982323c0e107d",
            "updatable": true,
            "data": {
              "type": "boolean"
            },
            "label": "boolean",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 369.5,
            "sourceY": 210,
            "targetX": 496.07221745514045,
            "targetY": 12.589450560372141
          },
          {
            "id": "4d07f1a6e3b44e549512802d862b0ed3",
            "type": "animation",
            "source": "e93088c39cfb4de3adbc6c77d3df8fbf",
            "target": "78051a1630dc48349e933b7a2ab386f5",
            "sourceHandle": "4cf032f082614908ba39e49910e525a3",
            "targetHandle": "6a46c4409fc54edd826ea96ac3d74111",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 538.0722305832181,
            "sourceY": 12.589450560372141,
            "targetX": 621.4254689785652,
            "targetY": -56.02496079981836
          },
          {
            "id": "60f28a0bb3d145ee99fd2fddff3e5667",
            "type": "animation",
            "source": "e93088c39cfb4de3adbc6c77d3df8fbf",
            "target": "1fafc1d853a54e658ff429554bed6ba9",
            "sourceHandle": "4cf032f082614908ba39e49910e525a3",
            "targetHandle": "e12b19c9dd5c40eab8e08fac74a28830",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 538.0722305832181,
            "sourceY": 12.589450560372141,
            "targetX": 621.425468978565,
            "targetY": 130.02565461915972
          },
          {
            "id": "61b531b31ae54a81976c219c3dd7b94c",
            "type": "animation",
            "source": "1fafc1d853a54e658ff429554bed6ba9",
            "target": "fe0cbe8ca55c479ab38232d5773d6dae",
            "sourceHandle": "5cb293ef05a745b6a6691f753c18fe7c",
            "targetHandle": "ab9f1f7711d84f82a6baec372c5e61cd",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 663.4254015702412,
            "sourceY": 130.02565461915972,
            "targetX": 904.7094957336667,
            "targetY": 217.11324475185984
          },
          {
            "id": "415ad23d638b487a92d9a7fe5826fc79",
            "type": "animation",
            "source": "78051a1630dc48349e933b7a2ab386f5",
            "target": "6b19210a66434437ab1982792c44c1df",
            "sourceHandle": "d2481905df204dddb4d70dfcb274bbf4",
            "targetHandle": "20dc7d3d2d5648f9881a3bcc05812c7d",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 663.4254015702413,
            "sourceY": -56.02496079981836,
            "targetX": 904.7094957336666,
            "targetY": 45.57721635138358
          },
          {
            "id": "cda06d283a554195848e393691c09374",
            "type": "animation",
            "source": "78051a1630dc48349e933b7a2ab386f5",
            "target": "5c0afdab43b64a50bb03e2006324630f",
            "sourceHandle": "d2481905df204dddb4d70dfcb274bbf4",
            "targetHandle": "4728089bc265443ea55617a27ee598e3",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 663.4254015702413,
            "sourceY": -56.02496079981836,
            "targetX": 904.7094957336665,
            "targetY": -124.90322585457477
          }
        ],
        "position": [
          -14.96813723880075,
          192.90211976925042
        ],
        "zoom": 0.8705505632961241,
        "viewport": {
          "x": -14.96813723880075,
          "y": 192.90211976925042,
          "zoom": 0.8705505632961241
        }
      }
}

export { whileFlow };
