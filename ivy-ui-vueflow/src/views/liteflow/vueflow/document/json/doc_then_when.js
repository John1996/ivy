function thenWhenFlow(){
    return {
        "nodes": [
          {
            "id": "2aeb4c3e89674928bd098638be909dc7",
            "type": "when",
            "initialized": false,
            "position": {
              "x": 233.5,
              "y": 203.984375
            },
            "data": {
              "id": "f216bfd99fca444884f93db65a7a7079",
              "type": "when",
              "name": "并行",
              "mode": "default",
              "nodeDataWhen": {
                "ignoreError": true
              },
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "a494c527642e42b2841e9be2231b19fb"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "092dacd7d97b4d3190dee99772673d7b"
                  }
                ]
              }
            },
            "label": "并行"
          },
          {
            "id": "8ab3d8e79a6342c0b4023f5c466b2910",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 385.5,
              "y": 47.484375
            },
            "data": {
              "id": "a",
              "type": "common",
              "name": "普通组件a",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "e0f8c5117d334e2a80bafca9b75c4531"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "372fc6ed166646d6aac7ffe38f988aaf"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "c687fb79b1054bf9ba0b90bc439e5f65"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "67e52c5b764b4b21b433d3e532ca18ef"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "4f47430189d8499bb22951619a36cae0",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 817.5,
              "y": 118.484375
            },
            "data": {
              "id": "b",
              "type": "common",
              "name": "普通组件b",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "1c3545fd21c34e189d0e12a1fac8ac52"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "e668741fd6e849cbb2498d3ca6ea67d0"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "3466ddd7001b4e8f860ada65f2e9c913"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "ec56fbd182d7420e816993cf36822c2e"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "9a6072e820ac45a2adb4fd2c03e5bf60",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 385.5,
              "y": 240.484375
            },
            "data": {
              "id": "c",
              "type": "common",
              "name": "普通组件c",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "7b6231d779d44127a6fdda4fdd32cebb"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "3eb0316c70b84aa5b2f6b63a51ba5f90"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "f8ccd44876bd495d94b5062997bfe674"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "2e22a0fdf2f94d5fb00432054561c4d7"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "141ec09c23bd4cbf857fdb3809fee613",
            "type": "when",
            "initialized": false,
            "position": {
              "x": 1210.5,
              "y": 181.984375
            },
            "data": {
              "id": "703edd094b734db89caf8ae1d5a59f20",
              "type": "when",
              "name": "并行",
              "mode": "default",
              "nodeDataWhen": {
                "id": "a"
              },
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "b707696f04e346029017808f4ef8b837"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "49ca9d0a1f964937a1f6f3a6c65db525"
                  }
                ]
              }
            },
            "label": "并行"
          },
          {
            "id": "8a04415109054aff89465301d0f564c2",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 1455.5,
              "y": 55.484375
            },
            "data": {
              "id": "d",
              "type": "common",
              "name": "普通组件d",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "9f22ad17d56740a5948f32647cb88f08"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "26d997f6a5d24e2cb59d98d3cc929084"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "e2158a4908f042bbbc9e5d331d98e037"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "c6d5072ae6ce4c498975cba9941288a6"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "ab7ac18b345f4ff9af8fb59d13b995ef",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 1314.5,
              "y": 307.484375
            },
            "data": {
              "id": "e",
              "type": "common",
              "name": "普通组件e",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "d5a031bee371444ab6504e801f3fee7e"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "daf21693b2b94fe9bb920fa031ebd2ec"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "1d6af250bf4b40ad8dbe50061acbeb67"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "c46aac8b75784843adbb290164abac63"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "dc917864370940f3a829453750507ab7",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 1846.5,
              "y": 55.484375
            },
            "data": {
              "id": "f",
              "type": "common",
              "name": "普通组件f",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "a6bbd36100a44de8ac235435bd03e71d"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "93804401e8294de694e9f5c64b6d49d2"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "adf06e90aa404764ba8b12676ae093bb"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "8a3feea876b64cc98513f8c7f42237e3"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "7d5e447702fd47298d04d622f166f6d3",
            "type": "then",
            "initialized": false,
            "position": {
              "x": 1330.5,
              "y": 133.984375
            },
            "data": {
              "id": "8dbb144f4c194fd69d09a94b9b461779",
              "type": "then",
              "name": "串行",
              "mode": "default",
              "nodeDataThen": {
                "id": "c"
              },
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "09c011f7781643cab1b301c24e378edb"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "551957f7ec634504af464e5da7a7a8b1"
                  }
                ]
              }
            },
            "label": "串行"
          },
          {
            "id": "66b9bd773a9e4988beed43f1aeffd951",
            "type": "then",
            "initialized": false,
            "position": {
              "x": 2235.5,
              "y": 114.984375
            },
            "data": {
              "id": "661bf71ff48f4a4c87d910e6fa8b85fb",
              "type": "then",
              "name": "串行",
              "mode": "default",
              "nodeDataThen": {
                "id": "d"
              },
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "8ce644c4eef34332b253508d99ff16a7"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "a60d571925404c63a5e7977547f0c437"
                  }
                ]
              }
            },
            "label": "串行"
          },
          {
            "id": "9946ef59b4b646c2ac69ff5f63368667",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 2361.5,
              "y": 49.484375
            },
            "data": {
              "id": "g",
              "type": "common",
              "name": "普通组件g",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "a193b6e8a8524e0490891f8dcc5df23a"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "8150a3344e4844f4a042dc43edbf6f0d"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "3a4a8c4952064657be45a27b65192695"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "8a0d8eb002b6487089b26f682432cef3"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "35f2eb9872fa42ce8cf1505824791801",
            "type": "common",
            "initialized": false,
            "position": {
              "x": -136.5,
              "y": 141.484375
            },
            "data": {
              "id": "m",
              "type": "common",
              "name": "普通组件m",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "7ef00f5075bd4fa3b015d2c1da900939"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "a61e6c9b3f714b839690e1f3ac734386"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "fc2fcee4a2204dfd808a93d3cc2fbd45"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "125ecb6473204848bd5567a4302323ab"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "9491ee818003442ab8280525ef9e571c",
            "type": "then",
            "initialized": false,
            "position": {
              "x": -320.5,
              "y": 202.984375
            },
            "data": {
              "id": "bea5cdfb45514521ba045d541fcd5cc5",
              "type": "then",
              "name": "串行",
              "mode": "default",
              "nodeDataThen": {
                "isCatch": false,
                "id": "aa"
              },
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "f0725dca2e7f46cd9a01ef5825b48175"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "79562dbded9b45ea9679b8070e350b66"
                  }
                ]
              }
            },
            "label": "串行"
          }
        ],
        "edges": [
          {
            "id": "45696a99407746a4826f3beb61a7cbca",
            "type": "animation",
            "source": "2aeb4c3e89674928bd098638be909dc7",
            "target": "8ab3d8e79a6342c0b4023f5c466b2910",
            "sourceHandle": "092dacd7d97b4d3190dee99772673d7b",
            "targetHandle": "e0f8c5117d334e2a80bafca9b75c4531",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 272.5,
            "sourceY": 221.984375,
            "targetX": 379.5,
            "targetY": 127.984375
          },
          {
            "id": "024c2792aebc45e6a9fba8c72463af15",
            "type": "animation",
            "source": "8ab3d8e79a6342c0b4023f5c466b2910",
            "target": "4f47430189d8499bb22951619a36cae0",
            "sourceHandle": "372fc6ed166646d6aac7ffe38f988aaf",
            "targetHandle": "1c3545fd21c34e189d0e12a1fac8ac52",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 691.5,
            "sourceY": 127.984375,
            "targetX": 811.5,
            "targetY": 198.984375
          },
          {
            "id": "3057b7eca2464356a054d9fada2b6e5c",
            "type": "animation",
            "source": "2aeb4c3e89674928bd098638be909dc7",
            "target": "9a6072e820ac45a2adb4fd2c03e5bf60",
            "sourceHandle": "092dacd7d97b4d3190dee99772673d7b",
            "targetHandle": "7b6231d779d44127a6fdda4fdd32cebb",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 272.5,
            "sourceY": 221.984375,
            "targetX": 379.5,
            "targetY": 320.984375
          },
          {
            "id": "20ee8237f7eb4fdca6bf40531f2a46ab",
            "type": "animation",
            "source": "9a6072e820ac45a2adb4fd2c03e5bf60",
            "target": "4f47430189d8499bb22951619a36cae0",
            "sourceHandle": "3eb0316c70b84aa5b2f6b63a51ba5f90",
            "targetHandle": "1c3545fd21c34e189d0e12a1fac8ac52",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 691.5,
            "sourceY": 320.984375,
            "targetX": 811.5,
            "targetY": 198.984375
          },
          {
            "id": "2ae1fc77fee2407bbe27ba7d40c174aa",
            "type": "animation",
            "source": "4f47430189d8499bb22951619a36cae0",
            "target": "141ec09c23bd4cbf857fdb3809fee613",
            "sourceHandle": "e668741fd6e849cbb2498d3ca6ea67d0",
            "targetHandle": "b707696f04e346029017808f4ef8b837",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 1123.5,
            "sourceY": 198.984375,
            "targetX": 1207.5,
            "targetY": 199.984375
          },
          {
            "id": "d9b563a67fe342618cab58b8b1a13f05",
            "type": "animation",
            "source": "141ec09c23bd4cbf857fdb3809fee613",
            "target": "ab7ac18b345f4ff9af8fb59d13b995ef",
            "sourceHandle": "49ca9d0a1f964937a1f6f3a6c65db525",
            "targetHandle": "d5a031bee371444ab6504e801f3fee7e",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 1249.5,
            "sourceY": 199.984375,
            "targetX": 1308.5,
            "targetY": 387.984375
          },
          {
            "id": "879e20ed725943ee802eda7c37241d42",
            "type": "animation",
            "source": "8a04415109054aff89465301d0f564c2",
            "target": "dc917864370940f3a829453750507ab7",
            "sourceHandle": "26d997f6a5d24e2cb59d98d3cc929084",
            "targetHandle": "a6bbd36100a44de8ac235435bd03e71d",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 1761.5,
            "sourceY": 135.984375,
            "targetX": 1840.5,
            "targetY": 135.984375
          },
          {
            "id": "458b378c4bbc40729bcfa0010af1ffda",
            "type": "animation",
            "source": "141ec09c23bd4cbf857fdb3809fee613",
            "target": "7d5e447702fd47298d04d622f166f6d3",
            "sourceHandle": "49ca9d0a1f964937a1f6f3a6c65db525",
            "targetHandle": "09c011f7781643cab1b301c24e378edb",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 1249.5,
            "sourceY": 199.984375,
            "targetX": 1327.5,
            "targetY": 151.984375
          },
          {
            "id": "d8cd2e9843774121a297eb38328dcb26",
            "type": "animation",
            "source": "7d5e447702fd47298d04d622f166f6d3",
            "target": "8a04415109054aff89465301d0f564c2",
            "sourceHandle": "551957f7ec634504af464e5da7a7a8b1",
            "targetHandle": "9f22ad17d56740a5948f32647cb88f08",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 1369.5,
            "sourceY": 151.984375,
            "targetX": 1449.5,
            "targetY": 135.984375
          },
          {
            "id": "eaa6ba7dda4f4451a8798f01190f80f8",
            "type": "animation",
            "source": "dc917864370940f3a829453750507ab7",
            "target": "66b9bd773a9e4988beed43f1aeffd951",
            "sourceHandle": "93804401e8294de694e9f5c64b6d49d2",
            "targetHandle": "8ce644c4eef34332b253508d99ff16a7",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 2152.5,
            "sourceY": 135.984375,
            "targetX": 2232.5,
            "targetY": 132.984375
          },
          {
            "id": "38080d1de7ec41b6b0beaeb47308fa99",
            "type": "animation",
            "source": "66b9bd773a9e4988beed43f1aeffd951",
            "target": "9946ef59b4b646c2ac69ff5f63368667",
            "sourceHandle": "a60d571925404c63a5e7977547f0c437",
            "targetHandle": "a193b6e8a8524e0490891f8dcc5df23a",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 2274.5,
            "sourceY": 132.984375,
            "targetX": 2355.5,
            "targetY": 129.984375
          },
          {
            "id": "ae093a4a87a14546ac0f9b77a89e8284",
            "type": "animation",
            "source": "35f2eb9872fa42ce8cf1505824791801",
            "target": "2aeb4c3e89674928bd098638be909dc7",
            "sourceHandle": "a61e6c9b3f714b839690e1f3ac734386",
            "targetHandle": "a494c527642e42b2841e9be2231b19fb",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 169.5,
            "sourceY": 221.984375,
            "targetX": 230.5,
            "targetY": 221.984375
          },
          {
            "id": "8ac57f8adae1418cb7942728cac667ef",
            "type": "animation",
            "source": "9491ee818003442ab8280525ef9e571c",
            "target": "35f2eb9872fa42ce8cf1505824791801",
            "sourceHandle": "79562dbded9b45ea9679b8070e350b66",
            "targetHandle": "7ef00f5075bd4fa3b015d2c1da900939",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": -281.5,
            "sourceY": 220.984375,
            "targetX": -142.5,
            "targetY": 221.984375
          }
        ],
        "position": [
          -958,
          -11
        ],
        "zoom": 1,
        "viewport": {
          "x": -958,
          "y": -11,
          "zoom": 1
        }
      }
}

export { thenWhenFlow };
