function iteratorFlow(){
    return {
      "nodes": [
        {
          "id": "170a9152cf8446139005962abe3882d0",
          "type": "iterator",
          "initialized": false,
          "position": {
            "x": 157.5,
            "y": 150.5
          },
          "data": {
            "id": "a",
            "type": "iterator",
            "name": "循环迭代组件a",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "e0b488b9d0f34167ba53ac2559cea06e"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "451ecc53ab084a06a37c0f7c5533f885"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "2621bebdd8da42da968d26d04e4ca1c0"
                },
                {
                  "position": "bottom",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "ca07a641ec03463a8ce68557c3d72944"
                }
              ],
              "extendHandles": []
            },
            "nodeDataIterator": {},
            "nodeDataLoop": {}
          },
          "label": "循环迭代组件"
        },
        {
          "id": "69084a9249774a738c630f3be16f28bc",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 631.5,
            "y": 145.5
          },
          "data": {
            "id": "c",
            "type": "common",
            "name": "普通组件c",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "a1e3c2abbfa340649f457e62822bf90c"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "1ff62c9830a5413596bef9d65491a35d"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "0d76739b924d40659ed27622b1273823"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "c5a0b9cb696940a98a8f96755af6c24e"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "75457f0adda346cbbd6c7c2453daf8a3",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 156.5,
            "y": 433.5
          },
          "data": {
            "id": "b",
            "type": "common",
            "name": "普通组件b",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "06f0a1567d5e491291466394f1b37cfb"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "9d910264a7904f33a8b05bdc9f2ff3b8"
                },
                {
                  "position": "top",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "e665d277864f4315bac84509a6ebb384"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "e3ab7d7e09794e8fafa1f38539c12ea2"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "80633b15b5cd4f34aac4f4385de66ce9",
          "type": "boolean",
          "initialized": false,
          "position": {
            "x": 638.5,
            "y": 371.484375
          },
          "data": {
            "id": "d",
            "type": "boolean",
            "name": "布尔组件d",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": ""
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": ""
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": ""
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": ""
                }
              ],
              "extendHandles": []
            },
            "nodeDataBoolean": {}
          },
          "label": "普通组件"
        }
      ],
      "edges": [
        {
          "id": "269722004b6447a798107dacc906ae26",
          "type": "animation",
          "source": "170a9152cf8446139005962abe3882d0",
          "target": "69084a9249774a738c630f3be16f28bc",
          "sourceHandle": "451ecc53ab084a06a37c0f7c5533f885",
          "targetHandle": "a1e3c2abbfa340649f457e62822bf90c",
          "updatable": true,
          "data": {
            "type": "do"
          },
          "label": "do",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 463.5,
          "sourceY": 226,
          "targetX": 625.5,
          "targetY": 226
        },
        {
          "id": "19ad42f7f1ab427984ab652ae343f6c3",
          "type": "animation",
          "source": "170a9152cf8446139005962abe3882d0",
          "target": "75457f0adda346cbbd6c7c2453daf8a3",
          "sourceHandle": "ca07a641ec03463a8ce68557c3d72944",
          "targetHandle": "e665d277864f4315bac84509a6ebb384",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 307.5,
          "sourceY": 307.5,
          "targetX": 306.5,
          "targetY": 427.5
        },
        {
          "id": "cf837fab0a5543dcad3f2e52876c0961",
          "type": "animation",
          "source": "170a9152cf8446139005962abe3882d0",
          "target": "80633b15b5cd4f34aac4f4385de66ce9",
          "sourceHandle": "451ecc53ab084a06a37c0f7c5533f885",
          "targetHandle": null,
          "updatable": true,
          "data": {
            "type": "break"
          },
          "label": "break",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 463.5,
          "sourceY": 226,
          "targetX": 632.5,
          "targetY": 446.984375
        }
      ],
      "position": [
        46,
        -68
      ],
      "zoom": 1,
      "viewport": {
        "x": 46,
        "y": -68,
        "zoom": 1
      }
    }
}

export { iteratorFlow };
