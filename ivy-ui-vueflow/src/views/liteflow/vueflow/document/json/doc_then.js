function thenFlow() {
    return {
      "nodes": [
        {
          "id": "82a62861869f42d4aaa69e6e25e73fb6",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 234.5,
            "y": 128.484375
          },
          "data": {
            "id": "a",
            "type": "common",
            "name": "a",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "cfb20a703ff94c7fb05487e03164982e"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "c4eef310abdd44ad91c8dbd4968d0cff"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "4cc0e72b15494aaf9260cd25d0227b22"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "3b9be0d7228242d38043897be19552bd"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件",
          "targetPosition": "left",
          "sourcePosition": "right"
        },
        {
          "id": "319ebb74fef54fe788372de83c9f6012",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 596.5000000000001,
            "y": 128.48437500000006
          },
          "data": {
            "id": "b",
            "type": "common",
            "name": "b",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "b4f6f9e9a0b64821b1475aa7681dd101"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "8664fa49e2544e81a026d923b96eae57"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "f3192ed25d1045cf80e2bb502325b346"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "ceb451a158804da38a0f9cba6daa8b8c"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件",
          "targetPosition": "left",
          "sourcePosition": "right"
        },
        {
          "id": "67f572f41f7944b79c772a44a74dffc8",
          "type": "note",
          "initialized": false,
          "position": {
            "x": 58.45852373231867,
            "y": 60.85994619695606
          },
          "data": {
            "id": "6548752d4577439e8f1c090eaebd6a31",
            "type": "note",
            "name": "注释",
            "mode": "default",
            "nodeDataNote": {
              "label": "串行编排方式一\n不使用串行节点",
              "fontSize": 16,
              "showResizer": false
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "ef078f730a2d439b85bb3b3b3a3d3636"
                },
                {
                  "position": "right",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "7c0afb89c8dd4f55828c0bac3c09abd5"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "0898722908564909a11703cbdaa0d3e6"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "6721712e27a249229e964e42680944b6"
                }
              ]
            }
          },
          "label": "注释",
          "style": {
            "width": "168px",
            "height": "63px"
          }
        },
        {
          "id": "d140fc92807942aab63a4342b0501bf1",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 240.5,
            "y": 371.484375
          },
          "data": {
            "id": "c",
            "type": "common",
            "name": "c",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "d3b510853b8b4bedb037e72f46e3e5c6"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "db3a317b25f04b33b8ca3ee236c5f4c3"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "2674f4e252f143269c41e1ab02cfbbed"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "cf8649d8a45d4eab83e6a08f6eb5050a"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "734ca167aa984159b438e4fb7567b1e6",
          "type": "then",
          "initialized": false,
          "position": {
            "x": 105.69552497963696,
            "y": 435.405792167448
          },
          "data": {
            "id": "acd37131c8054ea79fccb1a9a87638fe",
            "type": "then",
            "name": "串行(未完成)",
            "mode": "default",
            "nodeDataThen": {
              "id": "cc"
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "aaf6217b983b4f5ca933b8aa500fd0d3"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "9c8a5d7f175145298df9765fea10eb03"
                }
              ]
            }
          },
          "label": "串行(未完成)"
        },
        {
          "id": "4b2730bb033d4a7daa6f9abb83cfd26c",
          "type": "note",
          "initialized": false,
          "position": {
            "x": 67.77357484976557,
            "y": 311.20787002190167
          },
          "data": {
            "id": "5a29a900e57645b48d6dcf14f99396d0",
            "type": "note",
            "name": "注释",
            "mode": "default",
            "nodeDataNote": {
              "label": "串行编排方式二\n以串行节点起始",
              "fontSize": 16,
              "showResizer": false,
              "imgPath": "/src/views/liteflow/vueflow/img/2.png"
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "f5c5ae3bfe044a32a045dc66f682348c"
                },
                {
                  "position": "right",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "aa084c7128284793859b89a898c3ba25"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "ed28b96102e0417eb2af87678946eeb7"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "ac610d9a5a03436a82b8091bc75432c7"
                }
              ]
            }
          },
          "label": "注释",
          "style": {
            "width": "154px",
            "height": "66px"
          }
        },
        {
          "id": "608559c7f2054816b0b787ded6f6a799",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 739.6813703807838,
            "y": 372.8038829107729
          },
          "data": {
            "id": "e",
            "type": "common",
            "name": "普通组件e",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "d0a4742a1ba1430ba435a1c6c63a9310"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "282a706048ea48569f77c31b6d793018"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "06e67b0292f04227b0a6c9d9f760d867"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "d8225cb8f2d84a1dab00de1a2ceff388"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "e7c4f96742c74cffb4578f947e8ff352",
          "type": "then",
          "initialized": false,
          "position": {
            "x": 613.2620381283225,
            "y": 433.984375
          },
          "data": {
            "id": "6c8d3bac2b09426686b5ee080835d06a",
            "type": "then",
            "name": "串行",
            "mode": "default",
            "nodeDataThen": {
              "id": "ee"
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "faec9931448447698486eb92de7b414f"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "90112f256e7748149e00a77a77b17346"
                }
              ]
            }
          },
          "label": "串行"
        },
        {
          "id": "e946d1cc5e7c4e6cac15d2f30631d9da",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 240.3922782691874,
            "y": 618.8167424315319
          },
          "data": {
            "id": "m",
            "type": "common",
            "name": "普通组件m",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "a70d7d79c498430796d65b9c3d9ce542"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "94ab588dabbe4a6089724c445e4db4a3"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "c2f108775fd14fae9acb346abf9f7d19"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "8063bc0c92cc4bfb87f0a753edc5b28a"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "af072c652d1b4ff5932b56317235dab7",
          "type": "then",
          "initialized": false,
          "position": {
            "x": 622.3922782691872,
            "y": 682.316742431532
          },
          "data": {
            "id": "dbf072b23ffa4d18863fc473e4f7fb3e",
            "type": "then",
            "name": "串行",
            "mode": "default",
            "nodeDataThen": {
              "id": "nn"
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "c5d76b063ed64161a13a0c23e1afed03"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "b0bb2a1cd7304ae0bc64721b73463aa1"
                }
              ]
            }
          },
          "label": "串行"
        },
        {
          "id": "bbe769fad5034729b49519caef749deb",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 741.3922782691868,
            "y": 619.816742431532
          },
          "data": {
            "id": "n",
            "type": "common",
            "name": "普通组件n",
            "mode": "default",
            "nodeDataBase": {},
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "d2f81cc6c7f54be880eabbcd8b5a0b34"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "6e39eb5b67cc492392f1e327f6ef837f"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "84db9ffbea4b4fcc9b1510e3677b416b"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "55b7c71d84ca42b1833a307d684a765e"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "2525b3a6b8a24428996774ca7102c21a",
          "type": "note",
          "initialized": false,
          "position": {
            "x": 62.2678494661435,
            "y": 538.9142819853967
          },
          "data": {
            "id": "d6ccdd9fd0df4d0a94eb5f54a7965afe",
            "type": "note",
            "name": "注释",
            "mode": "default",
            "nodeDataNote": {
              "label": "串行编排方式三\n非串行节点起始",
              "fontSize": 16,
              "showResizer": false,
              "imgPath": "/src/views/liteflow/vueflow/img/12.png"
            },
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "d765a833523d47d38024b3d6ce63495e"
                },
                {
                  "position": "right",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "0eed176965d744598befeba69407683c"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "1b748e28cceb44f48a938f59a5ea5555"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "78ad23d0c92642bd982e32ab6d24d3e8"
                }
              ]
            }
          },
          "label": "注释",
          "style": {
            "width": "161px",
            "height": "75px"
          }
        }
      ],
      "edges": [
        {
          "id": "798c5394076a4e4d8a27388451aecd19",
          "type": "animation",
          "source": "82a62861869f42d4aaa69e6e25e73fb6",
          "target": "319ebb74fef54fe788372de83c9f6012",
          "sourceHandle": "c4eef310abdd44ad91c8dbd4968d0cff",
          "targetHandle": "b4f6f9e9a0b64821b1475aa7681dd101",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 540.4998857404678,
          "sourceY": 208.98434909372412,
          "targetX": 590.4999751459493,
          "targetY": 208.98434909372418
        },
        {
          "id": "4906b6f7761d435d853cba45b2290224",
          "type": "animation",
          "source": "67f572f41f7944b79c772a44a74dffc8",
          "target": "82a62861869f42d4aaa69e6e25e73fb6",
          "sourceHandle": "6721712e27a249229e964e42680944b6",
          "targetHandle": "cfb20a703ff94c7fb05487e03164982e",
          "updatable": true,
          "data": {
            "type": "note"
          },
          "label": "注释",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 142.45850870726198,
          "sourceY": 126.8599142936701,
          "targetX": 228.50004525689764,
          "targetY": 208.98434909372412
        },
        {
          "id": "e09b76364f8b4d50a7f6f68919318320",
          "type": "animation",
          "source": "734ca167aa984159b438e4fb7567b1e6",
          "target": "d140fc92807942aab63a4342b0501bf1",
          "sourceHandle": "9c8a5d7f175145298df9765fea10eb03",
          "targetHandle": "d3b510853b8b4bedb037e72f46e3e5c6",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 144.69552145501936,
          "sourceY": 453.40581924710074,
          "targetX": 234.49997514594924,
          "targetY": 451.9843140382499
        },
        {
          "id": "71da65e56fc14197be26677723e89e5e",
          "type": "animation",
          "source": "4b2730bb033d4a7daa6f9abb83cfd26c",
          "target": "d140fc92807942aab63a4342b0501bf1",
          "sourceHandle": "ac610d9a5a03436a82b8091bc75432c7",
          "targetHandle": "d3b510853b8b4bedb037e72f46e3e5c6",
          "updatable": true,
          "data": {
            "type": "note"
          },
          "label": "注释",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 144.77357484976557,
          "sourceY": 380.20787002190167,
          "targetX": 234.49997514594924,
          "targetY": 451.9843140382499
        },
        {
          "id": "650ee29fd3034239b7f5f30e7289da06",
          "type": "animation",
          "source": "e7c4f96742c74cffb4578f947e8ff352",
          "target": "608559c7f2054816b0b787ded6f6a799",
          "sourceHandle": "90112f256e7748149e00a77a77b17346",
          "targetHandle": "d0a4742a1ba1430ba435a1c6c63a9310",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 652.2619995482307,
          "sourceY": 451.9843319687043,
          "targetX": 733.6814156376815,
          "targetY": 453.30382194902285
        },
        {
          "id": "fc8d3c0528e54b9291e5c1e26d348e86",
          "type": "animation",
          "source": "e946d1cc5e7c4e6cac15d2f30631d9da",
          "target": "af072c652d1b4ff5932b56317235dab7",
          "sourceHandle": "94ab588dabbe4a6089724c445e4db4a3",
          "targetHandle": "c5d76b063ed64161a13a0c23e1afed03",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 546.3922782691874,
          "sourceY": 699.3167119139538,
          "targetX": 619.3922782691872,
          "targetY": 700.316742431532
        },
        {
          "id": "5eaba9b1b0d0462c925430e43e302701",
          "type": "animation",
          "source": "af072c652d1b4ff5932b56317235dab7",
          "target": "bbe769fad5034729b49519caef749deb",
          "sourceHandle": "b0bb2a1cd7304ae0bc64721b73463aa1",
          "targetHandle": "d2f81cc6c7f54be880eabbcd8b5a0b34",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 661.3922782691872,
          "sourceY": 700.316742431532,
          "targetX": 735.3922782691868,
          "targetY": 700.3167119139539
        },
        {
          "id": "2e2b96851daf4da1858167b93bd3e8af",
          "type": "animation",
          "source": "2525b3a6b8a24428996774ca7102c21a",
          "target": "e946d1cc5e7c4e6cac15d2f30631d9da",
          "sourceHandle": "78ad23d0c92642bd982e32ab6d24d3e8",
          "targetHandle": "a70d7d79c498430796d65b9c3d9ce542",
          "updatable": true,
          "data": {
            "type": "note"
          },
          "label": "注释",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 142.76766313761647,
          "sourceY": 616.9141182327742,
          "targetX": 234.3922782691874,
          "targetY": 699.3167119139538
        },
        {
          "id": "bb48ca0c2c2a4e6182c45b0253885863",
          "type": "animation",
          "source": "d140fc92807942aab63a4342b0501bf1",
          "target": "e7c4f96742c74cffb4578f947e8ff352",
          "sourceHandle": "db3a317b25f04b33b8ca3ee236c5f4c3",
          "targetHandle": "faec9931448447698486eb92de7b414f",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 546.4998857404678,
          "sourceY": 451.9843140382499,
          "targetX": 610.261990645823,
          "targetY": 451.9843319687043
        }
      ],
      "position": [
        188.0238531914344,
        -21.94366448882016
      ],
      "zoom": 0.7578582832551997,
      "viewport": {
        "x": 188.0238531914344,
        "y": -21.94366448882016,
        "zoom": 0.7578582832551997
      }
    }
}


export { thenFlow };

