function contextFlow(){
    return {
        "nodes": [
          {
            "id": "63e261a4c6ed4130b9ed8982f555a88e",
            "type": "context",
            "initialized": false,
            "position": {
              "x": 184.12701056931536,
              "y": 231.46565385842644
            },
            "data": {
              "id": "8fa3332c1b8f45a6b72138547a33de0b",
              "type": "context",
              "name": "数据上下文(未完成)",
              "mode": "default",
              "nodeDataContext": {
                "fullClassName": "com.ivy.OrderContext",
                "asName": "aaa"
              },
              "style": {
                "handles": []
              }
            },
            "label": "数据上下文(未完成)"
          },
          {
            "id": "3a10a43d58974e938c5b5c8ad96ecce2",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 81.6920618888808,
              "y": 357.03445482786395
            },
            "data": {
              "id": "a",
              "type": "common",
              "name": "普通组件a",
              "mode": "default",
              "nodeDataBase": {
                "retryExceptions": []
              },
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "d172b5a45eb7451e90e4719ebba2b918"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "c840314c3b5048c4ae19825077245b5c"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "5c64e21aeae743b58472a1339106f432"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "95a6781f75764415a53c68b6a71582ac"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "48111a13797a441195dcc02de8cb78b6",
            "type": "context",
            "initialized": false,
            "position": {
              "x": 313.18224867324096,
              "y": 231.93475368782896
            },
            "data": {
              "id": "d4c88754b7d14b93a812ffa8018230cb",
              "type": "context",
              "name": "数据上下文(未完成)",
              "mode": "default",
              "nodeDataContext": {
                "asName": "bbb",
                "fullClassName": "com.ivy.UserContext"
              },
              "style": {
                "handles": []
              }
            },
            "label": "数据上下文(未完成)"
          },
          {
            "id": "faf045cf928141e8a7efa1c876e7344e",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 507.98875375156024,
              "y": 357.44972181364346
            },
            "data": {
              "id": "b",
              "type": "common",
              "name": "普通组件b",
              "mode": "default",
              "nodeDataBase": {
                "retryExceptions": []
              },
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "ce20d88eb93b44e4a2bd4d93cca0a9ba"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "6ec133f860114186a1ee79c929a9fccb"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "f409307bbc4541b0b62e4fc7f4212085"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "cc13bc73f3c246d8b967464dde3a8aaf"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          }
        ],
        "edges": [
          {
            "id": "1b8415d78cba4f2c9c11194cff636796",
            "type": "animation",
            "source": "3a10a43d58974e938c5b5c8ad96ecce2",
            "target": "faf045cf928141e8a7efa1c876e7344e",
            "sourceHandle": "c840314c3b5048c4ae19825077245b5c",
            "targetHandle": "ce20d88eb93b44e4a2bd4d93cca0a9ba",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 387.69157893261297,
            "sourceY": 437.53436920629565,
            "targetX": 501.9887588878182,
            "targetY": 437.9496130641124
          }
        ],
        "position": [
          -59.458609347540346,
          -210.1546181001379
        ],
        "zoom": 1.3195079107728942,
        "viewport": {
          "x": -59.458609347540346,
          "y": -210.1546181001379,
          "zoom": 1.3195079107728942
        }
      }
}

export { contextFlow };
