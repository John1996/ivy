function ifFlow(){
    return {
        "nodes": [
          {
            "id": "3ad8a81ed9e2424892db5e20022aaf6f",
            "type": "if",
            "initialized": false,
            "position": {
              "x": 91.5,
              "y": 140.5
            },
            "data": {
              "id": "9557dd3bc0f3404e814e41921bd03719",
              "type": "if",
              "name": "条件组件",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "b3aa5f8c331e4cfab5188862ac98d758"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "cf3b70edf6244ac1a445ae4e4e60cefe"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "148431171d5048bfbcd53c7fb41b1d89"
                  },
                  {
                    "position": "bottom",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "ae1a97e041ae4ed6bd4c419b5a259ed5"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataIf": {}
            },
            "label": "条件组件"
          },
          {
            "id": "cd5c87f1b65a4503844e27c1c070c655",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 632.304920892271,
              "y": 275.7165204897033
            },
            "data": {
              "id": "d",
              "type": "common",
              "name": "普通组件d",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "b235b0f67b364127a2a3ce6cfdb373bb"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "821d9e89463b406fad626e8d13e031ec"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "66e413ff83ea47e698e73dca133ed653"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "856dd235ac2f45178a3920050fa7785e"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "bc02b12ba264454e922ddd2510ed5465",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 633.1513180522234,
              "y": 466.4384888466118
            },
            "data": {
              "id": "e",
              "type": "common",
              "name": "普通组件e",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "b114bcbafbac48beb29ed887f3c104c2"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "3b292c2c2e9d47b49190d771aa0cea7d"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "2c0021cdbd3d45e0809a490c1203645e"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "586045afdfea4be8b5b0d4e43b5842bc"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "9c286a77039f4d60b6d0a52980c739f8",
            "type": "common",
            "initialized": false,
            "position": {
              "x": 88.5,
              "y": 460.5
            },
            "data": {
              "id": "f",
              "type": "common",
              "name": "普通组件f",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "15b99b938b5b41d5afd4288960fd5b6c"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "517cc8bc27794ee3942daf77e5201a34"
                  },
                  {
                    "position": "top",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "040b3f1af33b4d049eeec8c2180bc5ad"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "cc4170c4853d46399fffd433a31dc6fe"
                  }
                ],
                "extendHandles": []
              }
            },
            "label": "普通组件"
          },
          {
            "id": "2f8542090da946ccaefcd23c3100d532",
            "type": "and",
            "initialized": false,
            "position": {
              "x": 524.6538557133474,
              "y": -41.63111909431191
            },
            "data": {
              "id": "7e340f9c23ab4bada95ed776fcb2572b",
              "type": "and",
              "name": "与",
              "mode": "default",
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "7439473c98fd4603aee6bb027d037529"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "1371dfb6b13343b6add9cfba7e5919dc"
                  }
                ]
              },
              "nodeDataAnd": {},
              "nodeDataThen": {}
            },
            "label": "与"
          },
          {
            "id": "bd08de7269d04b83b7c9435b8af90452",
            "type": "or",
            "initialized": false,
            "position": {
              "x": 709.3849632215528,
              "y": -123.44060956223136
            },
            "data": {
              "id": "a1a8063e62da48f1a255e59fc6a3a495",
              "type": "or",
              "name": "或",
              "mode": "default",
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "88977a1225c44d65b2e089d69f006261"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "50e9b8b5b93040dfa5f687cbe61038b0"
                  }
                ]
              },
              "nodeDataOr": {},
              "nodeDataThen": {}
            },
            "label": "或"
          },
          {
            "id": "d0dbc4115b6748dba6721b76cbaca767",
            "type": "not",
            "initialized": false,
            "position": {
              "x": 709.3849632215527,
              "y": 79.76360869679436
            },
            "data": {
              "id": "c3970a97996547928d625b3046c382de",
              "type": "not",
              "name": "非",
              "mode": "default",
              "style": {
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "a9af37a21fbe4d6496879cfef7232841"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "4d3ea1cd0c534182b7977b56d4cdc382"
                  }
                ]
              },
              "nodeDataNot": {},
              "nodeDataThen": {}
            },
            "label": "非"
          },
          {
            "id": "ee2266095da644fa8792ac77768c6c86",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 932.3325912194612,
              "y": -266.70862376246953
            },
            "data": {
              "id": "a",
              "type": "boolean",
              "name": "布尔组件a",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "bcbd219ef84f45c6b2bcb26a28deb616"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "ebb52f449bd64a7f937eaf5b88569a7e"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "db299990659f4c0c818e7c04e45436c8"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "b439358f5c1d4674bdd10284e90f393e"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          },
          {
            "id": "fa77c1c01d8341d48e2b1edad4e494bf",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 934.971607041007,
              "y": -85.93603998658298
            },
            "data": {
              "id": "b",
              "type": "boolean",
              "name": "布尔组件b",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "f5cd360c0e5e475eb0862a84cb1d3525"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "9c6a2300fd3a4efcb9f9e7c51adcd189"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "cc477e186b3c40fa995aad0534ab6da9"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "abd2fd8564944b7bb681077c1fc40963"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          },
          {
            "id": "35e3d658a600410e958e69ad7fdb592a",
            "type": "boolean",
            "initialized": false,
            "position": {
              "x": 934.971607041007,
              "y": 96.15605170007652
            },
            "data": {
              "id": "c",
              "type": "boolean",
              "name": "布尔组件c",
              "mode": "default",
              "nodeDataBase": {},
              "style": {
                "toolbar": {
                  "position": "top",
                  "showIcon": false,
                  "offset": 10
                },
                "handles": [
                  {
                    "position": "left",
                    "opacity": 1,
                    "type": "target",
                    "style": "",
                    "id": "9838b96312ac4618bd4eae5266e73286"
                  },
                  {
                    "position": "right",
                    "opacity": 1,
                    "type": "source",
                    "style": "",
                    "id": "5cb5eddb683c4a2fae2fc34a2e676fec"
                  },
                  {
                    "position": "top",
                    "opacity": 0,
                    "type": "target",
                    "style": "",
                    "id": "888c31cd89754137881caf0d4155ec23"
                  },
                  {
                    "position": "bottom",
                    "opacity": 0,
                    "type": "source",
                    "style": "",
                    "id": "337782177ab744189ece186344d9fedf"
                  }
                ],
                "extendHandles": []
              },
              "nodeDataBoolean": {}
            },
            "label": "布尔组件"
          }
        ],
        "edges": [
          {
            "id": "3449a3f913a1483a899e0d8e612dda0d",
            "type": "animation",
            "source": "3ad8a81ed9e2424892db5e20022aaf6f",
            "target": "cd5c87f1b65a4503844e27c1c070c655",
            "sourceHandle": "cf3b70edf6244ac1a445ae4e4e60cefe",
            "targetHandle": "b235b0f67b364127a2a3ce6cfdb373bb",
            "updatable": true,
            "data": {
              "type": "true"
            },
            "label": "true",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 397.5001474808358,
            "sourceY": 216.00005335762108,
            "targetX": 626.3049589724588,
            "targetY": 356.21651526836735
          },
          {
            "id": "31a4da1f427a4bc6a9efa93b53445596",
            "type": "animation",
            "source": "3ad8a81ed9e2424892db5e20022aaf6f",
            "target": "bc02b12ba264454e922ddd2510ed5465",
            "sourceHandle": "cf3b70edf6244ac1a445ae4e4e60cefe",
            "targetHandle": "b114bcbafbac48beb29ed887f3c104c2",
            "updatable": true,
            "data": {
              "type": "false"
            },
            "label": "false",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 397.5001474808358,
            "sourceY": 216.00005335762108,
            "targetX": 627.1513561324113,
            "targetY": 546.9384836252758
          },
          {
            "id": "42d7b898a00c48c3adac32da7afee3e2",
            "type": "animation",
            "source": "3ad8a81ed9e2424892db5e20022aaf6f",
            "target": "9c286a77039f4d60b6d0a52980c739f8",
            "sourceHandle": "ae1a97e041ae4ed6bd4c419b5a259ed5",
            "targetHandle": "040b3f1af33b4d049eeec8c2180bc5ad",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 241.50001005609724,
            "sourceY": 297.50007301108025,
            "targetX": 238.50001005609724,
            "targetY": 454.50003808018784
          },
          {
            "id": "9801e5e0c4954521b276a244e2d8e968",
            "type": "animation",
            "source": "3ad8a81ed9e2424892db5e20022aaf6f",
            "target": "2f8542090da946ccaefcd23c3100d532",
            "sourceHandle": "cf3b70edf6244ac1a445ae4e4e60cefe",
            "targetHandle": "7439473c98fd4603aee6bb027d037529",
            "updatable": true,
            "data": {
              "type": "boolean"
            },
            "label": "boolean",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 397.5001474808358,
            "sourceY": 216.00005335762108,
            "targetX": 521.6538747534413,
            "targetY": -23.631133758379917
          },
          {
            "id": "dd856671134b41e1a08e62e35e75e578",
            "type": "animation",
            "source": "2f8542090da946ccaefcd23c3100d532",
            "target": "bd08de7269d04b83b7c9435b8af90452",
            "sourceHandle": "1371dfb6b13343b6add9cfba7e5919dc",
            "targetHandle": "88977a1225c44d65b2e089d69f006261",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 563.6538073451175,
            "sourceY": -23.631133758379917,
            "targetX": 706.3849822616467,
            "targetY": -105.44058395809856
          },
          {
            "id": "355a70984d5d455583047383dc37fabc",
            "type": "animation",
            "source": "2f8542090da946ccaefcd23c3100d532",
            "target": "d0dbc4115b6748dba6721b76cbaca767",
            "sourceHandle": "1371dfb6b13343b6add9cfba7e5919dc",
            "targetHandle": "a9af37a21fbe4d6496879cfef7232841",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 563.6538073451175,
            "sourceY": -23.631133758379917,
            "targetX": 706.3849822616465,
            "targetY": 97.76359403272636
          },
          {
            "id": "cccad397ddc64b238a1aa4eab3050bac",
            "type": "animation",
            "source": "bd08de7269d04b83b7c9435b8af90452",
            "target": "ee2266095da644fa8792ac77768c6c86",
            "sourceHandle": "50e9b8b5b93040dfa5f687cbe61038b0",
            "targetHandle": "bcbd219ef84f45c6b2bcb26a28deb616",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 748.3849148533228,
            "sourceY": -105.44058395809856,
            "targetX": 926.332629299649,
            "targetY": -191.20859053894884
          },
          {
            "id": "b91575d2677845ea99c92bb057572e76",
            "type": "animation",
            "source": "bd08de7269d04b83b7c9435b8af90452",
            "target": "fa77c1c01d8341d48e2b1edad4e494bf",
            "sourceHandle": "50e9b8b5b93040dfa5f687cbe61038b0",
            "targetHandle": "f5cd360c0e5e475eb0862a84cb1d3525",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 748.3849148533228,
            "sourceY": -105.44058395809856,
            "targetX": 928.9716451211949,
            "targetY": -10.436026897162705
          },
          {
            "id": "83a6c02ac4e9450aac95026d45c13a3e",
            "type": "animation",
            "source": "d0dbc4115b6748dba6721b76cbaca767",
            "target": "35e3d658a600410e958e69ad7fdb592a",
            "sourceHandle": "4d3ea1cd0c534182b7977b56d4cdc382",
            "targetHandle": "9838b96312ac4618bd4eae5266e73286",
            "updatable": true,
            "data": {
              "type": "common"
            },
            "label": "",
            "animated": true,
            "markerEnd": "arrowclosed",
            "showToolbar": false,
            "sourceX": 748.3849148533227,
            "sourceY": 97.76359403272636,
            "targetX": 928.9716451211949,
            "targetY": 171.65606478949678
          }
        ],
        "position": [
          63.62445418722041,
          215.6785640232588
        ],
        "zoom": 0.7578582832551991,
        "viewport": {
          "x": 63.62445418722041,
          "y": 215.6785640232588,
          "zoom": 0.7578582832551991
        }
      }
}

export { ifFlow };
