function routerFlow(){
    return {
      "nodes": [
        {
          "id": "9b4ff414c27045a7915f80ed17ef34c3",
          "type": "router",
          "initialized": false,
          "position": {
            "x": 177.7081206946768,
            "y": 85.66476616552481
          },
          "data": {
            "id": "5038ed42418f4cedab6f6f376455f579",
            "type": "router",
            "name": "策略路由",
            "mode": "default",
            "nodeDataRouter": {
              "namespace": "router1"
            },
            "style": {
              "handles": [
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "059e0e8b0c7642c99e088eec11b30eea"
                }
              ]
            }
          },
          "label": "策略路由"
        },
        {
          "id": "3104430af1af47ef8a390e97ad1fd6f6",
          "type": "and",
          "initialized": false,
          "position": {
            "x": 362.6485558491995,
            "y": 84.51606781052777
          },
          "data": {
            "id": "fee8b696241a441b84a5ae09f7ac8f92",
            "type": "and",
            "name": "与",
            "mode": "default",
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "6c4b0043bd164971984f973aa7a30b95"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "3d67c2e2200549e6a07b0ee3dfb9935a"
                }
              ]
            },
            "nodeDataAnd": {},
            "nodeDataThen": {}
          },
          "label": "与"
        },
        {
          "id": "a68a231133b84e2883b431a68a8514bb",
          "type": "or",
          "initialized": false,
          "position": {
            "x": 538.3994041637459,
            "y": 36.27073690065228
          },
          "data": {
            "id": "7593e656c0e446e2a2e931df72119a93",
            "type": "or",
            "name": "或",
            "mode": "default",
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "19b60992f0b04d0fb30ec6ccde48e8b3"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "1c84e13fbefb40d79f711d633a441fa0"
                }
              ]
            },
            "nodeDataOr": {},
            "nodeDataThen": {}
          },
          "label": "或"
        },
        {
          "id": "7dce77d77a3f4c9aacff03aefbb911cb",
          "type": "not",
          "initialized": false,
          "position": {
            "x": 534.9533090987547,
            "y": 248.77993257510377
          },
          "data": {
            "id": "877bf959282f49799c1c40bc1e61e379",
            "type": "not",
            "name": "非",
            "mode": "default",
            "style": {
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "2a86d6a6a8f740cd9fa4ca25b9fce8c9"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "7105f338ccc4483d976b9b5320adf67f"
                }
              ]
            },
            "nodeDataNot": {},
            "nodeDataThen": {}
          },
          "label": "非"
        },
        {
          "id": "025a404dd91540db9e96dcf181564536",
          "type": "boolean",
          "initialized": false,
          "position": {
            "x": 698.1687863329928,
            "y": -31.16146547741775
          },
          "data": {
            "id": "a",
            "type": "boolean",
            "name": "布尔组件a",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "d74aa1b04ab24623ad2cf2042813173b"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "71cda122d4b445e8bab9f09ee86334cf"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "4cb0490ca51c447ba57d7658b62675c4"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "c83ab94f98704d2fbf70d5279dbd96c3"
                }
              ],
              "extendHandles": []
            },
            "nodeDataBoolean": {}
          },
          "label": "布尔组件"
        },
        {
          "id": "aee51a081d7f4d2ca508a23869780e26",
          "type": "boolean",
          "initialized": false,
          "position": {
            "x": 694.7226912680017,
            "y": 132.69631647025497
          },
          "data": {
            "id": "b",
            "type": "boolean",
            "name": "布尔组件b",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "1325111b12534214b5f72e245534ea23"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "0e03841631eb4987b2b6e30afe7dd90d"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "feb9f758dc7d4f4693978da684479993"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "755e36bcc96c452293169cc0a5c3531c"
                }
              ],
              "extendHandles": []
            },
            "nodeDataBoolean": {}
          },
          "label": "布尔组件"
        },
        {
          "id": "0616004a45c948768946ee6319fe78dc",
          "type": "boolean",
          "initialized": false,
          "position": {
            "x": 694.7226912680017,
            "y": 301.55497465481915
          },
          "data": {
            "id": "c",
            "type": "boolean",
            "name": "布尔组件c",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "ca7bae13e4874721adfbedd8b895871b"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "d617a61356624de38180dd98c9e4f5ae"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "6e3f0eda2c85480fb159529826fbe0a8"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "6365788e53df4537978eb78c61e825e2"
                }
              ],
              "extendHandles": []
            },
            "nodeDataBoolean": {}
          },
          "label": "布尔组件"
        },
        {
          "id": "72c297c54a2f4935954bc2607b89193e",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 306.49006816376004,
            "y": 487.07265216039946
          },
          "data": {
            "id": "m",
            "type": "common",
            "name": "普通组件m",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "1022c436a8e841f49b4455c6dcdc7658"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "1684f3394e884aac854fda93d71c79f6"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "561707fb5a894b839fe27bd6cd5de77f"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "d681b4f5d5f244bcaafd3889997de238"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        },
        {
          "id": "bc4c9b29b2dd494690606ecb5b670930",
          "type": "common",
          "initialized": false,
          "position": {
            "x": 693.9089037373925,
            "y": 485.4115251380749
          },
          "data": {
            "id": "n",
            "type": "common",
            "name": "普通组件n",
            "mode": "default",
            "nodeDataBase": {
              "retryExceptions": []
            },
            "style": {
              "toolbar": {
                "position": "top",
                "showIcon": false,
                "offset": 10
              },
              "handles": [
                {
                  "position": "left",
                  "opacity": 1,
                  "type": "target",
                  "style": "",
                  "id": "e6b6f48928934b7b8d69eb5f051a699c"
                },
                {
                  "position": "right",
                  "opacity": 1,
                  "type": "source",
                  "style": "",
                  "id": "340b0e0c4c824c729aebc35d49a85841"
                },
                {
                  "position": "top",
                  "opacity": 0,
                  "type": "target",
                  "style": "",
                  "id": "94bb84bcd0f04e9dbc26ee225eabcbee"
                },
                {
                  "position": "bottom",
                  "opacity": 0,
                  "type": "source",
                  "style": "",
                  "id": "ceef0af60246476f9f456ebe38e61636"
                }
              ],
              "extendHandles": []
            }
          },
          "label": "普通组件"
        }
      ],
      "edges": [
        {
          "id": "dc5c0c00e2f740fd91eed997cbcdf9b7",
          "type": "animation",
          "source": "9b4ff414c27045a7915f80ed17ef34c3",
          "target": "3104430af1af47ef8a390e97ad1fd6f6",
          "sourceHandle": "059e0e8b0c7642c99e088eec11b30eea",
          "targetHandle": "6c4b0043bd164971984f973aa7a30b95",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 216.708082114585,
          "sourceY": 103.66475818970332,
          "targetX": 359.64857847764836,
          "targetY": 102.51605983470628
        },
        {
          "id": "6664e38a96e04d478e28aeca8100c864",
          "type": "animation",
          "source": "3104430af1af47ef8a390e97ad1fd6f6",
          "target": "a68a231133b84e2883b431a68a8514bb",
          "sourceHandle": "3d67c2e2200549e6a07b0ee3dfb9935a",
          "targetHandle": "19b60992f0b04d0fb30ec6ccde48e8b3",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 401.6485172691077,
          "sourceY": 102.51605983470628,
          "targetX": 535.3994267921947,
          "targetY": 54.27072892483079
        },
        {
          "id": "2b4463ab4f9d4ce6a89420e1c82e3d24",
          "type": "animation",
          "source": "3104430af1af47ef8a390e97ad1fd6f6",
          "target": "7dce77d77a3f4c9aacff03aefbb911cb",
          "sourceHandle": "3d67c2e2200549e6a07b0ee3dfb9935a",
          "targetHandle": "2a86d6a6a8f740cd9fa4ca25b9fce8c9",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 401.6485172691077,
          "sourceY": 102.51605983470628,
          "targetX": 531.9533317272036,
          "targetY": 266.77992459928225
        },
        {
          "id": "de19dfb790244531a6eeb999f185eea7",
          "type": "animation",
          "source": "a68a231133b84e2883b431a68a8514bb",
          "target": "025a404dd91540db9e96dcf181564536",
          "sourceHandle": "1c84e13fbefb40d79f711d633a441fa0",
          "targetHandle": "d74aa1b04ab24623ad2cf2042813173b",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 577.3993655836541,
          "sourceY": 54.27072892483079,
          "targetX": 692.1687863329928,
          "targetY": 44.33853452258225
        },
        {
          "id": "a9bb25021f884e7c813568f5eec96855",
          "type": "animation",
          "source": "a68a231133b84e2883b431a68a8514bb",
          "target": "aee51a081d7f4d2ca508a23869780e26",
          "sourceHandle": "1c84e13fbefb40d79f711d633a441fa0",
          "targetHandle": "1325111b12534214b5f72e245534ea23",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 577.3993655836541,
          "sourceY": 54.27072892483079,
          "targetX": 688.7226912680017,
          "targetY": 208.19631647025497
        },
        {
          "id": "f2ff9bb0aea14758b2b7d1d5992db103",
          "type": "animation",
          "source": "7dce77d77a3f4c9aacff03aefbb911cb",
          "target": "0616004a45c948768946ee6319fe78dc",
          "sourceHandle": "7105f338ccc4483d976b9b5320adf67f",
          "targetHandle": "ca7bae13e4874721adfbedd8b895871b",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 573.953270518663,
          "sourceY": 266.77992459928225,
          "targetX": 688.7226912680017,
          "targetY": 377.054944137241
        },
        {
          "id": "affc70ae96cd4f38b1fed2304a0a767a",
          "type": "animation",
          "source": "72c297c54a2f4935954bc2607b89193e",
          "target": "bc4c9b29b2dd494690606ecb5b670930",
          "sourceHandle": "1684f3394e884aac854fda93d71c79f6",
          "targetHandle": "e6b6f48928934b7b8d69eb5f051a699c",
          "updatable": true,
          "data": {
            "type": "common"
          },
          "label": "",
          "animated": true,
          "markerEnd": "arrowclosed",
          "showToolbar": false,
          "sourceX": 612.490175376395,
          "sourceY": 567.5727274754651,
          "targetX": 687.9089418175804,
          "targetY": 565.9116004531405
        }
      ],
      "position": [
        93.04675964902756,
        55.890065122741134
      ],
      "zoom": 0.757858283255199,
      "viewport": {
        "x": 93.04675964902756,
        "y": 55.890065122741134,
        "zoom": 0.757858283255199
      }
    }
}
export { routerFlow };

