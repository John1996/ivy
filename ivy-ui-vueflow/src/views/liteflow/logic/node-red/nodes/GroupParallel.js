import { GroupNode } from "@logicflow/extension"

// 并行分组
class GroupParallel extends GroupNode.view {

}

class GroupParallelModel extends GroupNode.model {
  // 自定义形状
  initNodeData(data) {
    super.initNodeData(data)

    //是否限制分组子节点拖出分组，默认 false
    this.isRestrict = false

    //分组是否支持手动调整大小，默认 false
    this.resizable = true

    //分组是否显示展开收起按钮，默认 false
    this.foldable = true

    //分组宽度
    this.width = 300

    //分组高度
    this.height = 200

    //分组折叠后的宽度
    this.foldedWidth = 50

    //分组折叠后的高度
    this.foldedHeight = 50

    //isFolded	boolean	只读，表示分组是否被折叠。
    //isGroup	boolean	只读，永远为 true, 用于识别model为group
  }
  getNodeStyle() {
    const style = super.getNodeStyle()

    style.stroke = "#AEAFAE"
    style.strokeDasharray = "3 3"
    style.strokeWidth = 1
    
    return style
  }
}

export default {
  type: 'GroupParallel',
  view: GroupParallel,
  model: GroupParallelModel,
}
