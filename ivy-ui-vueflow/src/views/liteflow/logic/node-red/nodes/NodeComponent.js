import NodeComponentSVG from '@/views/liteflow/logic/node-red/images/NodeComponent.svg'
import { h } from '@logicflow/core'
import BaseNode from "./BaseNode"

class NodeComponentNode extends BaseNode.view {
  getIcon () {
    const {
      width,
      height,
    } = this.props.model

    return h('image', {
      width: height / 1.5,
      height: height / 1.5,
      x: - width / 2 + 7.5,
      y: - height / 2 + 7.5,
      href: NodeComponentSVG,
    })
  }
}

class NodeComponentModel extends BaseNode.model {
  initNodeData (data) {
    super.initNodeData(data)
    this.defaultFill = 'rgb(255, 255, 255)'
  }
}

export default {
  type: 'common',
  model: NodeComponentModel,
  view: NodeComponentNode,
}
