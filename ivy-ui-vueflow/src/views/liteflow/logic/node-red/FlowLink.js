import { BezierEdge, BezierEdgeModel } from "@logicflow/core"

class FlowLinkModel extends BezierEdgeModel {
  getEdgeStyle() {
    const style = super.getEdgeStyle()

    style.strokeWidth = 3
    style.stroke = this.isSelected ? '#ff7f0e' : '#999'
    
    return style
  }

  // 初始化数据
  initEdgeData (data) {
    super.initEdgeData(data)
    data.properties.linkType = 0
  }
  // getData () {
  //   const data = super.getData()
  //   if(!data.linkType){
  //     data.properties.linkType = 0
  //   }
  //   return data
  // }
}
class FlowLink extends BezierEdge {}

export default {
  type: 'flow-link',
  view: FlowLink,
  model: FlowLinkModel,
}
