import { createApp } from 'vue'
import FlowLink from "./FlowLink"

import NodeComponent from './nodes/NodeComponent'
import NodeForComponent from './nodes/NodeForComponent'
import NodeIfComponent from './nodes/NodeIfComponent'
import NodeIteratorComponent from './nodes/NodeIteratorComponent'
import NodeSwitchComponent from './nodes/NodeSwitchComponent'
import NodeWhileComponent from './nodes/NodeWhileComponent'

import GroupParallel from './nodes/GroupParallel'
import GroupSerial from './nodes/GroupSerial'
import NodeBreakComponent from './nodes/NodeBreakComponent'
import NodeEndComponent from './nodes/NodeEndComponent'
import NodeFallbackComponent from './nodes/NodeFallbackComponent'
import NodeFinallyComponent from './nodes/NodeFinallyComponent'
import NodePreComponent from './nodes/NodePreComponent'
import NodeScriptComponent from './nodes/NodeScriptComponent'
import NodeStartComponent from './nodes/NodeStartComponent'
import StartNode from "./nodes/StartNode"
import VueHtmlNode from './nodes/VueHtmlNode'
import Palette from './tools/Palette.vue'

class NodeRedExtension {
  static pluginName = 'NodeRedExtension'
  constructor ({ lf }) {
    lf.register(GroupParallel)
    lf.register(GroupSerial)
    lf.register(StartNode)
    lf.register(FlowLink)
    lf.register(VueHtmlNode)
    lf.register(NodeComponent)
    lf.register(NodeSwitchComponent)
    lf.register(NodeIfComponent)
    lf.register(NodeForComponent)
    lf.register(NodeWhileComponent)
    lf.register(NodeIteratorComponent)
    lf.register(NodeBreakComponent)
    lf.register(NodeStartComponent)
    lf.register(NodeEndComponent)
    lf.register(NodePreComponent)
    lf.register(NodeFinallyComponent)
    lf.register(NodeFallbackComponent)
    lf.register(NodeScriptComponent)
    lf.setDefaultEdgeType('flow-link')
    this.app = createApp(Palette, {
      lf,
    })
  }
  render(lf, domOverlay) {
    const node = document.createElement('div')

    node.className = 'node-red-palette'
    domOverlay.appendChild(node)
    this.app.mount(node)
  }
}

export default NodeRedExtension
