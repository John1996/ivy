import cn from './cn'
import en from './en'
import pt from './pt_br'

export default {
  en,
  cn,
  pt,
}
