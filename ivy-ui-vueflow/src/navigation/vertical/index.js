// import appsAndPages from './apps-and-pages'
import beetlsql from './beetlsql'
// import charts from './charts'
// import dashboard from './dashboard'
// import forms from './forms'
import liteflow from './liteflow'
// import others from './others'
// import uiElements from './ui-elements'

export default [
    // ...dashboard, 
    // ...appsAndPages, 
    // ...uiElements, 
    // ...forms, 
    // ...charts, 
    // ...others,
    ...beetlsql,
    ...liteflow,
    // ...monitor,
    // ...ai,
]
