export default [
  {
    title: '监控',
    icon: { icon: 'tabler-heart-rate-monitor' },
    children: [
      {
        title: '系统监控',
        to: 'monitor-os',
        icon: { icon: 'tabler-brand-windows' },
      },
      {
        title: 'JVM监控',
        to: 'monitor-jvm',
        icon: { icon: 'tabler-hexagon-letter-j' },
      },
    ],
    // badgeContent: '3',
    badgeClass: 'bg-primary',
  },
]
      