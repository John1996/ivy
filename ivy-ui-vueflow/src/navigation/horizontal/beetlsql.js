export default [
  {
    title: '数据库管理',
    icon: { icon: 'tabler-database' },
    children: [
      
      {
        title: '连接数据库',
        to: 'beetlsql-database',
        icon: { icon: 'tabler-link' },
      },
      {
        title: '动态数据源管理',
        to: 'beetlsql-datasource',
        icon: { icon: 'tabler-brand-mysql' },
      },
      {
        title: 'SQLManager管理',
        to: 'beetlsql-sqlmanager',
        icon: { icon: 'tabler-database' },
      },
      {
        title: '动态SQL管理',
        to: 'beetlsql-sql',
        icon: { icon: 'tabler-sql' },
      },
      {
        title: '数据流',
        to: 'beetlsql-stream',
        icon: { icon: 'tabler-arrow-move-right' },
      },
      
    ],
  },
]
