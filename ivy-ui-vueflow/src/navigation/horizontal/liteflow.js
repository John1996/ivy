export default [
  {
    title: 'LiteFlow',
    icon: { icon: 'tabler-smart-home' },
    children: [
      /* {
        title: '快速开始',
        to: 'liteflow-start',
        icon: { icon: 'tabler-player-play' },
      }, */
      {
        title: '动态类管理',
        to: 'liteflow-cla',
        icon: { icon: 'tabler-ad-2' },
      },
      /* {
        title: '组件模板',
        to: 'liteflow-cmp',
        icon: { icon: 'tabler-box' },
      }, */
      /* {
        title: '流程编排',
        to: 'liteflow-el',
        icon: { icon: 'tabler-chart-arrows' },
      },*/
      {
        title: '任务调度',
        to: 'liteflow-task',
        icon: { icon: 'tabler-calendar' },
      },
      /*{
        title: '链路追踪',
        to: 'liteflow-link',
        icon: { icon: 'tabler-arrow-guide' },
      }, */
      /* {
        title: 'liteflow-logic',
        to: 'liteflow-logic',
        icon: { icon: 'tabler-flower' },
      }, */
      {
        title: 'liteflow-vueflow',
        to: 'liteflow-vueflow',
        icon: { icon: 'tabler-flower' },
      },
    ],
  },
]
