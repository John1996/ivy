export default [
  {
    title: 'AI',
    icon: { icon: 'tabler-brand-github-copilot' },
    children: [
      {
        title: 'ChatGPT',
        to: 'ai-chatgpt',
        icon: { icon: 'tabler-brand-openai' },
      },
        
    ],
  },
]
  