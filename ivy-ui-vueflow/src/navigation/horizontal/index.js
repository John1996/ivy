
// import apps from './apps'
import beetlsql from './beetlsql'

// import charts from './charts'
// import dashboard from './dashboard'
import liteflow from './liteflow'

// import misc from './misc'

// import pages from './pages'
// import tables from './tables'
// import forms from './forms'
// import uiElements from './ui-elements'

export default [
  // ...dashboard, 
  // ...apps, 
  // ...pages, 
  // ...uiElements, 
  // ...forms, 
  // ...tables, 
  // ...charts, 
  // ...misc,
  ...beetlsql,
  ...liteflow,
  // ...monitor,
  // ...ai,
]
