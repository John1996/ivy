# SpringBoot

运行SpringBoot的jar项目的Docker容器(JRE11)。

使用方法:

[//]: # (1. 把使用编译好的springboot的jar部署包放到app目录下即可。（不要出现多个jar的情况，否则不保证得到你想要的运行结果，因为会默认执行其中一个。）)

[//]: # (2. 为了方便修改配置，默认在 `docker-compose.yml` 下配置了加载宿主机器中的 `config/application.yml`。如果不是你想要的，请自行修改。)

[//]: # (3. 运行命令 `docker-compose up -d`)

[//]: # (4.  `alpine` 环境容器，因为比较小，但缺点是中文支持不了，也不适用于一些对服务器运行的本地开发环境比较高依赖的应用。而`ubuntu`版本的容器支持中文，容器内资源比较丰富，体积也大了许多。)

安装mysql镜像：

```
docker pull mysql:8
#使用以下命令运行MySQL 8的Docker容器，并设置root用户的密码为123456：
docker run -p 3306:3306 --name mysql8 -e MYSQL_ROOT_PASSWORD=123456 -v /root/ivy/data:/var/lib/mysql:Z -d mysql:8
#检查容器是否正在运行
docker ps

其中：
-p 3306:3306：将容器的3306端口映射到主机的3306端口。
--name mysql8：为容器指定名称mysql8。
-e MYSQL_ROOT_PASSWORD=123456：设置root用户的密码为123456。
-d：以守护进程模式运行容器。
mysql:8：指定要运行的MySQL版本为8。
```

安装nginx镜像：

```
docker pull nginx

docker stop ivynginx
docker rm ivynginx

docker run --name ivynginx -p 80:80 -v /root/ivy/nginx.conf:/etc/nginx/nginx.conf:ro -v /root/ivy/html:/usr/share/nginx/html:ro -d nginx
```

前后端一键运行docker脚本：docker-run.sh

前端docker脚本：docker-nginx.sh

后端docker脚本：docker-web.sh
